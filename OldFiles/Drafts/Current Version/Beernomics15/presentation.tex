%!TEX program = xelatex
\documentclass[10pt, compress]{beamer}
\usetheme[titleprogressbar]{m}
\usepackage{booktabs}
\usepackage[scale=2]{ccicons}
\usepackage{minted}
\usepackage{natbib}

\usepgfplotslibrary{dateplot}

\usemintedstyle{trac}

\title[College Alcohol Consumption]{College Alcohol Consumption and NCAA Tournament Participation: the Health Costs of March Madness}
\author[White, Cowan and Wooten]{Dustin White \inst{1} \and Ben Cowan \inst{1} \and Jadrian Wooten \inst{2}}
\institute[]{\inst{1} Washington State University \and %
                      \inst{2} Pennsylvania State University}
%\setbeamercovered{transparent} 
%\setbeamertemplate{navigation symbols}{} 
%\logo{} 
%\institute{} 
%\date{} 
%\subject{} 
\begin{document}

\begin{frame}
\titlepage
\thispagestyle{empty}
\end{frame}

%\begin{frame}
%\tableofcontents
%\end{frame}

\begin{frame}{Introduction}
\begin{itemize}
\item Over 1,200 colleges and universities are members of the National Collegiate Athletic Association \citep{ncaa:members}
\item According to USAToday, in 2013 the average athletic department received a subsidy of approximately \$10.6 million (about 225 programs were included)
\pause
\item What are the costs of investing so heavily in athletics?
\end{itemize}
\end{frame}

\begin{frame}{Research Question}
\begin{itemize}
\item What is the effect of intercollegiate athletics on alcohol consumption?
\begin{itemize}
\item We study this question by using the NCAA Men's Basketball tournament to examine the effect of an additional game on the alcohol consumed by the student body at schools across the United States
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Previous Literature}
\begin{itemize}
\item \cite{tomaintercollegiate1998} found that winning a national championship in either men's basketball or football increases the number of applicants to a school
\item \cite{popeunderstanding2012, popethe2008} find that improved performance in football or basketball in a given year can increase the number of SAT scores submitted to a school by up to 10\%
\item \cite{lindoare2012} observed that academic performance suffered during years in which the football team at Oregon performed exceptionally well, and that this difference was most pronounced among males
\item \cite{hernandez-julianare2013} confirm the findings of Lindo, Swensen and Waddell by observing a similar effect at Clemson University
\end{itemize}
\end{frame}

\begin{frame}{Empirical Model}
\begin{itemize}
\item We use the difference in post-season length between teams that are invited to participate in postseason tournaments (which for our purposes is either the NCAA Tournament or NIT) and those that are not
\item We compare responses during the NCAA tournament and NIT to those before and after, and also compare responses of those students who attend or do not attend schools participating in the NCAA tournament
\item Our treatment group is students who respond \emph{during} the NCAA tournament, and who attend schools participating in the tournament during that year.
\end{itemize}
\end{frame}

\begin{frame}{Empirical Model}
\begin{itemize}
\item This leads us to a straightforward identification procedure using the following reduced form model:
\begin{align}
D_{isy} &= \beta_0 + \beta_1 \cdot part_{sy} + \beta_2 \cdot treated_{isy} \\
& + \mu_{isy} + \gamma_s + \delta_y + \lambda \cdot X_{isy} + \epsilon_{isy} \nonumber
\end{align}
\item $D$ represents the drinking behavior of interest, and $i$, $s$, and $y$ are indices for individual, school, and year, respectively
\end{itemize}
\end{frame}

\begin{frame}{Data}
\begin{itemize}
\item Harvard School of Public Health College Alcohol Study (CAS)
\item Conducted between 1993 and 2001 at 82 colleges and universities in the United States in 40 states and the District of Columbia, 44 of which participated in Division I Men's Basketball
\item Random sample of students from each school in each year
\item Gathered demographics and information about an individual's participation in school organizations, social activities, drug use, and sexual activity
\item Variable of interest is the \emph{interaction} of NCAA tournament (or NIT) participation by a student's school with responding to the survey during the time of the tournament
\end{itemize}
\end{frame}

\begin{frame}{Summary Statistics by Tournament Status}
\begin{tiny}
\begin{table}[!htbp] \centering 
  \caption{Summary Statistics (Means) for Tournament and Non-Tournament School-Years} 
  \label{summ} 
\begin{tabular}{@{\extracolsep{5pt}} lccc} 
\\[-1.8ex]\hline \\[-1.8ex] 
 & Non-Tournament Schools & Tournament Schools & Difference Significant (90\%)? \\ 
\hline 
\hline \\[-1.8ex] 
Binges Past 2 Weeks & 1.379 & 1.527 & Yes \\ 
Drink Past Month? & 0.652 & 0.699 & Yes \\ 
\# Drinks Past Month & 24.289 & 27.189 & Yes \\ 
Rounds in Tournament & 0 & 0.231 & Yes \\ 
Tournament Participation & 0 & 0.144 & Yes \\ 
Win Percent & 40.63 & 67.583 & Yes \\ 
Age & 21.039 & 20.678 & Yes \\ 
Greek & 0.153 & 0.209 & Yes \\ 
Married & 0.097 & 0.069 & Yes \\ 
Female & 0.574 & 0.577 & No \\ 
Freshman & 0.205 & 0.22 & Yes \\ 
Sophomore & 0.202 & 0.215 & Yes \\ 
Junior & 0.248 & 0.237 & No \\ 
Senior & 0.237 & 0.228 & No \\ 
White & 0.772 & 0.859 & Yes \\ 
Black & 0.063 & 0.036 & Yes \\ 
Asian & 0.075 & 0.046 & Yes \\ 
Other Race & 0.091 & 0.059 & Yes \\ 
GPA & 3.162 & 3.153 & No \\ 
\hline 
\hline \\[-1.8ex] 
\end{tabular} 
\end{table} 
\end{tiny}
\end{frame}

\begin{frame}{Summary Statistics by Date of Response}
\begin{tiny}
\begin{table}[!htbp] \centering 
  \caption{Summary Statistics (Means) for Tournament and Non-Tournament Observations at Tournament Schools} 
  \label{summ2} 
\begin{tabular}{@{\extracolsep{5pt}} lccc} 
\\[-1.8ex]\hline \\[-1.8ex] 
 & Tournament Respondents & Non-Tournament Respondents & Difference Significant (90\%)? \\ 
\hline 
\hline \\[-1.8ex] 
Binges Past 2 Weeks & 1.818 & 1.478 & Yes \\ 
Drink Past Month? & 0.631 & 0.71 & Yes \\ 
\# Drinks Past Month & 33.888 & 26.059 & Yes \\ 
Rounds in Tournament & 1.601 & 0 & Yes \\ 
Win Percent & 66.185 & 67.819 & Yes \\ 
Age & 20.644 & 20.683 & No \\ 
Greek & 0.194 & 0.211 & No \\ 
Married & 0.062 & 0.07 & No \\ 
Female & 0.587 & 0.575 & No \\ 
Freshman & 0.236 & 0.217 & No \\ 
Sophomore & 0.236 & 0.211 & No \\ 
Junior & 0.229 & 0.238 & No \\ 
Senior & 0.208 & 0.231 & No \\ 
White & 0.831 & 0.864 & Yes \\ 
Black & 0.042 & 0.035 & No \\ 
Asian & 0.048 & 0.046 & No \\ 
Other Race & 0.079 & 0.055 & Yes \\ 
GPA & 3.173 & 3.149 & No \\ 
\hline 
\hline \\[-1.8ex] 
\end{tabular} 
\end{table} 
\end{tiny}
\end{frame}


%\begin{frame}{Results - Tournament Participation}
%\begin{Tiny}
%\begin{table}[!htbp] \centering 
%  \caption{Effects on Drinking of NCAA Tournament Participation} 
%  \label{regtourney} 
%\begin{tabular}{@{\extracolsep{5pt}}lccc} 
%\\[-1.8ex]\hline 
%\hline \\[-1.8ex] 
%\\[-1.8ex] & Binges Past 2 Weeks & Drink Last Month? & Drinks Last Month \\ 
%\hline \\[-1.8ex] 
% Particip.*During Tourney (Binge) & 0.370$^{**}$ & N/A & N/A \\ 
%  & (0.170) &  &  \\ 
%  Particip.*During Tourney (Other) & N/A & $-$0.046 & 12.174$^{***}$ \\ 
%  &  & (0.043) & (4.381) \\ 
%  Particip.*After Tourney & 0.066 & $-$0.074 & $-$0.540 \\ 
%  & (0.205) & (0.053) & (4.408) \\ 
%  Participation & $-$0.022 & 0.023 & $-$0.525 \\ 
%  & (0.081) & (0.020) & (1.567) \\ 
%  During Tourney (Binge) & 0.139 & N/A & N/A \\ 
%  & (0.134) &  &  \\ 
%  Win Percent*During (Binge) & $-$0.003 & N/A & N/A \\ 
%  & (0.003) &  &  \\ 
%  During Tourney (Other) & N/A & 0.053$^{*}$ & 4.493$^{**}$ \\ 
%  &  & (0.029) & (2.100) \\ 
%  Win Percent*During (Other) & N/A & $-$0.001 & $-$0.054 \\ 
%  &  & (0.001) & (0.047) \\ 
%  After Tourney & 0.059 & 0.015 & 0.0004 \\ 
%  & (0.106) & (0.032) & (2.004) \\ 
%  Win Percent* After Tourney & $-$0.001 & 0.0002 & 0.033 \\ 
%  & (0.003) & (0.001) & (0.051) \\ 
%  Win Percent & 0.001 & 0.0004 & 0.027 \\ 
%  & (0.002) & (0.0005) & (0.036) \\ 
%    \hline
%  N & 19569 & 18247 & 19844 \\
% \hline 
%\hline \\[-1.8ex] 
%\multicolumn{4}{l}{\textit{Notes:}$^{***}$Significant at the 1 percent level.} \\ 
%\multicolumn{4}{l}{$^{**}$Significant at the 5 percent level.} \\ 
%\multicolumn{4}{l}{$^{*}$Significant at the 10 percent level.} \\ 
%\multicolumn{4}{l}{Other controls included in regression were indicators for ages between 17 and 24, race, marital status,} \\
%\multicolumn{4}{l}{ gender, membership in greek organizations, GPA, athletic conference, school and year.}  \\ 
%\multicolumn{4}{l}{Standard errors were clustered at the school level and robust to heteroscedasticity} \\ 
%\end{tabular} 
%\end{table} 
%
%\end{Tiny}
%\end{frame}



\begin{frame}{Preliminary Results - Continuous}
\begin{center}
\includegraphics[scale=.25]{../ContPlot.jpeg} 
\end{center}
\end{frame}




\begin{frame}{Preliminary Results - Discontinuous}
\begin{center}
\includegraphics[scale=.25]{../DiscontPlot.jpeg} 
\end{center}
\end{frame}




\begin{frame}{Results - Each Round}
\vspace{-2em}
\begin{Tiny}
\begin{table}[!htbp] \centering 
  \caption{Effects of Rounds in NCAA Tournament on Drinking} 
  \label{robust} 
\begin{tabular}{@{\extracolsep{5pt}}lccc} 
\\[-1.8ex]\hline 
\hline \\[-1.8ex] 
\\[-1.8ex] & Binge Occasions & Drink Last Month? & \# Drinks Past Month \\ 
\hline \\[-1.8ex] 
NCAA Rounds (Binge) & 0.184$^{***}$ & -- & -- \\ 
  & (0.066) & -- & -- \\ 
  NCAA Rounds (Other) & -- & $-$0.021$^{**}$ & 2.328$^{***}$ \\ 
  & -- & (0.010) & (0.650) \\ 
  NCAA Tournament Participation & $-$0.048 & $-$0.013 & $-$0.126 \\ 
  & (0.061) & (0.014) & (0.805) \\ 
  NIT Rounds (Binge) & 0.027 & -- & -- \\ 
  & (0.034) & -- & -- \\ 
  NIT Rounds (Other) & -- & $-$0.001 & 0.958$^{**}$ \\ 
  & -- & (0.007) & (0.439) \\ 
  NIT Tournament Participation & $-$0.158$^{***}$ & $-$0.025$^{*}$ & $-$3.289$^{***}$ \\ 
  & (0.057) & (0.014) & (0.757) \\ 
  Win Percent & 0.0003 & 0.001$^{***}$ & 0.016 \\ 
  & (0.001) & (0.0003) & (0.019) \\ 
  Greek & 0.887$^{***}$ & 0.122$^{***}$ & 15.093$^{***}$ \\ 
  & (0.040) & (0.007) & (0.546) \\ 
  Married & $-$0.415$^{***}$ & $-$0.091$^{***}$ & $-$5.568$^{***}$ \\ 
  & (0.041) & (0.013) & (0.547) \\ 
  Female & $-$0.723$^{***}$ & $-$0.033$^{***}$ & $-$14.820$^{***}$ \\ 
  & (0.026) & (0.006) & (0.351) \\ 
  GPA & $-$0.431$^{***}$ & $-$0.048$^{***}$ & $-$7.828$^{***}$ \\ 
  & (0.023) & (0.005) & (0.321) \\ 
    \hline
    N & 25000 & 25000 & 25000 \\
  R$^2$ & 0.177 & 0.112 & 0.146 \\
 \hline 
\hline \\[-1.8ex] 
\textit{Notes:} & \multicolumn{3}{l}{$^{***}$Significant at the 1 percent level.} \\ 
            \multicolumn{4}{l}{$^{**}$Significant at the 5 percent level.} \\ 
            \multicolumn{4}{l}{$^{*}$Significant at the 10 percent level.} \\ 
            \multicolumn{4}{l}{Other controls included in regression were indicators for ages between 17 and 24,}  \\ 
            \multicolumn{4}{l}{race, year in school, athletic conference, school, month and year.} \\ 
\end{tabular} 
\end{table} 

\end{Tiny}
\end{frame}

\begin{frame}{Gender Differences}
\vspace{-2em}
\begin{Tiny}
\begin{table}[!htbp] \centering 
  \caption{Effects of Rounds in NCAA Tournament on Drinking of Males} 
  \label{robmale} 
\begin{tabular}{@{\extracolsep{5pt}}lccc} 
\\[-1.8ex]\hline 
\hline \\[-1.8ex] 
\\[-1.8ex] & Binge Occasions & Drink Last Month? & \# Drinks Past Month \\ 
\hline \\[-1.8ex] 
NCAA Rounds (Binge) & 0.345$^{***}$ & -- & -- \\ 
  & (0.108) & -- & -- \\ 
  NCAA Rounds (Other) & -- & 0.0003 & 4.472$^{**}$ \\ 
  & -- & (0.014) & (1.812) \\ 
  NCAA Tournament Participation & $-$0.131 & $-$0.032 & $-$1.878 \\ 
  & (0.104) & (0.020) & (2.300) \\ 
  NIT Rounds (Binge) & 0.090 & -- & -- \\ 
  & (0.058) & -- & -- \\ 
  NIT Rounds (Other) & -- & 0.003 & 1.930 \\ 
  & -- & (0.010) & (1.244) \\ 
  NIT Tournament Participation & $-$0.171$^{*}$ & $-$0.032 & $-$4.469$^{**}$ \\ 
  & (0.100) & (0.021) & (2.239) \\ 
  Win Percent & 0.003 & 0.001$^{***}$ & 0.077 \\ 
  & (0.003) & (0.0005) & (0.056) \\ 
  Greek & 1.201$^{***}$ & 0.146$^{***}$ & 22.585$^{***}$ \\ 
  & (0.068) & (0.011) & (1.567) \\ 
  Married & $-$0.649$^{***}$ & $-$0.080$^{***}$ & $-$8.939$^{***}$ \\ 
  & (0.078) & (0.020) & (1.743) \\ 
  GPA & $-$0.497$^{***}$ & $-$0.051$^{***}$ & $-$10.085$^{***}$ \\ 
  & (0.039) & (0.007) & (0.938) \\ 
    \hline
    N & 10641 & 10641 & 10641 \\
  R$^2$ & 0.162 & 0.104 & 0.126 \\
 \hline 
\hline \\[-1.8ex] 
\textit{Notes:} & \multicolumn{3}{l}{$^{***}$Significant at the 1 percent level.} \\ 
            \multicolumn{4}{l}{$^{**}$Significant at the 5 percent level.} \\ 
            \multicolumn{4}{l}{$^{*}$Significant at the 10 percent level.} \\ 
            \multicolumn{4}{l}{Other controls included in regression were indicators for ages between 17 and 24,}  \\ 
            \multicolumn{4}{l}{race, year in school, athletic conference, school, month and year.} \\ 
\end{tabular} 
\end{table} 

\end{Tiny}
\end{frame}

\begin{frame}{Gender Differences}
\vspace{-2em}

\begin{Tiny}
\begin{table}[!htbp] \centering 
  \caption{Effects of Rounds in NCAA Tournament on Drinking of Females} 
  \label{robfemale} 
\begin{tabular}{@{\extracolsep{5pt}}lccc} 
\\[-1.8ex]\hline 
\hline \\[-1.8ex] 
\\[-1.8ex] & Binge Occasions & Drink Last Month? & \# Drinks Past Month \\ 
\hline \\[-1.8ex] 
NCAA Rounds (Binge) & 0.040 & -- & -- \\ 
  & (0.079) & -- & -- \\ 
  NCAA Rounds (Other) & -- & $-$0.039$^{***}$ & 0.582 \\ 
  & -- & (0.013) & (1.002) \\ 
  NCAA Tournament Participation & 0.021 & 0.004 & 1.135 \\ 
  & (0.072) & (0.018) & (1.289) \\ 
  NIT Rounds (Binge) & $-$0.030 & -- & -- \\ 
  & (0.038) & -- & -- \\ 
  NIT Rounds (Other) & -- & $-$0.002 & 0.178 \\ 
  & -- & (0.009) & (0.636) \\ 
  NIT Tournament Participation & $-$0.130$^{**}$ & $-$0.019 & $-$2.194$^{**}$ \\ 
  & (0.065) & (0.018) & (1.116) \\ 
  Win Percent & $-$0.002 & 0.001 & $-$0.032 \\ 
  & (0.002) & (0.0004) & (0.030) \\ 
  Greek & 0.622$^{***}$ & 0.100$^{***}$ & 8.985$^{***}$ \\ 
  & (0.047) & (0.010) & (0.831) \\ 
  Married & $-$0.321$^{***}$ & $-$0.094$^{***}$ & $-$4.701$^{***}$ \\ 
  & (0.042) & (0.016) & (0.740) \\ 
  GPA & $-$0.373$^{***}$ & $-$0.045$^{***}$ & $-$5.756$^{***}$ \\ 
  & (0.026) & (0.007) & (0.454) \\ 
    \hline
    N & 14359 & 14359 & 14359 \\
  R$^2$ & 0.147 & 0.115 & 0.123 \\
 \hline 
\hline \\[-1.8ex] 
\textit{Notes:} & \multicolumn{3}{l}{$^{***}$Significant at the 1 percent level.} \\ 
            \multicolumn{4}{l}{$^{**}$Significant at the 5 percent level.} \\ 
            \multicolumn{4}{l}{$^{*}$Significant at the 10 percent level.} \\ 
            \multicolumn{4}{l}{Other controls included in regression were indicators for ages between 17 and 24,}  \\ 
            \multicolumn{4}{l}{race, year in school, athletic conference, school, month and year.} \\ 
\end{tabular} 
\end{table} 

\end{Tiny}
\end{frame}


\begin{frame}{What about Spring Break?}
\begin{itemize}
\item If schools expecting to participate in the NCAA tournament regularly systematically schedule spring break to overlap the tournament, then spring break could confound our results
\item If this is \emph{not} the case, then the month effects should account for seasonal effects such as spring break
\item Additionally, we look at tournament schools only in order to determine if students at these schools behave differently in tournament and non-tournament years
\end{itemize}
\end{frame}


\begin{frame}{Results - Using Only Across Year Variation}
\vspace{-2em}

\begin{Tiny}
\begin{table}[!htbp] \centering 
  \caption{Effects of Participation on Drinking Among Tournament Schools} 
  \label{reground} 
\begin{tabular}{@{\extracolsep{5pt}}lccc} 
\\[-1.8ex]\hline 
\hline \\[-1.8ex] 
\\[-1.8ex] & Binge Occasions & Drink Last Month? & \# Drinks Past Month \\ 
\hline \\[-1.8ex] 
NCAA Rounds (Binge) & 0.207$^{***}$ & -- & -- \\ 
  & (0.071) & -- & -- \\ 
  NCAA Rounds (Other) & -- & $-$0.007 & 3.420$^{***}$ \\ 
  & -- & (0.011) & (0.741) \\ 
  NCAA Tournament Participation & $-$0.173 & $-$0.038 & $-$3.315$^{**}$ \\ 
  & (0.107) & (0.024) & (1.482) \\ 
  NIT Rounds (Binge) & 0.084$^{*}$ & -- & -- \\ 
  & (0.044) & -- & -- \\ 
  NIT Rounds (Other) & -- & 0.006 & 0.516 \\ 
  & -- & (0.009) & (0.516) \\ 
  NIT Tournament Participation & $-$0.253$^{**}$ & $-$0.025 & $-$4.684$^{***}$ \\ 
  & (0.099) & (0.022) & (1.325) \\ 
  Win Percent & 0.004 & 0.001 & 0.115$^{**}$ \\ 
  & (0.004) & (0.001) & (0.054) \\ 
  Greek & 0.872$^{***}$ & 0.102$^{***}$ & 15.027$^{***}$ \\ 
  & (0.060) & (0.011) & (0.820) \\ 
  Married & $-$0.576$^{***}$ & $-$0.098$^{***}$ & $-$8.164$^{***}$ \\ 
  & (0.078) & (0.023) & (1.093) \\ 
  Female & $-$0.740$^{***}$ & $-$0.015 & $-$15.857$^{***}$ \\ 
  & (0.045) & (0.010) & (0.616) \\ 
  GPA & $-$0.482$^{***}$ & $-$0.061$^{***}$ & $-$9.126$^{***}$ \\ 
  & (0.038) & (0.008) & (0.559) \\ 
      \hline
    N & 9527 & 9527 & 9527 \\
  R$^2$ & 0.153 & 0.097 & 0.127 \\
 \hline 
\hline \\[-1.8ex] 
\textit{Notes:} & \multicolumn{3}{l}{$^{***}$Significant at the 1 percent level.} \\ 
            \multicolumn{4}{l}{$^{**}$Significant at the 5 percent level.} \\ 
            \multicolumn{4}{l}{$^{*}$Significant at the 10 percent level.} \\ 
            \multicolumn{4}{l}{Other controls included in regression were indicators for ages between 17 and 24,}  \\ 
            \multicolumn{4}{l}{race, year in school, athletic conference, school, month and year.} \\ 
\end{tabular} 
\end{table} 
\end{Tiny}
\end{frame}

\begin{frame}{Conclusion}
\begin{itemize}
\item An additional round played in the NCAA tournament by an institution's basketball team induces a 12\% increase in binge drinking incidents among students at that school during the tournament
\item This corresponds to an increase of alcohol consumption by 2.3 beverages per student
\item This increase is more pronounced in males, as \cite{lindoare2012} find is the case with grade effects
\item Schools should be wary of increasing the length of postseason tournaments, since these tournaments induce increased health risks in the student body
\end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]{Bibliography}
\bibliographystyle{apalike}
\bibliography{newbib.bib}
\end{frame}

\end{document}