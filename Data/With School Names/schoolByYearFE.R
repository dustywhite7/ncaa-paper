library("stargazer")
library("lmtest")
library("sandwich")

# ##### OLD ESTIMATIONS
# orobustbinge <- lm(num5pl ~ Rounds*post2w + Rounds*after + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + married + female + frosh + soph + jun + sen + white + black + asian + other + gpa + college + athconf + month*factor(year), data = data2)
# orobustdrink <- lm(drinklast30 ~ Rounds*postm + Rounds*after + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + married + female + frosh + soph + jun + sen + white + black + asian + other + gpa + college + athconf + month*factor(year), data = data2)
# orobustvol <- lm(vol30 ~ Rounds*postm + Rounds*after + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + married + female + frosh + soph + jun + sen + white + black + asian + other + gpa + college + athconf + month*factor(year), data = data2)

##### EVERYONE
robustbinge <- lm(num5pl ~ exTourney + Tourney + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + married + female +  frosh + soph + jun + sen + white + black + asian + other + gpa + college + athconf + factor(year)*factor(college), data = data2)
robustdrink <- lm(drinklast30 ~ exTourneym + Tourney + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + married + female + frosh + soph + jun + sen + white + black + asian + other + gpa + college + athconf + factor(year)*factor(college), data = data2)
robustvol <- lm(vol30 ~ exTourneym + Tourney + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + married + female + frosh + soph + jun + sen + white + black + asian + other + gpa + college + athconf + factor(year)*factor(college), data = data2)
# robustbinge <- lm(num5pl ~ exRounds + Tourney + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + married + female +  frosh + soph + jun + sen + white + black + asian + other + gpa + college + athconf + month*factor(year)*factor(year), data = data2)
# robustdrink <- lm(drinklast30 ~ exRoundsm + Tourney + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + married + female + frosh + soph + jun + sen + white + black + asian + other + gpa + college + athconf + month*factor(year), data = data2)
# robustvol <- lm(vol30 ~ exRoundsm + Tourney + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + married + female + frosh + soph + jun + sen + white + black + asian + other + gpa + college + athconf + month*factor(year), data = data2)


##### MALES
robustbingem <- lm(num5pl ~ exTourney + Tourney + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + married + female + frosh + soph + jun + sen + white + black + asian + other + gpa + college + athconf + factor(year)*factor(college), data = males)
robustdrinkm <- lm(drinklast30 ~ exTourneym + Tourney + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + married + female + frosh + soph + jun + sen + white + black + asian + other + gpa + college + athconf + factor(year)*factor(college), data = males)
robustvolm <- lm(vol30 ~ exTourneym + Tourney + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + married + female + frosh + soph + jun + sen + white + black + asian + other + gpa + college + athconf + factor(year)*factor(college), data = males)
# robustbingem <- lm(num5pl ~ exRounds + Tourney + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + married + female + frosh + soph + jun + sen + white + black + asian + other + gpa + college + athconf + month*factor(year), data = males)
# robustdrinkm <- lm(drinklast30 ~ exRoundsm + Tourney + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + married + female + frosh + soph + jun + sen + white + black + asian + other + gpa + college + athconf + month*factor(year), data = males)
# robustvolm <- lm(vol30 ~ exRoundsm + Tourney + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + married + female + frosh + soph + jun + sen + white + black + asian + other + gpa + college + athconf + month*factor(year), data = males)

##### FEMALES
robustbingef <- lm(num5pl ~ exTourney + Tourney + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + married + female + frosh + soph + jun + sen + white + black + asian + other + gpa + college + athconf + factor(year)*factor(college), data = females)
robustdrinkf <- lm(drinklast30 ~ exTourneym + Tourney + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + married + female + frosh + soph + jun + sen + white + black + asian + other + gpa + college + athconf + factor(year)*factor(college), data = females)
robustvolf <- lm(vol30 ~ exTourneym + Tourney + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + married + female + frosh + soph + jun + sen + white + black + asian + other + gpa + college + athconf + factor(year)*factor(college), data = females)
# robustbingef <- lm(num5pl ~ exRounds + Tourney + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + married + female + frosh + soph + jun + sen + white + black + asian + other + gpa + college + athconf + month*factor(year), data = females)
# robustdrinkf <- lm(drinklast30 ~ exRoundsm + Tourney + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + married + female + frosh + soph + jun + sen + white + black + asian + other + gpa + college + athconf + month*factor(year), data = females)
# robustvolf <- lm(vol30 ~ exRoundsm + Tourney + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + married + female + frosh + soph + jun + sen + white + black + asian + other + gpa + college + athconf + month*factor(year), data = females)


# compute Stata like df-adjustment
G <- length(unique(data2$college))
N <- length(data2$college)


# compute Stata like df-adjustment
Gm <- length(unique(males$college))
Nm <- length(males$college)


# compute Stata like df-adjustment
Gf <- length(unique(females$college))
Nf <- length(females$college)

# Clustered and Heteroskedasticity corrected results: EVERYONE
dfa <- (G/(G - 1)) * (N - 1)/robustbinge$df.residual
college_c_vcov <- dfa * vcovHC(robustbinge, type = "HC0", cluster = "group", adjust = T)
rbinge <- coeftest(robustbinge, vcov = college_c_vcov)

dfa <- (G/(G - 1)) * (N - 1)/robustdrink$df.residual
college_c_vcov <- dfa * vcovHC(robustdrink, type = "HC0", cluster = "group", adjust = T)
rdrink <- coeftest(robustdrink, vcov = college_c_vcov)

dfa <- (G/(G - 1)) * (Nm - 1)/robustvol$df.residual
college_c_vcov <- dfa * vcovHC(robustvol, type = "HC0", cluster = "group", adjust = T)
rvol <- coeftest(robustvol, vcov = college_c_vcov)

# dfa <- (G/(G - 1)) * (N - 1)/orobustbinge$df.residual
# college_c_vcov <- dfa * vcovHC(orobustbinge, type = "HC0", cluster = "group", adjust = T)
# orbinge <- coeftest(orobustbinge, vcov = college_c_vcov)
# 
# dfa <- (G/(G - 1)) * (N - 1)/orobustdrink$df.residual
# college_c_vcov <- dfa * vcovHC(orobustdrink, type = "HC0", cluster = "group", adjust = T)
# ordrink <- coeftest(orobustdrink, vcov = college_c_vcov)
# 
# dfa <- (G/(G - 1)) * (Nm - 1)/orobustvol$df.residual
# college_c_vcov <- dfa * vcovHC(orobustvol, type = "HC0", cluster = "group", adjust = T)
# orvol <- coeftest(orobustvol, vcov = college_c_vcov)

# Clustered and Heteroskedasticity corrected results: MALES
dfa <- (Gm/(Gm - 1)) * (Nm - 1)/robustbingem$df.residual
college_c_vcov <- dfa * vcovHC(robustbingem, type = "HC0", cluster = "group", adjust = T)
rbingem <- coeftest(robustbingem, vcov = college_c_vcov)

dfa <- (Gm/(Gm - 1)) * (Nm - 1)/robustdrinkm$df.residual
college_c_vcov <- dfa * vcovHC(robustdrinkm, type = "HC0", cluster = "group", adjust = T)
rdrinkm <- coeftest(robustdrinkm, vcov = college_c_vcov)

dfa <- (Gm/(Gm - 1)) * (Nm - 1)/robustvolm$df.residual
college_c_vcov <- dfa * vcovHC(robustvolm, type = "HC0", cluster = "group", adjust = T)
rvolm <- coeftest(robustvolm, vcov = college_c_vcov)

# Clustered and Heteroskedasticity corrected results: FEMALES
dfa <- (Gf/(Gf - 1)) * (Nf - 1)/robustbingef$df.residual
college_c_vcov <- dfa * vcovHC(robustbingef, type = "HC0", cluster = "group", adjust = T)
rbingef <- coeftest(robustbingef, vcov = college_c_vcov)

dfa <- (Gf/(Gf - 1)) * (Nf - 1)/robustdrinkf$df.residual
college_c_vcov <- dfa * vcovHC(robustdrinkf, type = "HC0", cluster = "group", adjust = T)
rdrinkf <- coeftest(robustdrinkf, vcov = college_c_vcov)

dfa <- (Gf/(Gf - 1)) * (Nf - 1)/robustvolf$df.residual
college_c_vcov <- dfa * vcovHC(robustvolf, type = "HC0", cluster = "group", adjust = T)
rvolf <- coeftest(robustvolf, vcov = college_c_vcov)



#capture.output(
#OLD OUTPUT
#   stargazer(orbinge, ordrink, orvol,
#             style = "qje",
#             label = "robust",
#             model.numbers = FALSE,
#             title = "Effects on Drinking of Rounds in NCAA Tournament",
#             dep.var.labels = c("Binge Occasions", "Drink Last Month", "Number of Drinks"),
#             keep = c("Rounds:post2wTRUE", "Rounds:postmTRUE", "Tourney", "Percent", "greek", "married", "female", "frosh", "soph", "jun", "sen", "white", "black", "asian", "gpa"),
#             order = c("Rounds:post2wTRUE", "Rounds:postmTRUE", "Tourney", "Percent", "greek", "married", "female", "frosh", "soph", "jun", "sen", "white", "black", "asian", "gpa"),
#             covariate.labels = c("Rounds (Binge)", "Rounds (Other)", "School Tournament Participation", "Win Percent", "Greek",
#                                  "Married", "Female", "Freshman", "Sophomore", "Junior", "Senior",
#                                  "White", "Black", "Asian", "GPA"),
#             no.space = TRUE,
#             digits = 3,
#             notes.align = "l",
#             notes.append = FALSE,
#             notes =  "$^{***}$Significant at the 1 percent level.} \\\\ 
#             \\multicolumn{4}{l}{$^{**}$Significant at the 5 percent level.} \\\\ 
#             \\multicolumn{4}{l}{$^{*}$Significant at the 10 percent level.} \\\\ 
#             \\multicolumn{4}{l}{Other controls included in regression were indicators for ages}  \\\\ 
#             \\multicolumn{4}{l}{between 17 and 24, athletic conference, school, month and year."
#   ),
#   stargazer(rbinge, rdrink, rvol,
#             style = "qje",
#             label = "robust",
#             model.numbers = FALSE,
#             title = "Effects on Drinking of Rounds in NCAA Tournament",
#             dep.var.labels = c("Binge Occasions", "Drink Last Month", "Number of Drinks"),
#             keep = c("exRounds", "exRoundsm", "Tourney", "Percent", "greek", "married", "female", "frosh", "soph", "jun", "sen", "white", "black", "asian", "gpa"),
#             order = c("exRounds", "exRoundsm", "Tourney", "Percent", "greek", "married", "frosh", "soph", "jun", "sen", "white", "black", "asian", "gpa"),
#             covariate.labels = c("NCAA Rounds (Binge)", "NCAA Rounds (Other)", "Tournament Year", "Win Percent", "Greek",
#                                  "Married", "Female", "Freshman", "Sophomore", "Junior", "Senior",
#                                  "White", "Black", "Asian", "GPA"),
#             no.space = TRUE,
#             digits = 3,
#             notes.align = "l",
#             notes.append = FALSE,
#             notes =  "$^{***}$Significant at the 1 percent level.} \\\\ 
#             \\multicolumn{4}{l}{$^{**}$Significant at the 5 percent level.} \\\\ 
#             \\multicolumn{4}{l}{$^{*}$Significant at the 10 percent level.} \\\\ 
#             \\multicolumn{4}{l}{Other controls included in regression were indicators for ages}  \\\\ 
#             \\multicolumn{4}{l}{between 17 and 24, athletic conference, school, month and year.} \\\\"
#   )#,
#   stargazer(rbingem, rdrinkm, rvolm,
#             style = "qje",
#             label = "robmale",
#             model.numbers = FALSE,
#             title = "Effects on Drinking of Rounds in NCAA Tournament on Males",
#             dep.var.labels = c("Binge Occasions", "Drink Last Month", "Number of Drinks"),
#             keep = c("exRounds", "exRoundsm", "Tourney", "Percent", "greek", "married", "frosh", "soph", "jun", "sen", "white", "black", "asian", "gpa"),
#             order = c("exRounds", "exRoundsm", "Tourney", "Percent", "greek", "married", "frosh", "soph", "jun", "sen", "white", "black", "asian", "gpa"),
#             covariate.labels = c("NCAA Rounds (Binge)", "NCAA Rounds (Other)", "Tournament Year", "Win Percent", "Greek",
#                                  "Married", "Freshman", "Sophomore", "Junior", "Senior",
#                                  "White", "Black", "Asian", "GPA"),
#             no.space = TRUE,
#             digits = 3,
#             notes.align = "l",
#             notes.append = FALSE,
#             notes =  "$^{***}$Significant at the 1 percent level.} \\\\ 
#             \\multicolumn{4}{l}{$^{**}$Significant at the 5 percent level.} \\\\ 
#             \\multicolumn{4}{l}{$^{*}$Significant at the 10 percent level.} \\\\ 
#             \\multicolumn{4}{l}{Other controls included in regression were indicators for ages}  \\\\ 
#             \\multicolumn{4}{l}{between 17 and 24, athletic conference, school, month and year.} \\\\"
#   )#,
#   
#   stargazer(rbingef, rdrinkf, rvolf,
#             style = "qje",
#             label = "robfemale",
#             model.numbers = FALSE,
#             title = "Effects on Drinking of Rounds in NCAA Tournament on Females",
#             dep.var.labels = c("Binge Occasions", "Drink Last Month", "Number of Drinks"),
#             keep = c("exRounds", "exRoundsm", "Tourney", "Percent", "greek", "married", "frosh", "soph", "jun", "sen", "white", "black", "asian", "gpa"),
#             order = c("exRounds", "exRoundsm", "Tourney", "Percent", "greek", "married", "frosh", "soph", "jun", "sen", "white", "black", "asian", "gpa"),
#             covariate.labels = c("NCAA Rounds (Binge)", "NCAA Rounds (Other)", "Tournament Year", "Win Percent", "Greek",
#                                  "Married", "Freshman", "Sophomore", "Junior", "Senior",
#                                  "White", "Black", "Asian", "GPA"),
#             no.space = TRUE,
#             digits = 3,
#             notes.align = "l",
#             notes.append = FALSE,
#             notes.label = NULL,
#             notes =  "$^{***}$Significant at the 1 percent level.} \\\\ 
#             \\multicolumn{4}{l}{$^{**}$Significant at the 5 percent level.} \\\\ 
#             \\multicolumn{4}{l}{$^{*}$Significant at the 10 percent level.} \\\\ 
#             \\multicolumn{4}{l}{Other controls included in regression were indicators for ages}  \\\\ 
#             \\multicolumn{4}{l}{between 17 and 24, athletic conference, school, month and year.} \\\\"
#   )#,

#   file = "/Users/dusty/Desktop/TablesRobust.tex"
#   , file = "/Users/dusty/Documents/Current Research/Drinking and College Sports/Drafts/Third Draft/TablesRobust2.tex"
#  )


sigVals <- function(coef, pval) {
  if (pval<0.01) {
    return(paste(toString(round(coef, digits=3)),"$^{***}$", sep=""))
  }
  else if (pval<0.05) {
    return(paste(toString(round(coef, digits=3)),"$^{**}$", sep=""))
  }
  else if (pval<0.1) {
    return(paste(toString(round(coef, digits=3)),"$^{*}$", sep=""))
  }
  else {
    return(toString(round(coef, digits=3)))
  }
}



print(paste(" & ", sigVals(rbinge[2,1], rbinge[2,4]),
            " & ", sigVals(rdrink[2,1], rdrink[2,4]),
            " & ", sigVals(rvol[2,1], rvol[2,4]),
            " & ", sigVals(rbingem[2,1], rbingem[2,4]),
            " & ", sigVals(rdrinkm[2,1], rdrinkm[2,4]),
            " & ", sigVals(rvolm[2,1], rvolm[2,4]),
            " & ", sigVals(rbingef[2,1], rbingef[2,4]),
            " & ", sigVals(rdrinkf[2,1], rdrinkf[2,4]),
            " & ", sigVals(rvolf[2,1], rvolf[2,4]),
            " \\", sep=""), quote=FALSE)

print(paste(" & (", toString(round(rbinge[2,2], digits=3)),
            ") & (", toString(round(rdrink[2,2], digits=3)),
            ") & (", toString(round(rvol[2,2], digits=3)),
            ") & (", toString(round(rbingem[2,2], digits=3)),
            ") & (", toString(round(rdrinkm[2,2], digits=3)),
            ") & (", toString(round(rvolm[2,2], digits=3)),
            ") & (", toString(round(rbingef[2,2], digits=3)),
            ") & (", toString(round(rdrinkf[2,2], digits=3)),
            ") & (", toString(round(rvolf[2,2], digits=3)),
            ") \\", sep=""), quote=FALSE)







