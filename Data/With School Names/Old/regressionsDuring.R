library("stargazer", lib.loc="/Library/Frameworks/R.framework/Versions/3.1/Resources/library")
library("lmtest", lib.loc="/Library/Frameworks/R.framework/Versions/3.1/Resources/library")
library("sandwich", lib.loc="/Library/Frameworks/R.framework/Versions/3.1/Resources/library")


during2w <- subset(data2, data2$post2w==1)
mduring2w <-subset(during2w, during2w$female == 0)
fduring2w <-subset(during2w, during2w$female == 1)
duringm <- subset(data2, data2$postm==1)
mduringm <-subset(duringm, duringm$female == 0)
fduringm <-subset(duringm, duringm$female == 1)

roundbinge <- lm(num5pl ~ Rounds + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa + athconf, data=during2w)
tourneybinge <- lm(num5pl ~ Tourney + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa + athconf, data=during2w)
percentbinge <- lm(num5pl ~ Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa + athconf, data=during2w)


roundvol <- lm(vol30 ~ Rounds + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa + athconf, data=duringm)
tourneyvol <- lm(vol30 ~ Tourney + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa + athconf, data=duringm)
percentvol <- lm(vol30 ~ Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa + athconf, data=duringm)

rounddrink <- lm(drinklast30 ~ Rounds + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa + athconf, data=duringm)
tourneydrink <- lm(drinklast30 ~ Tourney + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa + athconf, data=duringm)
percentdrink <- lm(drinklast30 ~ Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa + athconf, data=duringm)


##### MALES ONLY #####
roundbingem <- lm(num5pl ~ Rounds + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa + athconf, data=mduring2w)
rounddrinkm <- lm(drinklast30 ~ Rounds + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa + athconf, data=mduringm)
roundvolm <- lm(vol30 ~ Rounds + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa + athconf, data=mduringm)

##### FEMALES ONLY #####
roundbingef <- lm(num5pl ~ Rounds + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa + athconf, data=fduring2w)
rounddrinkf <- lm(drinklast30 ~ Rounds + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa + athconf, data=fduringm)
roundvolf <- lm(vol30 ~ Rounds + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa + athconf, data=fduringm)

##### MARIJUANA USE - ROBUSTNESS CHECK #####
roundpot <- lm(marijuana ~ Rounds + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa + athconf, data=duringm)
tourneypot <- lm(marijuana ~ Tourney + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa + athconf, data=duringm)
percentpot <- lm(marijuana ~ Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa + athconf, data=duringm)



# compute Stata like df-adjustment
G2w <- length(unique(during2w$college))
N2w <- length(during2w$college)

# compute Stata like df-adjustment
Gm <- length(unique(duringm$college))
Nm <- length(duringm$college)

# compute Stata like df-adjustment
mG2w <- length(unique(mduring2w$college))
mN2w <- length(mduring2w$college)

# compute Stata like df-adjustment
mGm <- length(unique(mduringm$college))
mNm <- length(mduringm$college)

# compute Stata like df-adjustment
fG2w <- length(unique(fduring2w$college))
fN2w <- length(fduring2w$college)

# compute Stata like df-adjustment
fGm <- length(unique(fduringm$college))
fNm <- length(fduringm$college)

# Clustered and Heteroskedasticity corrected results: DRINKING DUMMY
dfa <- (Gm/(Gm - 1)) * (Nm - 1)/rounddrink$df.residual
college_c_vcov <- dfa * vcovHC(rounddrink, type = "HC0", cluster = "group", adjust = T)
rdrink <- coeftest(rounddrink, vcov = college_c_vcov)

dfa <- (Gm/(Gm - 1)) * (Nm - 1)/tourneydrink$df.residual
college_c_vcov <- dfa * vcovHC(tourneydrink, type = "HC0", cluster = "group", adjust = T)
tdrink <- coeftest(tourneydrink, vcov = college_c_vcov)

dfa <- (Gm/(Gm - 1)) * (Nm - 1)/percentdrink$df.residual
college_c_vcov <- dfa * vcovHC(percentdrink, type = "HC0", cluster = "group", adjust = T)
pdrink <- coeftest(percentdrink, vcov = college_c_vcov)

# Clustered and Heteroskedasticity corrected results: BINGE DRINKING
dfa <- (G2w/(G2w - 1)) * (N2w - 1)/roundbinge$df.residual
college_c_vcov <- dfa * vcovHC(roundbinge, type = "HC0", cluster = "group", adjust = T)
rbinge <- coeftest(roundbinge, vcov = college_c_vcov)

dfa <- (G2w/(G2w - 1)) * (N2w - 1)/tourneybinge$df.residual
college_c_vcov <- dfa * vcovHC(tourneybinge, type = "HC0", cluster = "group", adjust = T)
tbinge <- coeftest(tourneybinge, vcov = college_c_vcov)

dfa <- (G2w/(G2w - 1)) * (N2w - 1)/percentbinge$df.residual
college_c_vcov <- dfa * vcovHC(percentbinge, type = "HC0", cluster = "group", adjust = T)
pbinge <- coeftest(percentbinge, vcov = college_c_vcov)

# Clustered and Heteroskedasticity corrected results: DRINKING VOLUME
dfa <- (Gm/(Gm - 1)) * (Nm - 1)/roundvol$df.residual
college_c_vcov <- dfa * vcovHC(roundvol, type = "HC0", cluster = "group", adjust = T)
rvol <- coeftest(roundvol, vcov = college_c_vcov)

dfa <- (Gm/(Gm - 1)) * (Nm - 1)/tourneyvol$df.residual
college_c_vcov <- dfa * vcovHC(tourneyvol, type = "HC0", cluster = "group", adjust = T)
tvol <- coeftest(tourneyvol, vcov = college_c_vcov)

dfa <- (Gm/(Gm - 1)) * (Nm - 1)/percentvol$df.residual
college_c_vcov <- dfa * vcovHC(percentvol, type = "HC0", cluster = "group", adjust = T)
pvol <- coeftest(percentvol, vcov = college_c_vcov)

# Clustered and Heteroskedasticity corrected results: MALES
dfa <- (mG2w/(mG2w - 1)) * (mN2w - 1)/roundbingem$df.residual
college_c_vcov <- dfa * vcovHC(roundbingem, type = "HC0", cluster = "group", adjust = T)
rbingem <- coeftest(roundbingem, vcov = college_c_vcov)

dfa <- (mGm/(mGm - 1)) * (mNm - 1)/rounddrinkm$df.residual
college_c_vcov <- dfa * vcovHC(rounddrinkm, type = "HC0", cluster = "group", adjust = T)
rdrinkm <- coeftest(rounddrinkm, vcov = college_c_vcov)

dfa <- (mGm/(mGm - 1)) * (mNm - 1)/roundvolm$df.residual
college_c_vcov <- dfa * vcovHC(roundvolm, type = "HC0", cluster = "group", adjust = T)
rvolm <- coeftest(roundvolm, vcov = college_c_vcov)

# Clustered and Heteroskedasticity corrected results: FEMALES
dfa <- (fG2w/(fG2w - 1)) * (fN2w - 1)/roundbingef$df.residual
college_c_vcov <- dfa * vcovHC(roundbingef, type = "HC0", cluster = "group", adjust = T)
rbingef <- coeftest(roundbingef, vcov = college_c_vcov)

dfa <- (fGm/(fGm - 1)) * (fNm - 1)/rounddrinkf$df.residual
college_c_vcov <- dfa * vcovHC(rounddrinkf, type = "HC0", cluster = "group", adjust = T)
rdrinkf <- coeftest(rounddrinkf, vcov = college_c_vcov)

dfa <- (fGm/(fGm - 1)) * (fNm - 1)/roundvolf$df.residual
college_c_vcov <- dfa * vcovHC(roundvolf, type = "HC0", cluster = "group", adjust = T)
rvolf <- coeftest(roundvolf, vcov = college_c_vcov)

# Clustered and Heteroskedasticity corrected results: MARIJUANA
dfa <- (Gm/(Gm - 1)) * (Nm - 1)/roundpot$df.residual
college_c_vcov <- dfa * vcovHC(roundpot, type = "HC0", cluster = "group", adjust = T)
rpot <- coeftest(roundpot, vcov = college_c_vcov)

dfa <- (Gm/(Gm - 1)) * (Nm - 1)/tourneypot$df.residual
college_c_vcov <- dfa * vcovHC(tourneypot, type = "HC0", cluster = "group", adjust = T)
tpot <- coeftest(tourneypot, vcov = college_c_vcov)

dfa <- (Gm/(Gm - 1)) * (Nm - 1)/percentpot$df.residual
college_c_vcov <- dfa * vcovHC(percentpot, type = "HC0", cluster = "group", adjust = T)
ppot <- coeftest(percentpot, vcov = college_c_vcov)


# ##### PRINT REGRESSION TABLES
# 
# capture.output(
#   
#   stargazer(rbinge, rdrink, rvol,
#             style = "qje",
#             label = "reground",
#             model.numbers = FALSE,
#             title = "Effects on Drinking of Rounds in NCAA Tournament",
#             dep.var.labels = c("Binge Occasions", "Drink Last Month", "Number of Drinks"),
#             keep = c("Rounds", "Percent", "greek", "married", "female", "frosh", "soph", "jun", "sen", "white", "black", "asian", "other", "gpa"),
#             order = c("Rounds", "Percent", "greek", "married", "female", "frosh", "soph", "jun", "sen", "white", "black", "asian", "other", "gpa"),
#             covariate.labels = c("Rounds", "Percent", "Greek", "Married", "Female", "Freshman", "Sophomore", 
#                                  "Junior", "Senior", "White", "Black", "Asian", "Other", "GPA"),
#             keep.stat = c("n"),
#             no.space = TRUE,
#             notes.append = TRUE,
#             notes = "Other controls included in regression were indicators}  \\\\ 
#             & \\multicolumn{3}{r}{for ages between 17 and 24, athletic conference, school and year."
#   ),
#   
#   stargazer(tbinge, tdrink, tvol,
#             style = "qje",
#             label = "regtourney",
#             model.numbers = FALSE,
#             title = "Effects on Drinking of NCAA Tournament Participation",
#             dep.var.labels = c("Binge Occasions", "Drink Last Month", "Number of Drinks"),
#             keep = c("Tourney", "Percent", "greek", "married", "female", "frosh", "soph", "jun", "sen", "white", "black", "asian", "other", "gpa"),
#             order = c("Tourney", "Percent", "greek", "married", "female", "frosh", "soph", "jun", "sen", "white", "black", "asian", "other", "gpa"),
#             covariate.labels = c("Rounds", "Percent", "Greek", "Married", "Female", "Freshman", "Sophomore", 
#                                  "Junior", "Senior", "White", "Black", "Asian", "Other", "GPA"),
#             keep.stat = c("n"),
#             no.space = TRUE,
#             notes.append = TRUE,
#             notes = "Other controls included in regression were indicators}  \\\\ 
#             & \\multicolumn{3}{r}{for ages between 17 and 24, athletic conference, school and year."
#   ),
#   
#   #   stargazer(pbinge, pdrink, pvol,
#   #             style = "qje",
#   #             label = "regwp",
#   #             model.numbers = FALSE,
#   #             title = "Effects on Drinking of Season Win Percentage",
#   #             dep.var.labels = c("Binge Occasions", "Drink Last Month", "Number of Drinks"),
#   #             keep = c("Percent:post2w", "Percent:postm", "Percent:after", "Percent", "post2w", "postm", "after", "greek", "married", "female", "frosh", "soph", "jun", "sen", "white", "black", "asian", "other", "gpa"),
#   #             order = c("Percent:post2w", "Percent:postm", "Percent:after", "Percent", "post2w", "postm", "after", "greek", "married", "female", "frosh", "soph", "jun", "sen", "white", "black", "asian", "other", "gpa"),
#   #             covariate.labels = c("Win %*During Tourney (Binge)", "Win %*During Tourney (Other)", "Win %*After Tourney", "Win %", "During Tourney (Binge)", "During Tourney (Other)", 
#   #                                  "After Tourney", "Greek", "Married", "Female",
#   #                                  "Freshman", "Spohomore", "Junior", 
#   #                                  "Senior", "White", "Black", "Asian", "Other", "GPA"),
#   #             keep.stat = c("n"),
#   #             no.space = TRUE,
#   #             notes.append = TRUE,
#   #             notes = "Other controls included in regression were indicators}  \\\\ 
#   #             & \\multicolumn{3}{r}{for ages between 17 and 24, athletic conference, school and year."
#   #             ),
#   #   
#   #   stargazer(roundbingeath, tourneybingeath, percentbingeath,
#   #             style = "qje",
#   #             label = "regath",
#   #             model.numbers = FALSE,
#   #             title = "Effects of NCAA Tournament Participation on Athlete Binge Drinking",
#   #             dep.var.labels = c("Binges Last Month","Binges Last Month","Binges Last Month"),
#   #             keep = c("Rounds:post7", "Rounds", "Tourney:post7", "Tourney", "Percent:post7", "Percent", "post7", "greek", "religious", "pub_pri", "married", "female", "frosh", "soph", "jun", "sen", "white", "black", "asian", "other", "gpa"),
#   #             order = c("Rounds:post7", "Rounds", "Tourney:post7", "Tourney","Percent:post7", "Percent","post7", "greek", "married", "female", "frosh", "soph", "jun", "sen", "white", "black", "asian", "other", "gpa", "religious", "pub_pri"),
#   #             covariate.labels = c("Rounds*NCAA", "Rounds", "Tourney*NCAA", "Tourney", "Percent*NCAA", "Percent", "NCAA", 
#   #                                  "Greek", "Married", "Female",
#   #                                  "Freshman", "Spohomore", "Junior", 
#   #                                  "Senior", "White", "Black", "Asian", "Other", "GPA", "Religious Inst.", "Private"),
#   #             keep.stat = c("n"),
#   #             no.space = TRUE,
#   #             notes.append = TRUE,
#   #             notes = "Other controls included in regression were}  \\\\ 
#   #             & \\multicolumn{3}{r}{indicators for ages between 17 and 24, school and year."
#   #   ),
#   #   
#   stargazer(rbingem, rdrinkm, rvolm,
#             style = "qje",
#             label = "regmale",
#             model.numbers = FALSE,
#             title = "Effects on Drinking of Rounds in NCAA Tournament on Males",
#             dep.var.labels = c("Binge Occasions", "Drink Last Month", "Number of Drinks"),
#             keep = c("Rounds", "Percent", "greek", "married", "frosh", "soph", "jun", "sen", "white", "black", "asian", "other", "gpa"),
#             order = c("Rounds", "Percent", "greek", "married", "frosh", "soph", "jun", "sen", "white", "black", "asian", "other", "gpa"),
#             covariate.labels = c("Rounds", "Percent", "Greek", "Married", "Freshman", "Sophomore", 
#                                  "Junior", "Senior", "White", "Black", "Asian", "Other", "GPA"),
#             keep.stat = c("n"),
#             no.space = TRUE,
#             notes.append = TRUE,
#             notes = "Other controls included in regression were indicators}  \\\\ 
#             & \\multicolumn{3}{r}{for ages between 17 and 24, athletic conference, school and year."
#   ),
#   
#   stargazer(rbingef, rdrinkf, rvolf,
#             style = "qje",
#             label = "regfemale",
#             model.numbers = FALSE,
#             title = "Effects on Drinking of Rounds in NCAA Tournament on Females",
#             dep.var.labels = c("Binge Occasions", "Drink Last Month", "Number of Drinks"),
#             keep = c("Rounds", "Percent", "greek", "married", "frosh", "soph", "jun", "sen", "white", "black", "asian", "other", "gpa"),
#             order = c("Rounds", "Percent", "greek", "married", "frosh", "soph", "jun", "sen", "white", "black", "asian", "other", "gpa"),
#             covariate.labels = c("Rounds", "Percent", "Greek", "Married", "Freshman", "Sophomore", 
#                      "Junior", "Senior", "White", "Black", "Asian", "Other", "GPA"),
#             keep.stat = c("n"),
#             no.space = TRUE,
#             notes.append = TRUE,
#             notes = "Other controls included in regression were indicators}  \\\\ 
#             & \\multicolumn{3}{r}{for ages between 17 and 24, athletic conference, school and year."
#   ),
#   
#   
#   #   stargazer(rpot, tpot, ppot,
#   #             style = "qje",
#   #             label = "regpot",
#   #             model.numbers = FALSE,
#   #             title = "Effects of NCAA Tournament Participation on Marijuana Use",
#   #             dep.var.labels = c("Use Marijuana Last Month?"),
#   #             keep = c("Rounds:postm", "Rounds", "Tourney:postm", "Tourney", "Percent:postm", "Percent", "postm", "after", "greek", "married", "female", "frosh", "soph", "jun", "sen", "white", "black", "asian", "other", "gpa"),
#   #             order = c("Rounds:postm", "Rounds", "Tourney:postm", "Tourney","Percent:postm", "Percent","postm", "after", "greek", "married", "female", "frosh", "soph", "jun", "sen", "white", "black", "asian", "other", "gpa"),
#   #             covariate.labels = c("Rounds*During Tourney", "Rounds", "Tourney*During Tourney", "Tourney", "Percent*During Tourney", "Percent", "During Tourney", 
#   #                                  "After Tourney",  "Greek", "Married", "Female",
#   #                                  "Freshman", "Spohomore", "Junior", 
#   #                                  "Senior", "White", "Black", "Asian", "Other", "GPA"),
#   #             keep.stat = c("n"),
#   #             no.space = TRUE,
#   #             notes.append = TRUE,
#   #             notes = "Other controls included in regression were indicators}  \\\\ 
#   #             & \\multicolumn{3}{r}{for ages between 17 and 24, athletic conference, school and year."
#   #             )
#   #   
#   
#   , file = "/Users/dusty/Documents/Current Research/Drinking and College Sports/Drafts/Third Draft/TablesDuring.tex"
#   )
