#########                                                                       ##########
######### THIS SCRIPT CORRECTS FOR THE ENDOGENEITY OF GPA AND GREEK MEMBERSHIP  ##########
#########                                                                       ##########

###########
### SIMULTANEOUS MODELS 
###########

library("systemfit", lib.loc="/Library/Frameworks/R.framework/Versions/3.1/Resources/library")

##### SIMULTANEOUS BINGE MODEL #####

eq1 <- num5pl ~ Percent*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian +  gpa
eq2 <- greek ~ Percent*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + num5pl + religious + pub_pri + married + female + frosh + soph + jun + sen + college + white + black + asian + studentorg + happy
eq3 <- gpa ~ Percent*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + num5pl + religious + pub_pri + married + female + frosh + soph + jun + sen + college + white + black + asian + studentorg + happy

system <- list(Binge = eq1, Greek = eq2, GPA = eq3)

fitbinge <- systemfit(system, data = data2, method = "SUR")


##### SIMULTANEOUS DRINK MODEL #####

eq1 <- drinklast30 ~ Percent*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + gpa
eq2 <- greek ~ Percent*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + drinklast30 + religious + pub_pri + married + female + frosh + soph + jun + sen + college + white + black + asian + studentorg + happy
eq3 <- gpa ~ Percent*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + drinklast30 + religious + pub_pri + married + female + frosh + soph + jun + sen + college + white + black + asian + studentorg + happy

system <- list(Drink = eq1, Greek = eq2, GPA = eq3)

fitdrink <- systemfit(system, data = data2, method = "SUR")

##### SIMULTANEOUS VOLUME MODEL #####

eq1 <- vol30 ~ Percent*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + gpa
eq2 <- greek ~ Percent*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + drinklast30 + religious + pub_pri + married + female + frosh + soph + jun + sen + college + white + black + asian + studentorg + happy
eq3 <- gpa ~ Percent*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + drinklast30 + religious + pub_pri + married + female + frosh + soph + jun + sen + college + white + black + asian + studentorg + happy

system <- list(Drink = eq1, Greek = eq2, GPA = eq3)

fitvolume <- systemfit(system, data = data2, method = "SUR")

capture.output(
  texreg(fitbinge,
         caption = "The Effect of Win Percent on Binges in Past Month",
         label = "percentbingesur",
         custom.model.names = c("Binges Past Month", "Greek Membership", "GPA"),
         omit.coef = "(college)|(a1)|(a2)|(year)",
         custom.note = "Other Controls included were age indicators, college indicators, and year of survey response.",
         custom.coef.names = c("Intercept", "Win Percent", "During Tournament", NA, NA, NA, NA, NA,
                               NA, NA, NA, "Greek Membership",
                               "Religious Inst.", "Private School", "Married", "Female", NA, NA, NA,
                               "Freshman", "Sophomore", "Junior", "Senior", 
                               NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, 
                               NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,
                               "White", "Black", "Asian", "GPA", "Interaction of Win % and During",
                               "Binges in Past Month", "Member of Student Org.", "Happiness"),
         reorder.coef = c(17,2,3,4,7,8,9,10,11,12,13,14,15,16,18,19,20,5,6,1),
         caption.above = TRUE,
         digits = 4,
         stars = c(.01,.05,.1)
  ),
  texreg(fitdrink,
         caption = "The Effect of Win Percent on Drinking in Past Month",
         label = "percentdrinksur",
         custom.model.names = c("Drink Past Month", "Greek Membership", "GPA"),
         omit.coef = "(college)|(a1)|(a2)|(year)",
         custom.note = "Other Controls included were age indicators, college indicators, and year of survey response.",
         custom.coef.names = c("Intercept", "Win Percent", "During Tournament", NA, NA, NA, NA, NA,
                               NA, NA, NA, "Greek Membership",
                               "Religious Inst.", "Private School", "Married", "Female", NA, NA, NA,
                               "Freshman", "Sophomore", "Junior", "Senior", 
                               NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, 
                               NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,
                               "White", "Black", "Asian", "GPA", "Interaction of Win % and During",
                               "Binges in Past Month", "Member of Student Org.", "Happiness"),
         reorder.coef = c(17,2,3,4,7,8,9,10,11,12,13,14,15,16,18,19,20,5,6,1),
         caption.above = TRUE,
         digits = 4,
         stars = c(.01,.05,.1)
  ),
  texreg(fitvolume,
         caption = "The Effect of Win Percent on Number of Drinks in Past Month",
         label = "percentvolsur",
         custom.model.names = c("Number of Drinks in Past Month", "Greek Membership", "GPA"),
         omit.coef = "(college)|(a1)|(a2)|(year)",
         custom.note = "Other Controls included were age indicators, college indicators, and year of survey response.",
         custom.coef.names = c("Intercept", "Win Percent", "During Tournament", NA, NA, NA, NA, NA,
                               NA, NA, NA, "Greek Membership",
                               "Religious Inst.", "Private School", "Married", "Female", NA, NA, NA,
                               "Freshman", "Sophomore", "Junior", "Senior", 
                               NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, 
                               NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,
                               "White", "Black", "Asian", "GPA", "Interaction of Win % and During",
                               "Binges in Past Month", "Member of Student Org.", "Happiness"),
         reorder.coef = c(17,2,3,4,7,8,9,10,11,12,13,14,15,16,18,19,20,5,6,1),
         caption.above = TRUE,
         digits = 4,
         stars = c(.01,.05,.1)
  ),
  
  , file = "/Users/dusty/Box Sync/Research/Drinking and College Sports/Drafts/SecondDraft Sports/TablesPercent.tex"
)


