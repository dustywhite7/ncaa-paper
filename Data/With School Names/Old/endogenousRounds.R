#########                                                                       ##########
######### THIS SCRIPT CORRECTS FOR THE ENDOGENEITY OF GPA AND GREEK MEMBERSHIP  ##########
#########                                                                       ##########

###########
### SIMULTANEOUS MODELS 
###########

library("systemfit", lib.loc="/Library/Frameworks/R.framework/Versions/3.1/Resources/library")

# starprep=function(s){
#   s2 <- signif(s,6)
#   for (i in 1:nrow(s)) {
#     if (is.finite(s[i,4])){
#       if (s[i,4]<=.1) {
#         s2[i,1] = paste(signif(s[i,1],6),"*", sep = "")
#       }
#       if (s[i,4]<=.05) {
#         s2[i,1] = paste(signif(s[i,1],6),"**", sep = "")
#       }
#       if (s[i,4]<=.01) {
#         s2[i,1] = paste(signif(s[i,1],6),"***", sep = "")
#       }
#     }
#   }
#   return(s2)
# }

##### SIMULTANEOUS BINGE MODEL #####

eq1 <- num5pl ~ Rounds*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian +  gpa
eq2 <- greek ~ Rounds*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + num5pl + religious + pub_pri + married + female + frosh + soph + jun + sen + college + white + black + asian + studentorg + happy
eq3 <- gpa ~ Rounds*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + num5pl + religious + pub_pri + married + female + frosh + soph + jun + sen + college + white + black + asian + studentorg + happy

system <- list(Binge = eq1, Greek = eq2, GPA = eq3)

fitbinge <- systemfit(system, data = data2, method = "SUR")
# summary(fitbinge)
# 
# fitbinge1 <- starprep(summary(fitbinge$eq[[1]])$coefficients)
# fitbinge2 <- starprep(summary(fitbinge$eq[[2]])$coefficients)
# fitbinge3 <- starprep(summary(fitbinge$eq[[3]])$coefficients)



##### SIMULTANEOUS DRINK MODEL #####

eq1 <- drinklast30 ~ Rounds*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + gpa
eq2 <- greek ~ Rounds*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + drinklast30 + religious + pub_pri + married + female + frosh + soph + jun + sen + college + white + black + asian + studentorg + happy
eq3 <- gpa ~ Rounds*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + drinklast30 + religious + pub_pri + married + female + frosh + soph + jun + sen + college + white + black + asian + studentorg + happy

system <- list(Drink = eq1, Greek = eq2, GPA = eq3)

fitdrink <- systemfit(system, data = data2, method = "SUR")
# summary(fitdrink)

# fitdrink1 <- starprep(summary(fitbinge$eq[[1]])$coefficients)
# fitdrink2 <- starprep(summary(fitbinge$eq[[2]])$coefficients)
# fitdrink3 <- starprep(summary(fitbinge$eq[[3]])$coefficients)


##### SIMULTANEOUS VOLUME MODEL #####

eq1 <- vol30 ~ Rounds*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + gpa
eq2 <- greek ~ Rounds*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + drinklast30 + religious + pub_pri + married + female + frosh + soph + jun + sen + college + white + black + asian + studentorg + happy
eq3 <- gpa ~ Rounds*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + drinklast30 + religious + pub_pri + married + female + frosh + soph + jun + sen + college + white + black + asian + studentorg + happy

system <- list(Drink = eq1, Greek = eq2, GPA = eq3)

fitvolume <- systemfit(system, data = data2, method = "SUR")
# summary(fitvolume)

# fitvol1 <- starprep(summary(fitbinge$eq[[1]])$coefficients)
# fitvol2 <- starprep(summary(fitbinge$eq[[2]])$coefficients)
# fitvol3 <- starprep(summary(fitbinge$eq[[3]])$coefficients)

# ###########
# ### TWO-STAGE RESIDUAL INCLUSION MODELS 
# ###########
# 
# cor.test(data2$num5pl, as.integer(data2$studentorg))
# cor.test(data2$drinklast30, as.integer(data2$studentorg))
# cor.test(data2$vol30, as.integer(data2$studentorg))
# cor.test(data2$greek, as.integer(data2$studentorg))
# 
# cor.test(data2$num5pl, data2$happy)
# cor.test(data2$drinklast30, data2$happy)
# cor.test(data2$vol30, data2$happy)
# cor.test(data2$gpa, data2$happy)
# 
# data2$greekres <- residuals(lm(greek ~ a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + num5pl + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + studentorg, na.action = na.exclude, data = data2))
# data2$gpares <- residuals(lm(gpa ~ a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + vol30 + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + happy, na.action = na.exclude, data = data2))
# 
# binge2stage <- lm(num5pl ~ Rounds*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greekres + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpares, data = data2)
# summary(binge2stage)

capture.output(
  texreg(fitbinge,
         caption = "The Effect of Rounds of Participation in NCAA Tournament on Binges in Past Month",
         label = "roundbingesur",
         custom.model.names = c("Binges Past Month", "Greek Membership", "GPA"),
         omit.coef = "(college)|(a1)|(a2)|(year)",
         custom.note = "Other Controls included were age indicators, college indicators, and year of survey response.",
         custom.coef.names = c("Intercept", "Rounds in NCAA Tournament", "During Tournament", NA, NA, NA, NA, NA,
                               NA, NA, NA, "Greek Membership",
                               "Religious Inst.", "Private School", "Married", "Female", NA, NA, NA,
                               "Freshman", "Sophomore", "Junior", "Senior", 
                               NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, 
                               NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,
                               "White", "Black", "Asian", "GPA", "Interaction of Rounds and During",
                               "Binges in Past Month", "Member of Student Org.", "Happiness"),
         reorder.coef = c(17,2,3,4,7,8,9,10,11,12,13,14,15,16,18,19,20,5,6,1),
         caption.above = TRUE,
         digits = 4,
         stars = c(.01,.05,.1)
  ),
  texreg(fitdrink,
         caption = "The Effect of Rounds of Participation in NCAA Tournament on Drinking in Past Month",
         label = "rounddrinksur",
         custom.model.names = c("Drink Past Month", "Greek Membership", "GPA"),
         omit.coef = "(college)|(a1)|(a2)|(year)",
         custom.note = "Other Controls included were age indicators, college indicators, and year of survey response.",
         custom.coef.names = c("Intercept", "Rounds in NCAA Tournament", "During Tournament", NA, NA, NA, NA, NA,
                               NA, NA, NA, "Greek Membership",
                               "Religious Inst.", "Private School", "Married", "Female", NA, NA, NA,
                               "Freshman", "Sophomore", "Junior", "Senior", 
                               NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, 
                               NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,
                               "White", "Black", "Asian", "GPA", "Interaction of Rounds and During",
                               "Binges in Past Month", "Member of Student Org.", "Happiness"),
         reorder.coef = c(17,2,3,4,7,8,9,10,11,12,13,14,15,16,18,19,20,5,6,1),
         caption.above = TRUE,
         digits = 4,
         stars = c(.01,.05,.1)
  ),
  texreg(fitvolume,
         caption = "The Effect of Rounds of Participation in NCAA Tournament on Number of Drinks in Past Month",
         label = "roundvolsur",
         custom.model.names = c("Number of Drinks in Past Month", "Greek Membership", "GPA"),
         omit.coef = "(college)|(a1)|(a2)|(year)",
         custom.note = "Other Controls included were age indicators, college indicators, and year of survey response.",
         custom.coef.names = c("Intercept", "Rounds in NCAA Tournament", "During Tournament", NA, NA, NA, NA, NA,
                               NA, NA, NA, "Greek Membership",
                               "Religious Inst.", "Private School", "Married", "Female", NA, NA, NA,
                               "Freshman", "Sophomore", "Junior", "Senior", 
                               NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, 
                               NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,
                               "White", "Black", "Asian", "GPA", "Interaction of Rounds and During",
                               "Binges in Past Month", "Member of Student Org.", "Happiness"),
         reorder.coef = c(17,2,3,4,7,8,9,10,11,12,13,14,15,16,18,19,20,5,6,1),
         caption.above = TRUE,
         digits = 4,
         stars = c(.01,.05,.1)
  ),
  
  , file = "/Users/dusty/Box Sync/Research/Drinking and College Sports/Drafts/SecondDraft Sports/TablesRounds.tex"
)
