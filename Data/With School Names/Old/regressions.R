library("stargazer", lib.loc="/Library/Frameworks/R.framework/Versions/3.1/Resources/library")
library("systemfit", lib.loc="/Library/Frameworks/R.framework/Versions/3.1/Resources/library")


roundbinge <- lm(num5pl ~ Rounds*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa, data=data2)
tourneybinge <- lm(num5pl ~ Tourney*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa, data=data2)
percentbinge <- lm(num5pl ~ Percent*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa, data=data2)

roundvol <- lm(vol30 ~ Rounds*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa, data=data2)
tourneyvol <- lm(vol30 ~ Tourney*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa, data=data2)
percentvol <- lm(vol30 ~ Percent*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa, data=data2)

rounddrink <- lm(drinklast30 ~ Rounds*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa, data=data2)
tourneydrink <- lm(drinklast30 ~ Tourney*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa, data=data2)
percentdrink <- lm(drinklast30 ~ Percent*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa, data=data2)


##### MALES ONLY #####
roundbingem <- lm(num5pl ~ Rounds*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa, data=males)
rounddrinkm <- lm(drinklast30 ~ Rounds*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa, data=males)
roundvolm <- lm(vol30 ~ Rounds*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa, data=males)

##### FEMALES ONLY #####
roundbingef <- lm(num5pl ~ Rounds*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa, data=females)
rounddrinkf <- lm(drinklast30 ~ Rounds*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa, data=females)
roundvolf <- lm(vol30 ~ Rounds*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa, data=females)

##### MARIJUANA USE - ROBUSTNESS CHECK #####
roundpot <- lm(marijuana ~ Rounds*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa, data=data2)
tourneypot <- lm(marijuana ~ Tourney*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa, data=data2)
percentpot <- lm(marijuana ~ Percent*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa, data=data2)

# roundbin <- lm(binged ~ Rounds*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa, data=data2)
# tourneybin <- lm(binged ~ Tourney*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa, data=data2)
# percentbin <- lm(binged ~ Percent*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa, data=data2)

roundbingeath <- lm(num5pl ~ Rounds*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa, data=athletes)
tourneybingeath <- lm(num5pl ~ Tourney*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa, data=athletes)
percentbingeath <- lm(num5pl ~ Percent*post7 + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + religious + pub_pri + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + college + white + black + asian + other + gpa, data=athletes)

# ###### SUMMARY STATISTICS
# stargazer(data2,
#           style = "qje",
#           title = "Summary Statistics",
#           omit.summary.stat = "n",
#           no.space = TRUE
# )

##### PRINT REGRESSION TABLES

capture.output(

  stargazer(roundbinge, rounddrink, roundvol,
            style = "qje",
            label = "reground",
            model.numbers = FALSE,
            title = "Effects on Drinking of Rounds in NCAA Tournament",
            dep.var.labels = c("Binge Occasions", "Drink Last Month", "Number of Drinks"),
            keep = c("Rounds:post7", "Percent", "post7", "greek", "religious", "pub_pri", "married", "female", "frosh", "soph", "jun", "sen", "white", "black", "asian", "other", "gpa"),
            order = c("Rounds:post7", "Percent", "post7", "greek", "married", "female", "frosh", "soph", "jun", "sen", "white", "black", "asian", "other", "gpa", "religious", "pub_pri"),
            covariate.labels = c("Rounds*NCAA", "Percent", "NCAA", 
                                 "Greek", "Married", "Female",
                                 "Freshman", "Spohomore", "Junior", 
                                 "Senior", "White", "Black", "Asian", "Other", "GPA", "Religious Inst.", "Private"),
            keep.stat = c("n"),
            no.space = TRUE,
            notes.append = TRUE,
            notes = "Other controls included in regression were}  \\\\ 
            & \\multicolumn{3}{r}{indicators for ages between 17 and 24, and year."
  ),
  
  stargazer(tourneybinge, tourneydrink, tourneyvol,
            style = "qje",
            label = "regtourney",
            model.numbers = FALSE,
            title = "Effects on Drinking of NCAA Tournament Participation",
            dep.var.labels = c("Binge Occasions", "Drink Last Month", "Number of Drinks"),
            keep = c("Tourney:post7", "Percent", "post7", "greek", "religious", "pub_pri", "married", "female", "frosh", "soph", "jun", "sen", "white", "black", "asian", "other", "gpa"),
            order = c("Tourney:post7", "Percent", "post7", "greek", "married", "female", "frosh", "soph", "jun", "sen", "white", "black", "asian", "other", "gpa", "religious", "pub_pri"),
            covariate.labels = c("Tourney*NCAA", "Percent", "NCAA",
                                 "Greek", "Married", "Female",
                                 "Freshman", "Spohomore", "Junior", 
                                 "Senior", "White", "Black", "Asian", "Other", "GPA", "Religious Inst.", "Private"),
            keep.stat = c("n"),
            no.space = TRUE,
            notes.append = TRUE,
            notes = "Other controls included in regression were}  \\\\ 
                  & \\multicolumn{3}{r}{indicators for ages between 17 and 24, and year."
  ),
  
stargazer(percentbinge, percentdrink, percentvol,
          style = "qje",
          label = "regwp",
          model.numbers = FALSE,
          title = "Effects on Drinking of Season Win Percentage",
          dep.var.labels = c("Binge Occasions", "Drink Last Month", "Number of Drinks"),
          keep = c("Percent:post7", "Percent", "post7", "greek", "religious", "pub_pri", "married", "female", "frosh", "soph", "jun", "sen", "white", "black", "asian", "other", "gpa"),
          order = c("Percent:post7", "Percent", "post7", "greek", "married", "female", "frosh", "soph", "jun", "sen", "white", "black", "asian", "other", "gpa", "religious", "pub_pri"),
          covariate.labels = c("Percent*NCAA", "Percent", "NCAA",
                               "Greek", "Married", "Female",
                               "Freshman", "Spohomore", "Junior", 
                               "Senior", "White", "Black", "Asian", "Other", "GPA", "Religious Inst.", "Private"),
          keep.stat = c("n"),
          no.space = TRUE,
          notes.append = TRUE,
          notes = "Other controls included in regression were}  \\\\ 
                  & \\multicolumn{3}{r}{indicators for ages between 17 and 24, and year."
          ),

stargazer(roundbingeath, tourneybingeath, percentbingeath,
          style = "qje",
          label = "regath",
          model.numbers = FALSE,
          title = "Effects of NCAA Tournament Participation on Athlete Binge Drinking",
          dep.var.labels = c("Binges Last Month","Binges Last Month","Binges Last Month"),
          keep = c("Rounds:post7", "Rounds", "Tourney:post7", "Tourney", "Percent:post7", "Percent", "post7", "greek", "religious", "pub_pri", "married", "female", "frosh", "soph", "jun", "sen", "white", "black", "asian", "other", "gpa"),
          order = c("Rounds:post7", "Rounds", "Tourney:post7", "Tourney","Percent:post7", "Percent","post7", "greek", "married", "female", "frosh", "soph", "jun", "sen", "white", "black", "asian", "other", "gpa", "religious", "pub_pri"),
          covariate.labels = c("Rounds*NCAA", "Rounds", "Tourney*NCAA", "Tourney", "Percent*NCAA", "Percent", "NCAA", 
                               "Greek", "Married", "Female",
                               "Freshman", "Spohomore", "Junior", 
                               "Senior", "White", "Black", "Asian", "Other", "GPA", "Religious Inst.", "Private"),
          keep.stat = c("n"),
          no.space = TRUE,
          notes.append = TRUE,
          notes = "Other controls included in regression were}  \\\\ 
                  & \\multicolumn{3}{r}{indicators for ages between 17 and 24, and year."
          ),

stargazer(roundbingem, rounddrinkm, roundvolm,
          style = "qje",
          label = "regmale",
          model.numbers = FALSE,
          title = "Effects on Drinking of Rounds in NCAA Tournament on Males",
          dep.var.labels = c("Binge Occasions", "Drink Last Month", "Number of Drinks"),
          keep = c("Rounds:post7", "Percent", "post7", "greek", "religious", "pub_pri", "married", "frosh", "soph", "jun", "sen", "white", "black", "asian", "other", "gpa"),
          order = c("Rounds:post7", "Percent", "post7", "greek", "married", "frosh", "soph", "jun", "sen", "white", "black", "asian", "other", "gpa", "religious", "pub_pri"),
          covariate.labels = c("Rounds*NCAA", "Percent", "NCAA", 
                               "Greek", "Married",
                               "Freshman", "Spohomore", "Junior", 
                               "Senior", "White", "Black", "Asian", "Other", "GPA", "Religious Inst.", "Private"),
          keep.stat = c("n"),
          no.space = TRUE,
          notes.append = TRUE,
          notes = "Other controls included in regression were}  \\\\ 
                  & \\multicolumn{3}{r}{indicators for ages between 17 and 24, and year."
          ),

stargazer(roundbingef, rounddrinkf, roundvolf,
          style = "qje",
          label = "regfemale",
          model.numbers = FALSE,
          title = "Effects on Drinking of Rounds in NCAA Tournament on Females",
          dep.var.labels = c("Binge Occasions", "Drink Last Month", "Number of Drinks"),
          keep = c("Rounds:post7", "Percent", "post7", "greek", "religious", "pub_pri", "married", "frosh", "soph", "jun", "sen", "white", "black", "asian", "other", "gpa"),
          order = c("Rounds:post7", "Percent", "post7", "greek", "married", "frosh", "soph", "jun", "sen", "white", "black", "asian", "other", "gpa", "religious", "pub_pri"),
          covariate.labels = c("Rounds*NCAA", "Percent", "NCAA", 
                               "Greek", "Married", 
                               "Freshman", "Spohomore", "Junior", 
                               "Senior", "White", "Black", "Asian", "Other", "GPA", "Religious Inst.", "Private"),
          keep.stat = c("n"),
          no.space = TRUE,
          notes.append = TRUE,
          notes = "Other controls included in regression were}  \\\\ 
                  & \\multicolumn{3}{r}{indicators for ages between 17 and 24, and year."
          ),


stargazer(roundpot, tourneypot, percentpot,
          style = "qje",
          label = "regpot",
          model.numbers = FALSE,
          title = "Effects of NCAA Tournament Participation on Marijuana Use",
          dep.var.labels = c("Use Marijuana Last Month?"),
          keep = c("Rounds:post7", "Rounds", "Tourney:post7", "Tourney", "Percent:post7", "Percent", "post7", "greek", "religious", "pub_pri", "married", "female", "frosh", "soph", "jun", "sen", "white", "black", "asian", "other", "gpa"),
          order = c("Rounds:post7", "Rounds", "Tourney:post7", "Tourney","Percent:post7", "Percent","post7", "greek", "married", "female", "frosh", "soph", "jun", "sen", "white", "black", "asian", "other", "gpa", "religious", "pub_pri"),
          covariate.labels = c("Rounds*NCAA", "Rounds", "Tourney*NCAA", "Tourney", "Percent*NCAA", "Percent", "NCAA", 
                               "Greek", "Married", "Female",
                               "Freshman", "Spohomore", "Junior", 
                               "Senior", "White", "Black", "Asian", "Other", "GPA", "Religious Inst.", "Private"),
          keep.stat = c("n"),
          no.space = TRUE,
          notes.append = TRUE,
          notes = "Other controls included in regression were}  \\\\ 
                  & \\multicolumn{3}{r}{indicators for ages between 17 and 24, and year."
)


, file = "/Users/dusty/Documents/Current Research/Drinking and College Sports/Drafts/Third Draft/Tables3.tex"
)