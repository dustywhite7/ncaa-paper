library("stargazer")

data2$hyptr <- ((data2$daystart2w < 96 & data2$daystart2w > 70) | (data2$dayend2w > 70 & data2$dayend2w < 96))

datasum <- subset(data2, select=c("num5pl", "drinklast30", "vol30", "exRounds", "exTourney", "Percent",
                                  "age",
                                  "greek", "married", "female", "year1993", "year1997", 
                                  "year1999", "year2001", "frosh", "soph", "jun", "sen", "white", "black", 
                                  "asian", "other", "gpa", "college", "athconf", "Tourney", "hyptr"))
datasum <- na.omit(datasum)
tourneyno <- subset(datasum, (datasum$exTourney==0 & datasum$Tourney==1))
tourneyview <- subset(datasum, (datasum$exTourney==1 & datasum$Tourney==1))
tourneyno <- subset(tourneyno, select=c("num5pl", "drinklast30", "vol30", "exRounds", "exTourney", "Percent",
                                    "age",
                                    "greek", "married", "female", "year1993", "year1997", 
                                    "year1999", "year2001", "frosh", "soph", "jun", "sen", "white", "black", 
                                    "asian", "other", "gpa"))
tourneyview <- subset(tourneyview, select=c("num5pl", "drinklast30", "vol30", "exRounds", "exTourney", "Percent",
                                    "age",
                                    "greek", "married", "female", "year1993", "year1997", 
                                    "year1999", "year2001", "frosh", "soph", "jun", "sen", "white", "black", 
                                    "asian", "other", "gpa"))

b<-round(colMeans(tourneyview, na.rm = TRUE), digits = 3)
c<-round(colMeans(tourneyno, na.rm = TRUE), digits = 3)
d <- matrix(nrow = 23, ncol = 1)
for (i in 1:ncol(tourneyno)) {if (i==5) {d[i] = 0}
                              else{d[i] = t.test(tourneyview[,i], tourneyno[,i])$p.value}}

mat <- matrix(c(as.vector(b),as.vector(c),as.vector(d)), 23,3)
colnames(mat) = c("Tournament Respondents", "Non-Tournament Respondents", "Probability of Identical Means")
rownames(mat) <- c("Mean Binges Past 2 Weeks", "% of Respondents Who Drank Last Month", "Mean Drinks Past Month", "Mean Rounds in Tournament"
                   , "% of Respondents Potentially Viewing Tourney", "Mean Win Percent", "Mean Age", "% of Respondents in Fraternity/Sorority", "% of Respondents Married", "% of Respondents Female", "% Responding in 1993", "% Responding in 1993", 
                   "% Responding in 1993", "% Responding in 1993", "% of Respondents Freshman", "% of Respondents Sophomore", "% of Respondents Junior", "% of Respondents Senior", "% of Respondents White", "% of Respondents Black", 
                   "% of Respondents Asian", "% of Respondents Other Race", "Mean GPA")
# 
# during <- subset(datasum, (datasum$postm==1| datasum$post2w==1))
# notduring <- subset(datasum, (datasum$postm==0 & datasum$post2w==0))
# 
# d<-colMeans(during, na.rm = TRUE)
# e<-colMeans(notduring, na.rm = TRUE)
# mat2 <- matrix(c(as.vector(d),as.vector(e)), 23,2)
# colnames(mat2) = c("During NCAA Tournament", "Other Response Dates")
# rownames(mat2) <- c("Binges Past 2 Weeks", "Drink Past Month?", "Number of Drinks Past Month", "Rounds in Tournament"
#                     , "Tournament Participation", "Win Percent", "Age", "Greek", "Married", "Female", "1993", "1997", 
#                     "1999", "2001", "Freshman", "Sophomore", "Junior", "Senior", "White", "Black", 
#                     "Asian", "Other Race", "GPA")
# 

nontourney <- subset(datasum, datasum$Tourney==0)
tourney <- subset(datasum, datasum$Tourney==1)
nontourney <- subset(nontourney, select=c("num5pl", "drinklast30", "vol30", "exRounds", "exTourney", "Percent",
                                        "age",
                                        "greek", "married", "female", "year1993", "year1997", 
                                        "year1999", "year2001", "frosh", "soph", "jun", "sen", "white", "black", 
                                        "asian", "other", "gpa"))
tourney <- subset(tourney, select=c("num5pl", "drinklast30", "vol30", "exRounds", "exTourney", "Percent",
                                           "age",
                                           "greek", "married", "female", "year1993", "year1997", 
                                           "year1999", "year2001", "frosh", "soph", "jun", "sen", "white", "black", 
                                           "asian", "other", "gpa"))

b<-round(colMeans(nontourney, na.rm = TRUE), digits = 3)
c<-round(colMeans(tourney, na.rm = TRUE), digits = 3)
d <- matrix(nrow = 23, ncol = 1)
for (i in 1:ncol(tourney)) {d[i] = t.test(nontourney[,i], tourney[,i])$p.value}

mat2 <- matrix(c(as.vector(b),as.vector(c), as.vector(d)), 23,3)
colnames(mat2) = c("Non-Tournament Schools", "Tournament Schools", "Probability of Identical Means")
rownames(mat2) <- c("Mean Binges Past 2 Weeks", "% of Respondents Who Drank Last Month", "Mean Drinks Past Month", "Mean Rounds in Tournament"
                   , "% of Respondents Potentially Viewing Tourney", "Mean Win Percent", "Mean Age", "% of Respondents in Fraternity/Sorority", "% of Respondents Married", "% of Respondents Female", "% Responding in 1993", "% Responding in 1993", 
                   "% Responding in 1993", "% Responding in 1993", "% of Respondents Freshman", "% of Respondents Sophomore", "% of Respondents Junior", "% of Respondents Senior", "% of Respondents White", "% of Respondents Black", 
                   "% of Respondents Asian", "% of Respondents Other Race", "Mean GPA")

# capture.output(

  stargazer(mat2, style = "qje", title = "Summary Statistics (Means) for Tournament and Non-Tournament School-Years", label = "summ")
  stargazer(mat, style = "qje", title = "Summary Statistics (Means) for Tournament and Non-Tournament Observations at Tournament Schools", label = "summ2")
# )


##### TESTING HYPOTHETICAL TREATMENT VS OTHER TIMES AT NON-TOURNEY SCHOOLS
hypt <- subset(datasum, (datasum$hyptr==1 & datasum$Tourney==0))
hypno <- subset(datasum, (datasum$hyptr==0 & datasum$Tourney==0))
hypt <- subset(hypt, select=c("num5pl", "drinklast30", "vol30", "exRounds", "exTourney", "Percent",
                                        "age",
                                        "greek", "married", "female", "year1993", "year1997", 
                                        "year1999", "year2001", "frosh", "soph", "jun", "sen", "white", "black", 
                                        "asian", "other", "gpa"))
hypno <- subset(hypno, select=c("num5pl", "drinklast30", "vol30", "exRounds", "exTourney", "Percent",
                                            "age",
                                            "greek", "married", "female", "year1993", "year1997", 
                                            "year1999", "year2001", "frosh", "soph", "jun", "sen", "white", "black", 
                                            "asian", "other", "gpa"))

b<-round(colMeans(hypt, na.rm = TRUE), digits = 3)
c<-round(colMeans(hypno, na.rm = TRUE), digits = 3)
d <- matrix(nrow = 23, ncol = 1)
for (i in 1:ncol(tourneyno)) {if (i==4) {d[i] = 'No'}
                              else{if (i==5) {d[i] = 'No'}
                                   else{if (t.test(tourneyview[,i], tourneyno[,i])$p.value<.1)
                              {d[i]='Yes'}
                              else{d[i]='No'}}}}

mat3 <- matrix(c(as.vector(b),as.vector(c),as.vector(d)), 23,3)
colnames(mat3) = c("[Hypothetical] Tournament Respondents", "Non-Tournament Respondents", "Difference Significant (90%)?")
rownames(mat3) <- c("Mean Binges Past 2 Weeks", "% of Respondents Who Drank Last Month", "Mean Drinks Past Month", "Mean Rounds in Tournament"
                    , "% of Respondents Potentially Viewing Tourney", "Mean Win Percent", "Mean Age", "% of Respondents in Fraternity/Sorority", "% of Respondents Married", "% of Respondents Female", "% Responding in 1993", "% Responding in 1993", 
                    "% Responding in 1993", "% Responding in 1993", "% of Respondents Freshman", "% of Respondents Sophomore", "% of Respondents Junior", "% of Respondents Senior", "% of Respondents White", "% of Respondents Black", 
                    "% of Respondents Asian", "% of Respondents Other Race", "Mean GPA")