library("stargazer")
library("lmtest")
library("sandwich")

# ##### OLD ESTIMATIONS
# orobustbinge <- lm(num5pl ~ Rounds*post2w + Rounds*after + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + white + black + asian + other + gpa + college + athconf + month, data = data2)
# orobustdrink <- lm(drinklast30 ~ Rounds*postm + Rounds*after + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + white + black + asian + other + gpa + college + athconf + month, data = data2)
# orobustvol <- lm(vol30 ~ Rounds*postm + Rounds*after + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + white + black + asian + other + gpa + college + athconf + month, data = data2)

##### EVERYONE
robustage <- lm(age ~ exTourneym + Tourney + Percent + greek + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + white + black + asian + other + gpa + college + athconf + month, data = data2)
robustgreek <- lm(greek ~ exTourneym + Tourney + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + white + black + asian + other + gpa + college + athconf + month, data = data2)
robustmarried <- lm(married ~ exTourneym + Tourney + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + white + black + asian + other + gpa + college + athconf + month, data = data2)
robustfrosh <- lm(frosh ~ exTourneym + Tourney + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + married + female + year1993 + year1997 + year1999 + white + black + asian + other + gpa + college + athconf + month, data = data2)
robustsoph <- lm(soph ~ exTourneym + Tourney + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + married + female + year1993 + year1997 + year1999 + white + black + asian + other + gpa + college + athconf + month, data = data2)
robustjun <- lm(jun ~ exTourneym + Tourney + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + married + female + year1993 + year1997 + year1999 + white + black + asian + other + gpa + college + athconf + month, data = data2)
robustsen <- lm(sen ~ exTourneym + Tourney + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + married + female + year1993 + year1997 + year1999 + white + black + asian + other + gpa + college + athconf + month, data = data2)
robustwhite <- lm(white ~ exTourneym + Tourney + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + gpa + college + athconf + month, data = data2)
robustblack <- lm(black ~ exTourneym + Tourney + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + gpa + college + athconf + month, data = data2)
robustasian <- lm(asian ~ exTourneym + Tourney + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + gpa + college + athconf + month, data = data2)
robustfemale <- lm(female ~ exTourneym + Tourney + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + married + year1993 + year1997 + year1999 + frosh + soph + jun + sen + white + black + asian + other + gpa + college + athconf + month, data = data2)
robustgpa <- lm(gpa ~ exTourneym + Tourney + Percent + a17 + a18 + a19 + a20 + a21 + a22 + a23 + a24 + greek + married + female + year1993 + year1997 + year1999 + frosh + soph + jun + sen + white + black + asian + other + college + athconf + month, data = data2)

# compute Stata like df-adjustment
G <- length(unique(data2$college))
N <- length(data2$college)

# Clustered and Heteroskedasticity corrected results: EVERYONE
dfa <- (G/(G - 1)) * (N - 1)/robustage$df.residual
college_c_vcov <- dfa * vcovHC(robustage, type = "HC0", cluster = "group", adjust = T)
rage <- coeftest(robustage, vcov = college_c_vcov)

dfa <- (G/(G - 1)) * (N - 1)/robustgreek$df.residual
college_c_vcov <- dfa * vcovHC(robustgreek, type = "HC0", cluster = "group", adjust = T)
rgreek <- coeftest(robustgreek, vcov = college_c_vcov)

dfa <- (G/(G - 1)) * (N - 1)/robustmarried$df.residual
college_c_vcov <- dfa * vcovHC(robustmarried, type = "HC0", cluster = "group", adjust = T)
rmarried <- coeftest(robustmarried, vcov = college_c_vcov)

dfa <- (G/(G - 1)) * (N - 1)/robustfrosh$df.residual
college_c_vcov <- dfa * vcovHC(robustfrosh, type = "HC0", cluster = "group", adjust = T)
rfrosh <- coeftest(robustfrosh, vcov = college_c_vcov)

dfa <- (G/(G - 1)) * (N - 1)/robustsoph$df.residual
college_c_vcov <- dfa * vcovHC(robustsoph, type = "HC0", cluster = "group", adjust = T)
rsoph <- coeftest(robustsoph, vcov = college_c_vcov)

dfa <- (G/(G - 1)) * (N - 1)/robustjun$df.residual
college_c_vcov <- dfa * vcovHC(robustjun, type = "HC0", cluster = "group", adjust = T)
rjun <- coeftest(robustjun, vcov = college_c_vcov)

dfa <- (G/(G - 1)) * (N - 1)/robustsen$df.residual
college_c_vcov <- dfa * vcovHC(robustsen, type = "HC0", cluster = "group", adjust = T)
rsen <- coeftest(robustsen, vcov = college_c_vcov)

dfa <- (G/(G - 1)) * (N - 1)/robustwhite$df.residual
college_c_vcov <- dfa * vcovHC(robustwhite, type = "HC0", cluster = "group", adjust = T)
rwhite <- coeftest(robustwhite, vcov = college_c_vcov)

dfa <- (G/(G - 1)) * (N - 1)/robustblack$df.residual
college_c_vcov <- dfa * vcovHC(robustblack, type = "HC0", cluster = "group", adjust = T)
rblack <- coeftest(robustblack, vcov = college_c_vcov)

dfa <- (G/(G - 1)) * (N - 1)/robustasian$df.residual
college_c_vcov <- dfa * vcovHC(robustasian, type = "HC0", cluster = "group", adjust = T)
rasian <- coeftest(robustasian, vcov = college_c_vcov)

dfa <- (G/(G - 1)) * (N - 1)/robustfemale$df.residual
college_c_vcov <- dfa * vcovHC(robustfemale, type = "HC0", cluster = "group", adjust = T)
rfemale <- coeftest(robustfemale, vcov = college_c_vcov)

dfa <- (G/(G - 1)) * (N - 1)/robustgpa$df.residual
college_c_vcov <- dfa * vcovHC(robustgpa, type = "HC0", cluster = "group", adjust = T)
rgpa <- coeftest(robustgpa, vcov = college_c_vcov)




stargazer(rage, rgreek, rmarried, rfrosh, rsoph, rjun, rsen, rwhite, rblack, rasian, rfemale, rgpa,
          style = "qje",
          label = "robfemale",
          model.numbers = FALSE,
          title = "NCAA Tournament Demographic Effects",
          dep.var.labels = c("Age", "Greek", "Married", "Freshman", "Sophomore", "Junior", "Senior", "White", "Black", "Asian", "Female", "GPA"),
          keep = c("exRoundsm", "Tourney", "Percent", "greek", "married", "frosh", "soph", "jun", "sen", "white", "black", "asian", "gpa"),
          order = c("exRoundsm", "Tourney", "Percent", "greek", "married", "frosh", "soph", "jun", "sen", "white", "black", "asian", "gpa"),
          covariate.labels = c("Tournament Observed", "Tournament Year", "Win Percent", "Greek",
                               "Married", "Freshman", "Sophomore", "Junior", "Senior",
                               "White", "Black", "Asian", "GPA"),
          no.space = TRUE,
          digits = 3,
          notes.align = "l",
          notes.append = FALSE,
          notes.label = NULL,
          notes =  "$^{***}$Significant at the 1 percent level.} \\\\
          \\multicolumn{4}{l}{$^{**}$Significant at the 5 percent level.} \\\\
          \\multicolumn{4}{l}{$^{*}$Significant at the 10 percent level.} \\\\
          \\multicolumn{4}{l}{Other controls included in regression were indicators for ages}  \\\\
          \\multicolumn{4}{l}{between 17 and 24, athletic conference, school, month and year.} \\\\"
)
