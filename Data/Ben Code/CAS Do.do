//
// Merging College-level and Student-level data
//


use "/Users/dustywhite/Desktop/Research Files (Aug 2013)/CAS/Student Compiled.dta"
merge m:1 coll_id year using "/Users/dustywhite/Desktop/Research Files (Aug 2013)/CAS/College Compiled.dta"
save "/Users/dustywhite/Desktop/Research Files (Aug 2013)/CAS/Complete Compiled.dta"


//
//Creating variables to match BRFSS
//


gen aidyear = .
replace aidyear = 1991 if state=="AR"
replace aidyear = 1993 if state=="GA"
replace aidyear = 1994 if state=="ND"
replace aidyear = 1996 if state=="MS"
replace aidyear = 1996 if state=="OK"
replace aidyear = 1997 if state=="FL"
replace aidyear = 1997 if state=="NM"
replace aidyear = 1997 if state=="MO"
replace aidyear = 1997 if state=="NJ"
replace aidyear = 1997 if state=="NY"
replace aidyear = 1998 if state=="LA"
replace aidyear = 1998 if state=="SC"
replace aidyear = 1999 if state=="KY"
replace aidyear = 1999 if state=="AK"
replace aidyear = 1999 if state=="IL"
replace aidyear = 1999 if state=="UT"
replace aidyear = 1999 if state=="WA"
replace aidyear = 2000 if state=="NV"
replace aidyear = 2000 if state=="MI"
replace aidyear = 2001 if state=="CA"
replace aidyear = 2001 if state=="ID"
replace aidyear = 2002 if state=="WV"
replace aidyear = 2002 if state=="MD"
replace aidyear = 2003 if state=="TN"
replace aidyear = 2004 if state=="SD"
replace aidyear = 2005 if state=="MA"
replace aidyear = 2005 if state=="MT"
replace aidyear = 2006 if state=="DE"
replace aidyear = 2006 if state=="WY"

gen strongstate = 0
replace strongstate = 1 if state == "GA" | state == "FL" | state =="NM" | state =="LA" | state =="SC" | state =="KY" | state =="NV" | state =="WV" | state =="TN"
gen anyaidstate = 0
replace anyaidstate = 1 if aidyear!=.


gen num5plus = c1
gen married = 1 if nevermar == 0
replace married = 0 if nevermar == 1
gen raceblack = aa
gen female = 1 if sex ==0
replace female = 0 if sex ==1
gen year1993 = 0
replace year1993 = 1 if year == 1993
gen year1997 = 0
replace year1997 = 1 if year == 1997
gen year1999 = 0
replace year1999 = 1 if year == 1999
gen year2001 = 0
replace year2001 = 1 if year == 2001
gen year2005 = 0
replace year2005 = 1 if year == 2005
gen freshman = 0
replace freshman = 1 if class == 1
gen sophomore = .
replace sophomore =1 if class == 2
replace sophomore = 0 if sophomore == .
gen juniorclass = 0
replace juniorclass = 1 if class ==3
gen seniorclass = 0
replace seniorclass = 1 if class > 3
gen drinklast30 = .
replace drinklast30 = 0 if c6 <4
replace drinklast30 = 1 if c6 > 3 & c6 < .

gen aideligible = 0
replace aideligible = 1 if (age - (aidyear - year)) > 18 & aidyear !=.
gen strongmerit = 1 if aideligible  == 1 & strongstate == 1
replace strongmerit = 0 if strongmerit == .
gen weakmerit = 1 if aideligible  == 1 & strongstate == 0
replace weakmerit = 0 if weakmerit == .
gen anymerit = aideligible

gen statefip = .
replace statefip = 	1	 if state ==	"AL"replace statefip = 	2	 if state ==	"AK"replace statefip = 	4	 if state ==	"AZ"replace statefip = 	5	 if state ==	"AR"replace statefip = 	6	 if state ==	"CA"replace statefip = 	8	 if state ==	"CO"replace statefip = 	9	 if state ==	"CT"replace statefip = 	10	 if state ==	"DE"replace statefip = 	11	 if state ==	"DC"replace statefip = 	12	 if state ==	"FL"replace statefip = 	13	 if state ==	"GA"replace statefip = 	15	 if state ==	"HI"replace statefip = 	16	 if state ==	"ID"replace statefip = 	17	 if state ==	"IL"replace statefip = 	18	 if state ==	"IN"replace statefip = 	19	 if state ==	"IA"replace statefip = 	20	 if state ==	"KS"replace statefip = 	21	 if state ==	"KY"replace statefip = 	22	 if state ==	"LA"replace statefip = 	23	 if state ==	"ME"replace statefip = 	24	 if state ==	"MD"replace statefip = 	25	 if state ==	"MA"replace statefip = 	26	 if state ==	"MI"replace statefip = 	27	 if state ==	"MN"replace statefip = 	28	 if state ==	"MS"replace statefip = 	29	 if state ==	"MO"replace statefip = 	30	 if state ==	"MT"replace statefip = 	31	 if state ==	"NE"replace statefip = 	32	 if state ==	"NV"replace statefip = 	33	 if state ==	"NH"replace statefip = 	34	 if state ==	"NJ"replace statefip = 	35	 if state ==	"NM"replace statefip = 	36	 if state ==	"NY"replace statefip = 	37	 if state ==	"NC"replace statefip = 	38	 if state ==	"ND"replace statefip = 	39	 if state ==	"OH"replace statefip = 	40	 if state ==	"OK"replace statefip = 	41	 if state ==	"OR"replace statefip = 	42	 if state ==	"PA"replace statefip = 	44	 if state ==	"RI"replace statefip = 	45	 if state ==	"SC"replace statefip = 	46	 if state ==	"SD"replace statefip = 	47	 if state ==	"TN"replace statefip = 	48	 if state ==	"TX"replace statefip = 	49	 if state ==	"UT"replace statefip = 	50	 if state ==	"VT"replace statefip = 	51	 if state ==	"VA"replace statefip = 	53	 if state ==	"WA"replace statefip = 	54	 if state ==	"WV"replace statefip = 	55	 if state ==	"WI"replace statefip = 	56	 if state ==	"WY"


//First Time Regressions

reg binge anymerit female raceblack asian other hispanic age freshman sophomore juniorclass seniorclass married year1993 year1997 year1999 year2001 year2005, vce(cluster statefip)
xtreg binge anymerit female raceblack asian other hispanic age freshman sophomore juniorclass seniorclass married year1993 year1997 year1999 year2001 year2005, fe vce(cluster statefip)
