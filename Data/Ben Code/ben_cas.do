capture log close
clear all
set more off
set mem 500m
set maxvar 10000
#delimit ;

cd "C:\Documents and Settings\ben.cowan\Bcowan\merit aid risky behaviors\CAS";

log using ben_cas.log, replace;

use "Complete Compiled";

format year %12.0g;



/* Need to correct definition of aideligible */
drop aideligible;
drop strongmerit;
drop weakmerit;
drop anymerit;

gen aideligible = 0;

gen froshyear=year if class==1;
replace froshyear=(year-1) if class==2;
replace froshyear=(year-2) if class==3;
replace froshyear=(year-3) if class==4;
replace froshyear=(year-4) if class==5;
replace froshyear=(year-5) if class==6;

*replace aideligible = 1 if (age - (year - aidyear)) <= 18 & aidyear !=.;
replace aideligible = 1 if (froshyear>=aidyear/*+1*/) & froshyear<.; /* I just realized there were a few people who were age-eligible because froshyear was missing--those with class = 6. Alternatively, I could assign a froshyear for these*/
/*
replace aideligible = 1 if year==1997 & state=="FL" & class<=3;
replace aideligible = 1 if year>=1999 & state=="FL";
replace aideligible = 1 if year>=aidyear & state=="SC";
replace aideligible = 1 if year==1999 & state=="LA" & class<=3;
replace aideligible = 1 if year==2001 & state=="LA";
*/
gen strongmerit = 1 if aideligible  == 1 & strongstate == 1;
replace strongmerit = 0 if strongmerit == .;
gen weakmerit = 1 if aideligible  == 1 & strongstate == 0;
replace weakmerit = 0 if weakmerit == .;
gen anymerit = aideligible;

gen hsyear=(year-1) if class==1;
replace hsyear=(year-2) if class==2;
replace hsyear=(year-3) if class==3;
replace hsyear=(year-4) if class==4;
replace hsyear=(year-5) if class==5;
replace hsyear=(year-6) if class==6;

gen hsaid=0;
replace hsaid=1 if hsyear>=aidyear & hsyear<.;

gen hsstrong = 1 if hsaid  == 1 & strongstate == 1;
replace hsstrong = 0 if hsstrong == .;
gen hsweak = 1 if hsaid  == 1 & strongstate == 0;
replace hsweak = 0 if hsweak == .;
gen hsany = hsaid;


tab coll_id, gen(cid);

gen a17=(age<18) if age<.;
gen a18=(age==18) if age<.;
gen a19=(age==19) if age<.;
gen a20=(age==20) if age<.;
gen a21=(age==21) if age<.;
gen a22=(age==22) if age<.;
gen a23=(age==23) if age<.;
gen a24=(age==24) if age<.;
gen a25=(age>24) if age<.;

/* agelt21 */

/* female--created by Dusty */

/* married--created by Dusty */

drop white black asian other;

gen white=(race==1) if race<.;
gen black=(race==2) if race<.;
gen asian=(race==3) if race<.;
gen other=(race==4) if race<.;

/* hispanic */

drop soph;

gen frosh=(class==1) if class<.;
gen soph=(class==2) if class<.;
gen jun=(class==3) if class<.;
gen sen=(class==4) if class<.;
gen gsen=(class>4) if class<.;

/* offcamp */

/* memgreek */



gen binged_2wk=(drinkcat>=2) if drinkcat<.;

/* vol30 */

gen dr5pl_2wk_0=(c2==1) if c2<. & year==1993;
gen dr5pl_2wk_1=(c2==2) if c2<. & year==1993;
gen dr5pl_2wk_2=(c2==3) if c2<. & year==1993;
gen dr5pl_2wk_3t5=(c2==4) if c2<. & year==1993;
gen dr5pl_2wk_6t9=(c2==5) if c2<. & year==1993;
gen dr5pl_2wk_10pl=(c2==6) if c2<. & year==1993;

replace dr5pl_2wk_0=(c1==1) if c1<. & year>1993;
replace dr5pl_2wk_1=(c1==2) if c1<. & year>1993;
replace dr5pl_2wk_2=(c1==3) if c1<. & year>1993;
replace dr5pl_2wk_3t5=(c1==4) if c1<. & year>1993;
replace dr5pl_2wk_6t9=(c1==5) if c1<. & year>1993;
replace dr5pl_2wk_10pl=(c1==6) if c1<. & year>1993;

gen dr5pl=(dr5pl_2wk_0==0) if dr5pl_2wk_0<.;

gen num5pl=0 if dr5pl_2wk_0==1;
replace num5pl=1 if dr5pl_2wk_1==1;
replace num5pl=2 if dr5pl_2wk_2==1;
replace num5pl=4 if dr5pl_2wk_3t5==1;
replace num5pl=7.5 if dr5pl_2wk_6t9==1;
replace num5pl=10 if dr5pl_2wk_10pl==1;



gen timesdrk_30days_0=(c7==.) if year==1993;
gen timesdrk_30days_1t2=(c7==1) if year==1993;
gen timesdrk_30days_3t5=(c7==2) if year==1993;
gen timesdrk_30days_6t9=(c7==3) if year==1993;
gen timesdrk_30days_10t19=(c7==4) if year==1993;
gen timesdrk_30days_20t39=(c7==5) if year==1993;
gen timesdrk_30days_40pl=(c7==6) if year==1993;

replace timesdrk_30days_0=(c11==.) if year==1997;
replace timesdrk_30days_1t2=(c11==2) if year==1997;
replace timesdrk_30days_3t5=(c11==3) if year==1997;
replace timesdrk_30days_6t9=(c11==4) if year==1997;
replace timesdrk_30days_10t19=(c11==5) if year==1997;
replace timesdrk_30days_20t39=(c11==6) if year==1997;
replace timesdrk_30days_40pl=(c11==7) if year==1997;

replace timesdrk_30days_0=(c9==. | c9==1) if year==1999;
replace timesdrk_30days_1t2=(c9==2) if year==1999;
replace timesdrk_30days_3t5=(c9==3) if year==1999;
replace timesdrk_30days_6t9=(c9==4) if year==1999;
replace timesdrk_30days_10t19=(c9==5) if year==1999;
replace timesdrk_30days_20t39=(c9==6) if year==1999;
replace timesdrk_30days_40pl=(c9==7) if year==1999;

replace timesdrk_30days_0=(c11==.) if year==2001;
replace timesdrk_30days_1t2=(c11==1) if year==2001;
replace timesdrk_30days_3t5=(c11==2) if year==2001;
replace timesdrk_30days_6t9=(c11==3) if year==2001;
replace timesdrk_30days_10t19=(c11==4) if year==2001;
replace timesdrk_30days_20t39=(c11==5) if year==2001;
replace timesdrk_30days_40pl=(c11==6) if year==2001;

replace timesdrk_30days_0=(c10==.) if year==2005;
replace timesdrk_30days_1t2=(c10==1) if year==2005;
replace timesdrk_30days_3t5=(c10==2) if year==2005;
replace timesdrk_30days_6t9=(c10==3) if year==2005;
replace timesdrk_30days_10t19=(c10==4) if year==2005;
replace timesdrk_30days_20t39=(c10==5) if year==2005;
replace timesdrk_30days_40pl=(c10==6) if year==2005;

gen times_gt5=(timesdrk_30days_6t9==1 | timesdrk_30days_10t19==1 | timesdrk_30days_20t39==1 | timesdrk_30days_40pl==1) if timesdrk_30days_0<.; 

gen numtimes=0 if timesdrk_30days_0==1;
replace numtimes=1.5 if timesdrk_30days_1t2==1;
replace numtimes=4 if timesdrk_30days_3t5==1;
replace numtimes=7.5 if timesdrk_30days_6t9==1;
replace numtimes=14.5 if timesdrk_30days_10t19==1;
replace numtimes=29.5 if timesdrk_30days_20t39==1;
replace numtimes=40 if timesdrk_30days_40pl==1;

gen drank=(numtimes>0) if numtimes<.;



gen drksday_30days_0=(c8==.) if year==1993;
gen drksday_30days_1=(c8==1) if year==1993;
gen drksday_30days_2=(c8==2) if year==1993;
gen drksday_30days_3=(c8==3) if year==1993;
gen drksday_30days_4=(c8==4) if year==1993;
gen drksday_30days_5=(c8==5) if year==1993;
gen drksday_30days_6=(c8==6) if year==1993;
gen drksday_30days_7=(c8==7) if year==1993;
gen drksday_30days_8=(c8==8) if year==1993;
gen drksday_30days_9pl=(c8==9) if year==1993;

replace drksday_30days_0=(c12==.) if year==1997;
replace drksday_30days_1=(c12==1) if year==1997;
replace drksday_30days_2=(c12==2) if year==1997;
replace drksday_30days_3=(c12==3) if year==1997;
replace drksday_30days_4=(c12==4) if year==1997;
replace drksday_30days_5=(c12==5) if year==1997;
replace drksday_30days_6=(c12==6) if year==1997;
replace drksday_30days_7=(c12==7) if year==1997;
replace drksday_30days_8=(c12==8) if year==1997;
replace drksday_30days_9pl=(c12==9) if year==1997;

replace drksday_30days_0=(c10==. | c10==0) if year==1999;
replace drksday_30days_1=(c10==1) if year==1999;
replace drksday_30days_2=(c10==2) if year==1999;
replace drksday_30days_3=(c10==3) if year==1999;
replace drksday_30days_4=(c10==4) if year==1999;
replace drksday_30days_5=(c10==5) if year==1999;
replace drksday_30days_6=(c10==6) if year==1999;
replace drksday_30days_7=(c10==7) if year==1999;
replace drksday_30days_8=(c10==8) if year==1999;
replace drksday_30days_9pl=(c10==9) if year==1999;

replace drksday_30days_0=(c12==.) if year==2001;
replace drksday_30days_1=(c12==1) if year==2001;
replace drksday_30days_2=(c12==2) if year==2001;
replace drksday_30days_3=(c12==3) if year==2001;
replace drksday_30days_4=(c12==4) if year==2001;
replace drksday_30days_5=(c12==5) if year==2001;
replace drksday_30days_6=(c12==6) if year==2001;
replace drksday_30days_7=(c12==7) if year==2001;
replace drksday_30days_8=(c12==8) if year==2001;
replace drksday_30days_9pl=(c12==9) if year==2001;

replace drksday_30days_0=(c11==.) if year==2005;
replace drksday_30days_1=(c11==1) if year==2005;
replace drksday_30days_2=(c11==2) if year==2005;
replace drksday_30days_3=(c11==3) if year==2005;
replace drksday_30days_4=(c11==4) if year==2005;
replace drksday_30days_5=(c11==5) if year==2005;
replace drksday_30days_6=(c11==6) if year==2005;
replace drksday_30days_7=(c11==7) if year==2005;
replace drksday_30days_8=(c11==8) if year==2005;
replace drksday_30days_9pl=(c11==9) if year==2005;

gen drks_gt3=(drksday_30days_4==1 | drksday_30days_5==1 | drksday_30days_6==1 | drksday_30days_7==1 | drksday_30days_8==1 | drksday_30days_9pl==1) if drksday_30days_0<.;

gen numdrks=0 if drksday_30days_0==1;
replace numdrks=1 if drksday_30days_1==1;
replace numdrks=2 if drksday_30days_2==1;
replace numdrks=3 if drksday_30days_3==1;
replace numdrks=4 if drksday_30days_4==1;
replace numdrks=5 if drksday_30days_5==1;
replace numdrks=6 if drksday_30days_6==1;
replace numdrks=7 if drksday_30days_7==1;
replace numdrks=8 if drksday_30days_8==1;
replace numdrks=9 if drksday_30days_9pl==1;



/* grade */

gen gpa_a=(f5==1) if f5<. & year==1993;
gen gpa_am=(f5==2) if f5<. & year==1993;
gen gpa_bpl=(f5==3) if f5<. & year==1993;
gen gpa_b=(f5==4) if f5<. & year==1993;
gen gpa_bm=(f5==5) if f5<. & year==1993;
gen gpa_cpl=(f5==6) if f5<. & year==1993;
gen gpa_c=(f5==7) if f5<. & year==1993;
gen gpa_cm=(f5==8) if f5<. & year==1993;
gen gpa_d=(f5==9) if f5<. & year==1993;
gen gpa_na=(f5==10) if f5<. & year==1993;

replace gpa_a=(f4==1) if f4<. & year==1997;
replace gpa_am=(f4==2) if f4<. & year==1997;
replace gpa_bpl=(f4==3) if f4<. & year==1997;
replace gpa_b=(f4==4) if f4<. & year==1997;
replace gpa_bm=(f4==5) if f4<. & year==1997;
replace gpa_cpl=(f4==6) if f4<. & year==1997;
replace gpa_c=(f4==7) if f4<. & year==1997;
replace gpa_cm=(f4==8) if f4<. & year==1997;
replace gpa_d=(f4==9) if f4<. & year==1997;
replace gpa_na=(f4==10) if f4<. & year==1997;

replace gpa_a=(f4==1) if f4<. & year==1999;
replace gpa_am=(f4==2) if f4<. & year==1999;
replace gpa_bpl=(f4==3) if f4<. & year==1999;
replace gpa_b=(f4==4) if f4<. & year==1999;
replace gpa_bm=(f4==5) if f4<. & year==1999;
replace gpa_cpl=(f4==6) if f4<. & year==1999;
replace gpa_c=(f4==7) if f4<. & year==1999;
replace gpa_cm=(f4==8) if f4<. & year==1999;
replace gpa_d=(f4==9) if f4<. & year==1999;
replace gpa_na=(f4==10) if f4<. & year==1999;

replace gpa_a=(f5==1) if f5<. & year==2001;
replace gpa_am=(f5==2) if f5<. & year==2001;
replace gpa_bpl=(f5==3) if f5<. & year==2001;
replace gpa_b=(f5==4) if f5<. & year==2001;
replace gpa_bm=(f5==5) if f5<. & year==2001;
replace gpa_cpl=(f5==6) if f5<. & year==2001;
replace gpa_c=(f5==7) if f5<. & year==2001;
replace gpa_cm=(f5==8) if f5<. & year==2001;
replace gpa_d=(f5==10) if f5<. & year==2001;
replace gpa_na=(f5==99) if f5<. & year==2001;

replace gpa_a=(f5==1) if f5<. & year==2005;
replace gpa_am=(f5==2) if f5<. & year==2005;
replace gpa_bpl=(f5==3) if f5<. & year==2005;
replace gpa_b=(f5==4) if f5<. & year==2005;
replace gpa_bm=(f5==5) if f5<. & year==2005;
replace gpa_cpl=(f5==6) if f5<. & year==2005;
replace gpa_c=(f5==7) if f5<. & year==2005;
replace gpa_cm=(f5==8) if f5<. & year==2005;
replace gpa_d=(f5==9) if f5<. & year==2005;
replace gpa_na=(f5==10) if f5<. & year==2005;

gen gstud=(gpa_a==1 | gpa_am==1 | gpa_bpl==1 | gpa_b==1) if gpa_a<.;

gen marginal=(gpa_b==1 | gpa_bm==1 | gpa_cpl==1) if gpa_a<.;

gen gpa=4.0 if gpa_a==1;
replace gpa=3.7 if gpa_am==1;
replace gpa=3.4 if gpa_bpl==1;
replace gpa=3.0 if gpa_b==1;
replace gpa=2.7 if gpa_bm==1;
replace gpa=2.4 if gpa_cpl==1;
replace gpa=2.0 if gpa_c==1;
replace gpa=1.7 if gpa_cm==1;
replace gpa=1.0 if gpa_d==1;


gen work_0=(g7_a==1) if g7_a<. & year==1997;
gen work_1t10=(g7_a==2) if g7_a<. & year==1997;
gen work_11t20=(g7_a==3) if g7_a<. & year==1997;
gen work_21t35=(g7_a==4) if g7_a<. & year==1997;
gen work_36t50=(g7_a==5) if g7_a<. & year==1997;
gen work_51t75=(g7_a==6) if g7_a<. & year==1997;
gen work_76t125=(g7_a==7) if g7_a<. & year==1997;
gen work_126pl=(g7_a==8) if g7_a<. & year==1997;

replace work_0=(g5a==1) if g5a<. & year==1999;
replace work_1t10=(g5a==2) if g5a<. & year==1999;
replace work_11t20=(g5a==3) if g5a<. & year==1999;
replace work_21t35=(g5a==4) if g5a<. & year==1999;
replace work_36t50=(g5a==5) if g5a<. & year==1999;
replace work_51t75=(g5a==6) if g5a<. & year==1999;
replace work_76t125=(g5a==7) if g5a<. & year==1999;
replace work_126pl=(g5a==8) if g5a<. & year==1999;

replace work_0=(g5a==1) if g5a<. & year==2001;
replace work_1t10=(g5a==2) if g5a<. & year==2001;
replace work_11t20=(g5a==3) if g5a<. & year==2001;
replace work_21t35=(g5a==4) if g5a<. & year==2001;
replace work_36t50=(g5a==5) if g5a<. & year==2001;
replace work_51t75=(g5a==6) if g5a<. & year==2001;
replace work_76t125=(g5a==7) if g5a<. & year==2001;
replace work_126pl=(g5a==8) if g5a<. & year==2001;

gen work=0 if work_0==1;
replace work=5.5 if work_1t10==1;
replace work=15.5 if work_11t20==1;
replace work=28 if work_21t35==1;
replace work=43 if work_36t50==1;
replace work=63 if work_51t75==1;
replace work=100.5 if work_76t125==1;
replace work=126 if work_126pl==1;


gen allowance_0=(g7_b==1) if g7_b<. & year==1997;
gen allowance_1t10=(g7_b==2) if g7_b<. & year==1997;
gen allowance_11t20=(g7_b==3) if g7_b<. & year==1997;
gen allowance_21t35=(g7_b==4) if g7_b<. & year==1997;
gen allowance_36t50=(g7_b==5) if g7_b<. & year==1997;
gen allowance_51t75=(g7_b==6) if g7_b<. & year==1997;
gen allowance_76t125=(g7_b==7) if g7_b<. & year==1997;
gen allowance_126pl=(g7_b==8) if g7_b<. & year==1997;

replace allowance_0=(g5b==1) if g5b<. & year==1999;
replace allowance_1t10=(g5b==2) if g5b<. & year==1999;
replace allowance_11t20=(g5b==3) if g5b<. & year==1999;
replace allowance_21t35=(g5b==4) if g5b<. & year==1999;
replace allowance_36t50=(g5b==5) if g5b<. & year==1999;
replace allowance_51t75=(g5b==6) if g5b<. & year==1999;
replace allowance_76t125=(g5b==7) if g5b<. & year==1999;
replace allowance_126pl=(g5b==8) if g5b<. & year==1999;

replace allowance_0=(g5b==1) if g5b<. & year==2001;
replace allowance_1t10=(g5b==2) if g5b<. & year==2001;
replace allowance_11t20=(g5b==3) if g5b<. & year==2001;
replace allowance_21t35=(g5b==4) if g5b<. & year==2001;
replace allowance_36t50=(g5b==5) if g5b<. & year==2001;
replace allowance_51t75=(g5b==6) if g5b<. & year==2001;
replace allowance_76t125=(g5b==7) if g5b<. & year==2001;
replace allowance_126pl=(g5b==8) if g5b<. & year==2001;

gen allowance=0 if allowance_0==1;
replace allowance=5.5 if allowance_1t10==1;
replace allowance=15.5 if allowance_11t20==1;
replace allowance=28 if allowance_21t35==1;
replace allowance=43 if allowance_36t50==1;
replace allowance=63 if allowance_51t75==1;
replace allowance=100.5 if allowance_76t125==1;
replace allowance=126 if allowance_126pl==1;



gen hstimesdrk_mo_0=(g8==1) if g8<. & year==1993;
gen hstimesdrk_mo_1t2=(g8==2) if g8<. & year==1993;
gen hstimesdrk_mo_3t5=(g8==3) if g8<. & year==1993;
gen hstimesdrk_mo_6t9=(g8==4) if g8<. & year==1993;
gen hstimesdrk_mo_10t19=(g8==5) if g8<. & year==1993;
gen hstimesdrk_mo_20t39=(g8==6) if g8<. & year==1993;
gen hstimesdrk_mo_40pl=(g8==7) if g8<. & year==1993;

replace hstimesdrk_mo_0=(g9==1) if g9<. & year==1997;
replace hstimesdrk_mo_1t2=(g9==2) if g9<. & year==1997;
replace hstimesdrk_mo_3t5=(g9==3) if g9<. & year==1997;
replace hstimesdrk_mo_6t9=(g9==4) if g9<. & year==1997;
replace hstimesdrk_mo_10t19=(g9==5) if g9<. & year==1997;
replace hstimesdrk_mo_20t39=(g9==6) if g9<. & year==1997;
replace hstimesdrk_mo_40pl=(g9==7) if g9<. & year==1997;

replace hstimesdrk_mo_0=(g10==1) if g10<. & year==1999;
replace hstimesdrk_mo_1t2=(g10==2) if g10<. & year==1999;
replace hstimesdrk_mo_3t5=(g10==3) if g10<. & year==1999;
replace hstimesdrk_mo_6t9=(g10==4) if g10<. & year==1999;
replace hstimesdrk_mo_10t19=(g10==5) if g10<. & year==1999;
replace hstimesdrk_mo_20t39=(g10==6) if g10<. & year==1999;
replace hstimesdrk_mo_40pl=(g10==7) if g10<. & year==1999;

replace hstimesdrk_mo_0=(g9==1) if g9<. & year==2001;
replace hstimesdrk_mo_1t2=(g9==2) if g9<. & year==2001;
replace hstimesdrk_mo_3t5=(g9==3) if g9<. & year==2001;
replace hstimesdrk_mo_6t9=(g9==4) if g9<. & year==2001;
replace hstimesdrk_mo_10t19=(g9==5) if g9<. & year==2001;
replace hstimesdrk_mo_20t39=(g9==6) if g9<. & year==2001;
replace hstimesdrk_mo_40pl=(g9==7) if g9<. & year==2001;

gen hsnumtimes=0 if hstimesdrk_mo_0==1;
replace hsnumtimes=1.5 if hstimesdrk_mo_1t2==1;
replace hsnumtimes=4 if hstimesdrk_mo_3t5==1;
replace hsnumtimes=7.5 if hstimesdrk_mo_6t9==1;
replace hsnumtimes=14.5 if hstimesdrk_mo_10t19==1;
replace hsnumtimes=29.5 if hstimesdrk_mo_20t39==1;
replace hsnumtimes=40 if hstimesdrk_mo_40pl==1;



gen fcol=(g14==3 | g14==4) if g14<. & year==1993;
gen mcol=(g15==3 | g15==4) if g15<. & year==1993;

replace fcol=(g17==2 | g17==4) if g17<. & year==1997;
replace mcol=(g17==3 | g17==4) if g17<. & year==1997;

replace fcol=(g18==3 | g18==4) if g18<. & year==1999;
replace mcol=(g19==3 | g19==4) if g19<. & year==1999;

replace fcol=(g17==3 | g17==4) if g17<. & year==2001;
replace mcol=(g18==3 | g18==4) if g18<. & year==2001;



gen norel=(g4==1) if g4<. & year<2005;
gen cath=(g4==2) if g4<. & year<2005;
gen jew=(g4==3) if g4<. & year<2005;
gen mos=(g4==4) if g4<. & year<2005;
gen prot=(g4==5) if g4<. & year<2005;
gen othrel=(g4==6) if g4<. & year<2005;



/* School characteristics */

tab enroll, gen(en);

tab competit, gen(comp);

gen religious=(relbaron>0) if relbaron<.;



/* State by linear trends */
gen t=1 if year1993==1;
replace t=5 if year1997==1;
replace t=7 if year1999==1;
replace t=9 if year2001==1;
replace t=13 if year2005==1;

forvalues x = 1/55 {;
gen t`x' = t*(statefip==`x');
};

tab statefip, gen(s);



/* Region by year dummies */
gen neast=(region==1) if region<.;
gen south=(region==2) if region<.;
gen mwest=(region==3) if region<.;
gen west=(region==4) if region<.;

foreach x in neast south mwest west {;
foreach y in 1993 1997 1999 2001 2005 {;
gen `x'`y' = `x'*year`y';
};
};


drop _merge;
sort statefip year;
merge m:1 statefip year using "C:\Documents and Settings\ben.cowan\Bcowan\merit aid risky behaviors\BRFSS with State Controls\statecontrols", keepusing(medianinc taxrate unemp);
*drop _merge;

replace medianinc="." if medianinc=="NA";
destring medianinc, replace;



local small /*a17-a25 female white-other frosh-gsen married hispanic offcamp memgreek*/ year1993-year2005 /*gpa_a-gpa_na
pub_pri rural commuter en1-en4 comp1-comp8 religious medianinc taxrate unemp*/ s1-s41 /*t1-t55*/ /*neast1993-west2005*/; /* If I use this one I can include 2005 */

local smallpl /*a17-a25 female white-other frosh-gsen married hispanic offcamp memgreek*/ year1993-year2005 /*gpa_a-gpa_na
pub_pri rural commuter en1-en4 comp1-comp8 religious medianinc taxrate unemp*/ s1-s41 t1-t55 neast1993-west2005; /* If I use this one I can include 2005 */

local large a17-a25 female white-other frosh-gsen /*married*/ hispanic /*offcamp memgreek*/ year1993-year2005 /*gpa_a-gpa_na*/ /*hsnumtimes OR hstimesdrk_mo_0-hstimesdrk_mo_40pl*/ fcol mcol norel-othrel 
/*pub_pri rural commuter en1-en4 comp1-comp8 religious*/ medianinc taxrate unemp s1-s41 /*cid1-cid144*/ /*t1-t55 neast1993-west2005*/;

local largep a17-a25 female white-other frosh-gsen /*married*/ hispanic /*offcamp memgreek*/ year1993-year2005 /*gpa_a-gpa_na*/ /*hsnumtimes OR hstimesdrk_mo_0-hstimesdrk_mo_40pl*/ fcol mcol norel-othrel 
/*pub_pri rural commuter en1-en4 comp1-comp8 religious*/ medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 /*neast1993-west2005*/;

local largepl a17-a25 female white-other frosh-gsen /*married*/ hispanic /*offcamp memgreek*/ year1993-year2005 /*gpa_a-gpa_na*/ /*hsnumtimes OR hstimesdrk_mo_0-hstimesdrk_mo_40pl*/ fcol mcol norel-othrel 
/*pub_pri rural commuter en1-en4 comp1-comp8 religious*/ medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 neast1993-west2005;

local largepll a17-a25 female white-other frosh-gsen married hispanic offcamp memgreek year1993-year2005 gpa_a-gpa_na /*hsnumtimes OR hstimesdrk_mo_0-hstimesdrk_mo_40pl*/ fcol mcol norel-othrel 
pub_pri rural commuter en1-en4 comp1-comp8 religious medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 neast1993-west2005;



gen reg_sample=(num5pl<. & vol30<. & a17<. & female<. & white<. & frosh<. & married<. & hispanic<. & offcamp<. & memgreek<. & year1993<. & gpa_a<. & fcol<. & mcol<. & norel<. & 
pub_pri<. & rural<. & commuter<. & en1<. & comp1<. & religious<. & medianinc<. & taxrate<. & unemp<. & statefip<. /*& age<=22*/);

sum num5pl vol30 dr5pl drank hsnumtimes strongmerit age female white-other frosh-gsen married hispanic offcamp memgreek gpa fcol mcol norel-othrel pub_pri rural commuter religious neast-west 
medianinc unemp taxrate if reg_sample==1;
sum num5pl vol30 dr5pl drank hsnumtimes strongmerit age female white-other frosh-gsen married hispanic offcamp memgreek gpa fcol mcol norel-othrel pub_pri rural commuter religious neast-west 
medianinc unemp taxrate if reg_sample==1 & female==0;
sum num5pl vol30 dr5pl drank hsnumtimes strongmerit age female white-other frosh-gsen married hispanic offcamp memgreek gpa fcol mcol norel-othrel pub_pri rural commuter religious neast-west 
medianinc unemp taxrate if reg_sample==1 & female==1;

/* To compare with NLSY97 */
sum num5pl vol30 dr5pl drank age female white-other married hispanic fcol mcol neast-west if reg_sample==1 & (year==1999 | year==2001);
sum num5pl vol30 dr5pl drank age female white-other married hispanic fcol mcol neast-west if female==0 & reg_sample==1 & (year==1999 | year==2001);
sum num5pl vol30 dr5pl drank age female white-other married hispanic fcol mcol neast-west if female==1 & reg_sample==1 & (year==1999 | year==2001);



/************** Additional dependent variables to look at for EER piece on education and crime ****************/
/* drdrive */
/* bngdrive */
gen marij=(daye1a==1) if daye1a<.;
gen othdrug=(daye1b==1 | daye1c==1 | daye1d==1 | daye1e==1 | daye1f==1 | daye1g==1 | daye1h==1 | daye1i==1 | daye1j==1 | daye1k==1) if daye1b<.;
gen owncrime=(drprobi==1 | drprobj==1) if (drprobi<. & drprobj<.);
replace owncrime=0 if drinkcat==0;
gen unsex=(secalc_h==1 | secalc_i==1) if (secalc_h<. | secalc_i<.);
gen supbinge=(drinkcat==3) if drinkcat<.;
gen othcrime=(secalc_c==1 | secalc_d==1) if (secalc_c<. | secalc_d<.);
gen probsex=(e13>=1 | e14>=1 | e15>=1) if e13<. & e14<. & e15<. & year==1997;
replace probsex=(e10>=1 | e11>=1 | e12>=1) if e10<. & e11<. & e12<. & year==1999;
replace probsex=(e20>=1 | e21>=1 | e22>=1) if e20<. & e21<. & e22<. & year==2001;

gen homestate=(g12==1) if g12<. & year==1997;
replace homestate=(g13==1) if g13<. & year==1999;
replace homestate=(g12a==1) if g12a<. & year==2001;

/*
sum num5pl vol30 strongmerit if reg_sample==1 & gpa>=3.4;
sum num5pl vol30 strongmerit if reg_sample==1 & female==0 & gpa>=3.4;
sum num5pl vol30 strongmerit if reg_sample==1 & female==1 & gpa>=3.4;

sum num5pl vol30 strongmerit if reg_sample==1 & gpa<3.4 & gpa>=2.7;
sum num5pl vol30 strongmerit if reg_sample==1 & female==0 & gpa<3.4 & gpa>=2.7;
sum num5pl vol30 strongmerit if reg_sample==1 & female==1 & gpa<3.4 & gpa>=2.7;

sum num5pl vol30 strongmerit if reg_sample==1 & gpa<2.7;
sum num5pl vol30 strongmerit if reg_sample==1 & female==0 & gpa<2.7;
sum num5pl vol30 strongmerit if reg_sample==1 & female==1 & gpa<2.7;
*/


/*

/* Notes: if linear time trends by state are not included, all effects weaken. However, effects for freshmen with high GPA's are still large and significant. */

/* Regressions */

foreach x in /*binged_2wk*/ dr5pl num5pl /*times_gt5*/ drank /*numtimes*/ /*drks_gt3*/ /*numdrks*/ vol30 /*allowance*/ /*hsnumtimes*/ {;

*reg `x' strongmerit /*weakmerit*/ `large' /*if age<=22*/ /*if (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip);
*outreg2 strongmerit using `x'2, excel bdec(3) tdec(3) rdec(3) replace;
*reg `x' strongmerit /*weakmerit*/ `large' if (strongstate==1 | anyaidstate==0) /*& age<=22*/, vce(cluster statefip);
*outreg2 strongmerit using `x'2, excel bdec(3) tdec(3) rdec(3) append;
*reg `x' strongmerit /*weakmerit*/ `large' if (strongstate==1 /*| anyaidstate==0*/) /*& age<=22*/, vce(cluster statefip);
*outreg2 strongmerit using `x'2, excel bdec(3) tdec(3) rdec(3) append;
*reg `x' strongmerit weakmerit `large' /*if age<=22*/ /*if (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip);
*outreg2 strongmerit weakmerit using `x'2, excel bdec(3) tdec(3) rdec(3) append;

*reg `x' strongmerit /*weakmerit*/ `large' if (gpa>=3.4) /*& (strongstate==1 | anyaidstate==0)*/ /*& age<=22*/, vce(cluster statefip);
*outreg2 strongmerit using `x'2, excel bdec(3) tdec(3) rdec(3) append;
*reg `x' strongmerit /*weakmerit*/ `large' if (gpa>=3.4) & (strongstate==1 | anyaidstate==0) /*& age<=22*/, vce(cluster statefip);
*outreg2 strongmerit using `x'2, excel bdec(3) tdec(3) rdec(3) append;
*reg `x' strongmerit /*weakmerit*/ `large' if (gpa>=3.4) & (strongstate==1 /*| anyaidstate==0*/) /*& age<=22*/, vce(cluster statefip);
*outreg2 strongmerit using `x'2, excel bdec(3) tdec(3) rdec(3) append;
*reg `x' strongmerit weakmerit `large' if (gpa>=3.4) /*& (strongstate==1 | anyaidstate==0)*/ /*& age<=22*/, vce(cluster statefip);
*outreg2 strongmerit weakmerit using `x'2, excel bdec(3) tdec(3) rdec(3) append;

*reg `x' strongmerit /*weakmerit*/ `large' if (gpa<3.4 & gpa>=3.0) /*& (strongstate==1 | anyaidstate==0)*/ /*& age<=22*/, vce(cluster statefip);
*outreg2 strongmerit using `x'2, excel bdec(3) tdec(3) rdec(3) append;
*reg `x' strongmerit /*weakmerit*/ `large' if (gpa<3.4 & gpa>=3.0) & (strongstate==1 | anyaidstate==0) /*& age<=22*/, vce(cluster statefip);
*outreg2 strongmerit using `x'2, excel bdec(3) tdec(3) rdec(3) append;
*reg `x' strongmerit /*weakmerit*/ `large' if (gpa<3.4 & gpa>=3.0) & (strongstate==1 /*| anyaidstate==0*/) /*& age<=22*/, vce(cluster statefip);
*outreg2 strongmerit using `x'2, excel bdec(3) tdec(3) rdec(3) append;
*reg `x' strongmerit weakmerit `large' if (gpa<3.4 & gpa>=3.0) /*& (strongstate==1 | anyaidstate==0)*/ /*& age<=22*/, vce(cluster statefip);
*outreg2 strongmerit weakmerit using `x'2, excel bdec(3) tdec(3) rdec(3) append;

*reg `x' strongmerit /*weakmerit*/ `large' if (gpa<3.0) /*& (strongstate==1 | anyaidstate==0)*/ /*& age<=22*/, vce(cluster statefip);
*outreg2 strongmerit using `x'2, excel bdec(3) tdec(3) rdec(3) append;
*reg `x' strongmerit /*weakmerit*/ `large' if (gpa<3.0) & (strongstate==1 | anyaidstate==0) /*& age<=22*/, vce(cluster statefip);
*outreg2 strongmerit using `x'2, excel bdec(3) tdec(3) rdec(3) append;
*reg `x' strongmerit /*weakmerit*/ `large' if (gpa<3.0) & (strongstate==1 /*| anyaidstate==0*/) /*& age<=22*/, vce(cluster statefip);
*outreg2 strongmerit using `x'2, excel bdec(3) tdec(3) rdec(3) append;
*reg `x' strongmerit weakmerit `large' if (gpa<3.0) /*& (strongstate==1 | anyaidstate==0)*/ /*& age<=22*/, vce(cluster statefip);
*outreg2 strongmerit weakmerit using `x'2, excel bdec(3) tdec(3) rdec(3) append;






reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) replace;
reg `x' strongmerit /*weakmerit*/ `small' if (strongstate==1 | anyaidstate==0) & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2a, excel bdec(3) tdec(3) rdec(3) replace;

reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if (strongstate==1 | anyaidstate==0) & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2a, excel bdec(3) tdec(3) rdec(3) append;
/*
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & south==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if (strongstate==1 | anyaidstate==0) & reg_sample==1 & south==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2a, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `large' if frosh==1 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x', excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if frosh==0 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x', excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `large' if gpa>=3.0 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x', excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if gpa<3.0 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip);
*outreg2 strongmerit using `x', excel bdec(3) tdec(3) rdec(3) append; 

reg `x' strongmerit /*weakmerit*/ `large' if gpa>=3.0 & frosh==1 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x', excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if gpa<3.0 & frosh==1 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip);
*outreg2 strongmerit using `x', excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `large' if gpa>=3.0 & frosh==0 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x', excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if gpa<3.0 & frosh==0 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip);
*outreg2 strongmerit using `x', excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `large' if gpa>=3.4 /*if (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x', excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if gpa<3.4 & gpa>=2.7 /*if (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip);
*outreg2 strongmerit using `x', excel bdec(3) tdec(3) rdec(3) append; 
reg `x' strongmerit /*weakmerit*/ `large' if gpa<2.7 /*if (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x', excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `large' if gpa>=3.4 & frosh==1 /*if (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x', excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if gpa<3.4 & gpa>=2.7 & frosh==1 /*if (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x', excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if gpa<2.7 & frosh==1 /*if (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x', excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `large' if gpa>=3.4 & frosh==0 /*if (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x', excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if gpa<3.4 & gpa>=2.7 & frosh==0 /*if (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x', excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if gpa<2.7 & frosh==0 /*if (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x', excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `large' if age<=20 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x', excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if age>20 & age<. /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip);
*outreg2 strongmerit using `x', excel bdec(3) tdec(3) rdec(3) append;
*/



reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' if (strongstate==1 | anyaidstate==0) & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2a, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' if (strongstate==1 | anyaidstate==0) & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2a, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & south==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' if (strongstate==1 | anyaidstate==0) & reg_sample==1 & south==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2a, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & strongstate==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `largepll' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepll' if (strongstate==1 | anyaidstate==0) & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2a, excel bdec(3) tdec(3) rdec(3) append;
/*
reg `x' strongmerit /*weakmerit*/ `largepl' if frosh==1 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'2, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' if frosh==0 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'2, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `largepl' if gpa>=3.0 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'2, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' if gpa<3.0 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip);
*outreg2 strongmerit using `x'2, excel bdec(3) tdec(3) rdec(3) append; 

reg `x' strongmerit /*weakmerit*/ `largepl' if gpa>=3.0 & frosh==1 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'2, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' if gpa<3.0 & frosh==1 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip);
*outreg2 strongmerit using `x'2, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `largepl' if gpa>=3.0 & frosh==0 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'2, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' if gpa<3.0 & frosh==0 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip);
*outreg2 strongmerit using `x'2, excel bdec(3) tdec(3) rdec(3) append;
*/
/*
reg `x' strongmerit /*weakmerit*/ `largepl' if gpa>=3.4 & gpa<. /*if (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip); 
outreg2 strongmerit using `x'tab3, excel bdec(3) tdec(3) rdec(3) replace;
reg `x' strongmerit /*weakmerit*/ `largepl' if gpa<3.4 & gpa>=2.7 /*if (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab3, excel bdec(3) tdec(3) rdec(3) append; 
reg `x' strongmerit /*weakmerit*/ `largepl' if gpa<2.7 /*if (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip); 
outreg2 strongmerit using `x'tab3, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `largepl' if gpa>=3.4 & gpa<. & frosh==1 /*if (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip); 
outreg2 strongmerit using `x'tab3, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' if gpa<3.4 & gpa>=2.7 & frosh==1 /*if (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip); 
outreg2 strongmerit using `x'tab3, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' if gpa<2.7 & frosh==1 /*if (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip); 
outreg2 strongmerit using `x'tab3, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `largepl' if gpa>=3.4 & gpa<. & frosh==0 /*if (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip); 
outreg2 strongmerit using `x'tab3, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' if gpa<3.4 & gpa>=2.7 & frosh==0 /*if (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip); 
outreg2 strongmerit using `x'tab3, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' if gpa<2.7 & frosh==0 /*if (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip); 
outreg2 strongmerit using `x'tab3, excel bdec(3) tdec(3) rdec(3) append;
*/
/*
reg `x' strongmerit /*weakmerit*/ `largepl' if /*age<=20*/ fcol==1 & reg_sample==1 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'2, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' if /*age>20 & age<.*/ fcol==0 & reg_sample==1 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip);
*outreg2 strongmerit using `x'2, excel bdec(3) tdec(3) rdec(3) append; 
*/



reg `x' strongmerit /*weakmerit*/ `small' if female==0 /*& (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `small' if female==0 & (strongstate==1 | anyaidstate==0) & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2a, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `large' if female==0 /*& (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if female==0 & (strongstate==1 | anyaidstate==0) & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2a, excel bdec(3) tdec(3) rdec(3) append;
/*
reg `x' strongmerit /*weakmerit*/ `large' if female==0 /*& (strongstate==1 | anyaidstate==0)*/ & reg_sample==1 & south==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if female==0 & (strongstate==1 | anyaidstate==0) & reg_sample==1 & south==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2a, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `large' if female==0 & frosh==1 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'm, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if female==0 & frosh==0 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'm, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `large' if female==0 & gpa>=3.0 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'm, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if female==0 & gpa<3.0 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip);
*outreg2 strongmerit using `x'm, excel bdec(3) tdec(3) rdec(3) append; 

reg `x' strongmerit /*weakmerit*/ `large' if female==0 & gpa>=3.0 & frosh==1 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'm, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if female==0 & gpa<3.0 & frosh==1 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip);
*outreg2 strongmerit using `x'm, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `large' if female==0 & gpa>=3.0 & frosh==0 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'm, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if female==0 & gpa<3.0 & frosh==0 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip);
*outreg2 strongmerit using `x'm, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `large' if female==0 & gpa>=3.4 /*if (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'm, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if female==0 & gpa<3.4 & gpa>=2.7 /*if (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip);
*outreg2 strongmerit using `x'm, excel bdec(3) tdec(3) rdec(3) append; 
reg `x' strongmerit /*weakmerit*/ `large' if female==0 & gpa<2.7 /*if (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'm, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `large' if female==0 & gpa>=3.4 & frosh==1 /*if (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'm, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if female==0 & gpa<3.4 & gpa>=2.7 & frosh==1 /*if (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'm, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if female==0 & gpa<2.7 & frosh==1 /*if (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'm, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `large' if female==0 & gpa>=3.4 & frosh==0 /*if (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'm, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if female==0 & gpa<3.4 & gpa>=2.7 & frosh==0 /*if (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'm, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if female==0 & gpa<2.7 & frosh==0 /*if (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'm, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `large' if female==0 & age<=20 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'm, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if female==0 & age>20 & age<. /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip);
*outreg2 strongmerit using `x'm, excel bdec(3) tdec(3) rdec(3) append; 
*/



reg `x' strongmerit /*weakmerit*/ `largep' if female==0 /*& (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' if female==0 & (strongstate==1 | anyaidstate==0) & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2a, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `largepl' if female==0 /*& (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' if female==0 & (strongstate==1 | anyaidstate==0) & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2a, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `largep' if female==0 /*& (strongstate==1 | anyaidstate==0)*/ & reg_sample==1 & south==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' if female==0 & (strongstate==1 | anyaidstate==0) & reg_sample==1 & south==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2a, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & strongstate==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `largepll' if female==0 /*& (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepll' if female==0 & (strongstate==1 | anyaidstate==0) & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2a, excel bdec(3) tdec(3) rdec(3) append;
/*
reg `x' strongmerit /*weakmerit*/ `largepl' if female==0 & frosh==1 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'2m, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' if female==0 & frosh==0 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'2m, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `largepl' if female==0 & gpa>=3.0 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'2m, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' if female==0 & gpa<3.0 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip);
*outreg2 strongmerit using `x'2m, excel bdec(3) tdec(3) rdec(3) append; 

reg `x' strongmerit /*weakmerit*/ `largepl' if female==0 & gpa>=3.0 & frosh==1 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'2m, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' if female==0 & gpa<3.0 & frosh==1 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip);
*outreg2 strongmerit using `x'2m, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `largepl' if female==0 & gpa>=3.0 & frosh==0 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'2m, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' if female==0 & gpa<3.0 & frosh==0 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip);
*outreg2 strongmerit using `x'2m, excel bdec(3) tdec(3) rdec(3) append;
*/
/*
reg `x' strongmerit /*weakmerit*/ `largepl' if female==0 & gpa>=3.4 & gpa<. /*if (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip); 
outreg2 strongmerit using `x'tab3, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' if female==0 & gpa<3.4 & gpa>=2.7 /*if (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab3, excel bdec(3) tdec(3) rdec(3) append; 
reg `x' strongmerit /*weakmerit*/ `largepl' if female==0 & gpa<2.7 /*if (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip); 
outreg2 strongmerit using `x'tab3, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `largepl' if female==0 & gpa>=3.4 & gpa<. & frosh==1 /*if (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip); 
outreg2 strongmerit using `x'tab3, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' if female==0 & gpa<3.4 & gpa>=2.7 & frosh==1 /*if (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip); 
outreg2 strongmerit using `x'tab3, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' if female==0 & gpa<2.7 & frosh==1 /*if (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip); 
outreg2 strongmerit using `x'tab3, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `largepl' if female==0 & gpa>=3.4 & gpa<. & frosh==0 /*if (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip); 
outreg2 strongmerit using `x'tab3, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' if female==0 & gpa<3.4 & gpa>=2.7 & frosh==0 /*if (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip); 
outreg2 strongmerit using `x'tab3, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' if female==0 & gpa<2.7 & frosh==0 /*if (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip); 
outreg2 strongmerit using `x'tab3, excel bdec(3) tdec(3) rdec(3) append;
*/
/*
reg `x' strongmerit /*weakmerit*/ `largepl' if female==0 & /*age<=20*/ fcol==1 & reg_sample==1 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'2m, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' if female==0 & /*age>20 & age<.*/ fcol==0 & reg_sample==1 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip);
*outreg2 strongmerit using `x'2m, excel bdec(3) tdec(3) rdec(3) append;
*/



reg `x' strongmerit /*weakmerit*/ `small' if female==1 /*& (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `small' if female==1 & (strongstate==1 | anyaidstate==0) & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2a, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `large' if female==1 /*& (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if female==1 & (strongstate==1 | anyaidstate==0) & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2a, excel bdec(3) tdec(3) rdec(3) append;
/*
reg `x' strongmerit /*weakmerit*/ `large' if female==1 /*& (strongstate==1 | anyaidstate==0)*/ & reg_sample==1 & south==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if female==1 & (strongstate==1 | anyaidstate==0) & reg_sample==1 & south==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2a, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `large' if female==1 & frosh==1 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'f, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if female==1 & frosh==0 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'f, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `large' if female==1 & gpa>=3.0 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'f, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if female==1 & gpa<3.0 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip);
*outreg2 strongmerit using `x'f, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `large' if female==1 & gpa>=3.0 & frosh==1 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'f, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if female==1 & gpa<3.0 & frosh==1 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip);
*outreg2 strongmerit using `x'f, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `large' if female==1 & gpa>=3.0 & frosh==0 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'f, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if female==1 & gpa<3.0 & frosh==0 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip);
*outreg2 strongmerit using `x'f, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `large' if female==1 & gpa>=3.4 /*if (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'f, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if female==1 & gpa<3.4 & gpa>=2.7 /*if (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip);
*outreg2 strongmerit using `x'f, excel bdec(3) tdec(3) rdec(3) append; 
reg `x' strongmerit /*weakmerit*/ `large' if female==1 & gpa<2.7 /*if (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'f, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `large' if female==1 & gpa>=3.4 & frosh==1 /*if (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'f, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if female==1 & gpa<3.4 & gpa>=2.7 & frosh==1 /*if (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'f, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if female==1 & gpa<2.7 & frosh==1 /*if (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'f, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `large' if female==1 & gpa>=3.4 & frosh==0 /*if (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'f, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if female==1 & gpa<3.4 & gpa>=2.7 & frosh==0 /*if (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'f, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if female==1 & gpa<2.7 & frosh==0 /*if (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'f, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `large' if female==1 & age<=20 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'f, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' if female==1 & age>20 & age<. /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip);
*outreg2 strongmerit using `x'f, excel bdec(3) tdec(3) rdec(3) append;
*/



reg `x' strongmerit /*weakmerit*/ `largep' if female==1 /*& (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' if female==1 & (strongstate==1 | anyaidstate==0) & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2a, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `largepl' if female==1 /*& (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' if female==1 & (strongstate==1 | anyaidstate==0) & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2a, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `largep' if female==1 /*& (strongstate==1 | anyaidstate==0)*/ & reg_sample==1 & south==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' if female==1 & (strongstate==1 | anyaidstate==0) & reg_sample==1 & south==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2a, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & strongstate==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `largepll' if female==1 /*& (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepll' if female==1 & (strongstate==1 | anyaidstate==0) & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab2a, excel bdec(3) tdec(3) rdec(3) append;
/*
reg `x' strongmerit /*weakmerit*/ `largepl' if female==1 & frosh==1 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'2f, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' if female==1 & frosh==0 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'2f, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `largepl' if female==1 & gpa>=3.0 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'2f, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' if female==1 & gpa<3.0 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip);
*outreg2 strongmerit using `x'2f, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `largepl' if female==1 & gpa>=3.0 & frosh==1 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'2f, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' if female==1 & gpa<3.0 & frosh==1 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip);
*outreg2 strongmerit using `x'2f, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `largepl' if female==1 & gpa>=3.0 & frosh==0 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'2f, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' if female==1 & gpa<3.0 & frosh==0 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip);
*outreg2 strongmerit using `x'2f, excel bdec(3) tdec(3) rdec(3) append;
*/
/*
reg `x' strongmerit /*weakmerit*/ `largepl' if female==1 & gpa>=3.4 & gpa<. /*if (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip); 
outreg2 strongmerit using `x'tab3, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' if female==1 & gpa<3.4 & gpa>=2.7 /*if (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'tab3, excel bdec(3) tdec(3) rdec(3) append; 
reg `x' strongmerit /*weakmerit*/ `largepl' if female==1 & gpa<2.7 /*if (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip); 
outreg2 strongmerit using `x'tab3, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `largepl' if female==1 & gpa>=3.4 & gpa<. & frosh==1 /*if (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip); 
outreg2 strongmerit using `x'tab3, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' if female==1 & gpa<3.4 & gpa>=2.7 & frosh==1 /*if (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip); 
outreg2 strongmerit using `x'tab3, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' if female==1 & gpa<2.7 & frosh==1 /*if (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip); 
outreg2 strongmerit using `x'tab3, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `largepl' if female==1 & gpa>=3.4 & gpa<. & frosh==0 /*if (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip); 
outreg2 strongmerit using `x'tab3, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' if female==1 & gpa<3.4 & gpa>=2.7 & frosh==0 /*if (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip); 
outreg2 strongmerit using `x'tab3, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' if female==1 & gpa<2.7 & frosh==0 /*if (strongstate==1 | anyaidstate==0)*/ & reg_sample==1, vce(cluster statefip); 
outreg2 strongmerit using `x'tab3, excel bdec(3) tdec(3) rdec(3) append;
*/
/*
reg `x' strongmerit /*weakmerit*/ `largepl' if female==1 & /*age<=20*/ fcol==1 & reg_sample==1 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip); 
*outreg2 strongmerit using `x'2f, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' if female==1 & /*age>20 & age<.*/ fcol==0 & reg_sample==1 /*& (strongstate==1 | anyaidstate==0)*/, vce(cluster statefip);
*outreg2 strongmerit using `x'2f, excel bdec(3) tdec(3) rdec(3) append;
*/
  
  
  

};



reg num5pl strongmerit /*weakmerit*/ `largep' if female==0 & reg_sample==1 /*& (strongstate==1 | anyaidstate==0)*/;
estimates store men;
reg num5pl strongmerit /*weakmerit*/ `largep' if female==1 & reg_sample==1 /*& (strongstate==1 | anyaidstate==0)*/;
estimates store women;

suest men women, vce(cluster statefip);
test [men_mean]strongmerit=[women_mean]strongmerit;
test [men_mean=women_mean];

reg vol30 strongmerit /*weakmerit*/ `largep' if female==0 & reg_sample==1 /*& (strongstate==1 | anyaidstate==0)*/;
estimates store men;
reg vol30 strongmerit /*weakmerit*/ `largep' if female==1 & reg_sample==1 /*& (strongstate==1 | anyaidstate==0)*/;
estimates store women;

suest men women, vce(cluster statefip);
test [men_mean]strongmerit=[women_mean]strongmerit;
test [men_mean=women_mean];

/*
/*
reg hsnumtimes /*hsstrong*/ strongmerit /*hsweak*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1, vce(cluster statefip);
outreg2 /*hsstrong*/ strongmerit using hsdrink, excel bdec(3) tdec(3) rdec(3) replace;
reg hsnumtimes /*hsstrong*/ strongmerit /*hsweak*/ `small' if (strongstate==1 | anyaidstate==0) & reg_sample==1, vce(cluster statefip);
outreg2 /*hsstrong*/ strongmerit using hsdrink, excel bdec(3) tdec(3) rdec(3) append;
reg hsnumtimes /*hsstrong*/ strongmerit /*hsweak*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1, vce(cluster statefip);
outreg2 /*hsstrong*/ strongmerit using hsdrink, excel bdec(3) tdec(3) rdec(3) append;
reg hsnumtimes /*hsstrong*/ strongmerit /*hsweak*/ `large' if (strongstate==1 | anyaidstate==0) & reg_sample==1, vce(cluster statefip);
outreg2 /*hsstrong*/ strongmerit using hsdrink, excel bdec(3) tdec(3) rdec(3) append;
*/
reg hsnumtimes /*hsstrong*/ strongmerit /*hsweak*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1, vce(cluster statefip);
outreg2 /*hsstrong*/ strongmerit using hsdrink, excel bdec(3) tdec(3) rdec(3) append;
reg hsnumtimes /*hsstrong*/ strongmerit /*hsweak*/ `largep' if (strongstate==1 | anyaidstate==0) & reg_sample==1, vce(cluster statefip);
outreg2 /*hsstrong*/ strongmerit using hsdrink, excel bdec(3) tdec(3) rdec(3) append;

/*
reg hsnumtimes /*hsstrong*/ strongmerit /*hsweak*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1, vce(cluster statefip);
outreg2 /*hsstrong*/ strongmerit using hsdrink, excel bdec(3) tdec(3) rdec(3) append;
reg hsnumtimes /*hsstrong*/ strongmerit /*hsweak*/ `small' if female==0 & (strongstate==1 | anyaidstate==0) & reg_sample==1, vce(cluster statefip);
outreg2 /*hsstrong*/ strongmerit using hsdrink, excel bdec(3) tdec(3) rdec(3) append;
reg hsnumtimes /*hsstrong*/ strongmerit /*hsweak*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1, vce(cluster statefip);
outreg2 /*hsstrong*/ strongmerit using hsdrink, excel bdec(3) tdec(3) rdec(3) append;
reg hsnumtimes /*hsstrong*/ strongmerit /*hsweak*/ `large' if female==0 & (strongstate==1 | anyaidstate==0) & reg_sample==1, vce(cluster statefip);
outreg2 /*hsstrong*/ strongmerit using hsdrink, excel bdec(3) tdec(3) rdec(3) append;
*/
reg hsnumtimes /*hsstrong*/ strongmerit /*hsweak*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1, vce(cluster statefip);
outreg2 /*hsstrong*/ strongmerit using hsdrink, excel bdec(3) tdec(3) rdec(3) append;
reg hsnumtimes /*hsstrong*/ strongmerit /*hsweak*/ `largep' if female==0 & (strongstate==1 | anyaidstate==0) & reg_sample==1, vce(cluster statefip);
outreg2 /*hsstrong*/ strongmerit using hsdrink, excel bdec(3) tdec(3) rdec(3) append;

/*
reg hsnumtimes /*hsstrong*/ strongmerit /*hsweak*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1, vce(cluster statefip);
outreg2 /*hsstrong*/ strongmerit using hsdrink, excel bdec(3) tdec(3) rdec(3) append;
reg hsnumtimes /*hsstrong*/ strongmerit /*hsweak*/ `small' if female==1 & (strongstate==1 | anyaidstate==0) & reg_sample==1, vce(cluster statefip);
outreg2 /*hsstrong*/ strongmerit using hsdrink, excel bdec(3) tdec(3) rdec(3) append;
reg hsnumtimes /*hsstrong*/ strongmerit /*hsweak*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1, vce(cluster statefip);
outreg2 /*hsstrong*/ strongmerit using hsdrink, excel bdec(3) tdec(3) rdec(3) append;
reg hsnumtimes /*hsstrong*/ strongmerit /*hsweak*/ `large' if female==1 & (strongstate==1 | anyaidstate==0) & reg_sample==1, vce(cluster statefip);
outreg2 /*hsstrong*/ strongmerit using hsdrink, excel bdec(3) tdec(3) rdec(3) append;
*/
reg hsnumtimes /*hsstrong*/ strongmerit /*hsweak*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1, vce(cluster statefip);
outreg2 /*hsstrong*/ strongmerit using hsdrink, excel bdec(3) tdec(3) rdec(3) append;
reg hsnumtimes /*hsstrong*/ strongmerit /*hsweak*/ `largep' if female==1 & (strongstate==1 | anyaidstate==0) & reg_sample==1, vce(cluster statefip);
outreg2 /*hsstrong*/ strongmerit using hsdrink, excel bdec(3) tdec(3) rdec(3) append;
*/





/* Examining how other factors change with merit aid */
/* "work" and "allowance" are missing in 1993 */
/*
foreach x in white pub_pri /*south*/ female gpa gstud memgreek fcol mcol work allowance {;

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1, vce(cluster statefip);
reg `x' strongmerit /*weakmerit*/ `smallpl' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1, vce(cluster statefip);

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1, vce(cluster statefip);
reg `x' strongmerit /*weakmerit*/ `smallpl' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1, vce(cluster statefip);

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1, vce(cluster statefip);
reg `x' strongmerit /*weakmerit*/ `smallpl' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1, vce(cluster statefip);

};
*/


gen bothcol=(mcol==1 & fcol==1);

reg female strongmerit a17-a25 /*female*/ white-other frosh-gsen hispanic year1993-year2005 fcol mcol norel-othrel medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using tab3, excel bdec(3) tdec(3) rdec(3) replace;
reg age strongmerit /*a17-a25*/ female white-other frosh-gsen hispanic year1993-year2005 fcol mcol norel-othrel medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using tab3, excel bdec(3) tdec(3) rdec(3) append;
reg white strongmerit a17-a25 female /*white-other*/ frosh-gsen hispanic year1993-year2005 fcol mcol norel-othrel medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using tab3, excel bdec(3) tdec(3) rdec(3) append;
reg fcol strongmerit a17-a25 female white-other frosh-gsen hispanic year1993-year2005 /*fcol*/ mcol norel-othrel medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using tab3, excel bdec(3) tdec(3) rdec(3) append;
reg mcol strongmerit a17-a25 female white-other frosh-gsen hispanic year1993-year2005 fcol /*mcol*/ norel-othrel medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using tab3, excel bdec(3) tdec(3) rdec(3) append;
reg bothcol strongmerit a17-a25 female white-other frosh-gsen hispanic year1993-year2005 /*fcol mcol*/ norel-othrel medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using tab3, excel bdec(3) tdec(3) rdec(3) append;
reg norel strongmerit a17-a25 female white-other frosh-gsen hispanic year1993-year2005 fcol mcol /*norel-othrel*/ medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using tab3, excel bdec(3) tdec(3) rdec(3) append;
reg hsnumtimes strongmerit a17-a25 female white-other frosh-gsen hispanic year1993-year2005 fcol mcol norel-othrel medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using tab3, excel bdec(3) tdec(3) rdec(3) append;


reg age strongmerit /*a17-a25*/ female white-other frosh-gsen hispanic year1993-year2005 fcol mcol norel-othrel medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 if female==0 & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using tab3, excel bdec(3) tdec(3) rdec(3) append;
reg white strongmerit a17-a25 female /*white-other*/ frosh-gsen hispanic year1993-year2005 fcol mcol norel-othrel medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 if female==0 & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using tab3, excel bdec(3) tdec(3) rdec(3) append;
reg fcol strongmerit a17-a25 female white-other frosh-gsen hispanic year1993-year2005 /*fcol*/ mcol norel-othrel medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 if female==0 & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using tab3, excel bdec(3) tdec(3) rdec(3) append;
reg mcol strongmerit a17-a25 female white-other frosh-gsen hispanic year1993-year2005 fcol /*mcol*/ norel-othrel medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 if female==0 & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using tab3, excel bdec(3) tdec(3) rdec(3) append;
reg bothcol strongmerit a17-a25 female white-other frosh-gsen hispanic year1993-year2005 /*fcol mcol*/ norel-othrel medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 if female==0 & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using tab3, excel bdec(3) tdec(3) rdec(3) append;
reg norel strongmerit a17-a25 female white-other frosh-gsen hispanic year1993-year2005 fcol mcol /*norel-othrel*/ medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 if female==0 & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using tab3, excel bdec(3) tdec(3) rdec(3) append;
reg hsnumtimes strongmerit a17-a25 female white-other frosh-gsen hispanic year1993-year2005 fcol mcol norel-othrel medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 if female==0 & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using tab3, excel bdec(3) tdec(3) rdec(3) append;


reg age strongmerit /*a17-a25*/ female white-other frosh-gsen hispanic year1993-year2005 fcol mcol norel-othrel medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 if female==1 & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using tab3, excel bdec(3) tdec(3) rdec(3) append;
reg white strongmerit a17-a25 female /*white-other*/ frosh-gsen hispanic year1993-year2005 fcol mcol norel-othrel medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 if female==1 & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using tab3, excel bdec(3) tdec(3) rdec(3) append;
reg fcol strongmerit a17-a25 female white-other frosh-gsen hispanic year1993-year2005 /*fcol*/ mcol norel-othrel medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 if female==1 & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using tab3, excel bdec(3) tdec(3) rdec(3) append;
reg mcol strongmerit a17-a25 female white-other frosh-gsen hispanic year1993-year2005 fcol /*mcol*/ norel-othrel medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 if female==1 & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using tab3, excel bdec(3) tdec(3) rdec(3) append;
reg bothcol strongmerit a17-a25 female white-other frosh-gsen hispanic year1993-year2005 /*fcol mcol*/ norel-othrel medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 if female==1 & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using tab3, excel bdec(3) tdec(3) rdec(3) append;
reg norel strongmerit a17-a25 female white-other frosh-gsen hispanic year1993-year2005 fcol mcol /*norel-othrel*/ medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 if female==1 & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using tab3, excel bdec(3) tdec(3) rdec(3) append;
reg hsnumtimes strongmerit a17-a25 female white-other frosh-gsen hispanic year1993-year2005 fcol mcol norel-othrel medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 if female==1 & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using tab3, excel bdec(3) tdec(3) rdec(3) append;



/*

reg white strongmerit `small' if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using otherchars, excel bdec(3) tdec(3) rdec(3) replace;
reg white strongmerit a17-a25 female /*white-other*/ frosh-gsen married hispanic offcamp memgreek year1993-year2005 gpa_a-gpa_na /*hsnumtimes OR hstimesdrk_mo_0-hstimesdrk_mo_40pl*/ fcol mcol norel-othrel 
pub_pri rural commuter en1-en4 comp1-comp8 religious medianinc taxrate unemp s1-s41 /*cid1-cid144*/ /*t1-t55 neast1993-west2005*/ if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using otherchars, excel bdec(3) tdec(3) rdec(3) append;
reg white strongmerit a17-a25 female /*white-other*/ frosh-gsen married hispanic offcamp memgreek year1993-year2005 gpa_a-gpa_na /*hsnumtimes OR hstimesdrk_mo_0-hstimesdrk_mo_40pl*/ fcol mcol norel-othrel 
pub_pri rural commuter en1-en4 comp1-comp8 religious medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 neast1993-west2005 if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using otherchars, excel bdec(3) tdec(3) rdec(3) append;

reg female strongmerit `small' if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using otherchars, excel bdec(3) tdec(3) rdec(3) append;
reg female strongmerit a17-a25 /*female*/ white-other frosh-gsen married hispanic offcamp memgreek year1993-year2005 gpa_a-gpa_na /*hsnumtimes OR hstimesdrk_mo_0-hstimesdrk_mo_40pl*/ fcol mcol norel-othrel 
pub_pri rural commuter en1-en4 comp1-comp8 religious medianinc taxrate unemp s1-s41 /*cid1-cid144*/ /*t1-t55 neast1993-west2005*/ if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using otherchars, excel bdec(3) tdec(3) rdec(3) append;
reg female strongmerit a17-a25 /*female*/ white-other frosh-gsen married hispanic offcamp memgreek year1993-year2005 gpa_a-gpa_na /*hsnumtimes OR hstimesdrk_mo_0-hstimesdrk_mo_40pl*/ fcol mcol norel-othrel 
pub_pri rural commuter en1-en4 comp1-comp8 religious medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 neast1993-west2005 if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using otherchars, excel bdec(3) tdec(3) rdec(3) append;
/*
reg gpa strongmerit `small' if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using otherchars, excel bdec(3) tdec(3) rdec(3) append;
reg gpa strongmerit a17-a25 female white-other frosh-gsen married hispanic offcamp memgreek year1993-year2005 /*gpa_a-gpa_na*/ /*hsnumtimes OR hstimesdrk_mo_0-hstimesdrk_mo_40pl*/ fcol mcol norel-othrel 
pub_pri rural commuter en1-en4 comp1-comp8 religious medianinc taxrate unemp s1-s41 /*cid1-cid144*/ /*t1-t55 neast1993-west2005*/ if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using otherchars, excel bdec(3) tdec(3) rdec(3) append;
reg gpa strongmerit a17-a25 female white-other frosh-gsen married hispanic offcamp memgreek year1993-year2005 /*gpa_a-gpa_na*/ /*hsnumtimes OR hstimesdrk_mo_0-hstimesdrk_mo_40pl*/ fcol mcol norel-othrel 
pub_pri rural commuter en1-en4 comp1-comp8 religious medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 neast1993-west2005 if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using otherchars, excel bdec(3) tdec(3) rdec(3) append;
*/
reg gpa strongmerit `small' if reg_sample==1 & gpa<., vce(cluster statefip);
outreg2 strongmerit using otherchars, excel bdec(3) tdec(3) rdec(3) append;
reg gpa strongmerit a17-a25 female white-other frosh-gsen married hispanic offcamp memgreek year1993-year2005 /*gpa_a-gpa_na*/ /*hsnumtimes OR hstimesdrk_mo_0-hstimesdrk_mo_40pl*/ fcol mcol norel-othrel 
pub_pri rural commuter en1-en4 comp1-comp8 religious medianinc taxrate unemp s1-s41 /*cid1-cid144*/ /*t1-t55 neast1993-west2005*/ if reg_sample==1 & gpa<., vce(cluster statefip);
outreg2 strongmerit using otherchars, excel bdec(3) tdec(3) rdec(3) append;
reg gpa strongmerit a17-a25 female white-other frosh-gsen married hispanic offcamp memgreek year1993-year2005 /*gpa_a-gpa_na*/ /*hsnumtimes OR hstimesdrk_mo_0-hstimesdrk_mo_40pl*/ fcol mcol norel-othrel 
pub_pri rural commuter en1-en4 comp1-comp8 religious medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 neast1993-west2005 if reg_sample==1 & gpa<., vce(cluster statefip);
outreg2 strongmerit using otherchars, excel bdec(3) tdec(3) rdec(3) append;

gen passing=(gpa>=2.7) if gpa<.;
reg passing strongmerit `small' if reg_sample==1 & gpa<., vce(cluster statefip);
outreg2 strongmerit using otherchars, excel bdec(3) tdec(3) rdec(3) append;
reg passing strongmerit a17-a25 female white-other frosh-gsen married hispanic offcamp memgreek year1993-year2005 /*gpa_a-gpa_na*/ /*hsnumtimes OR hstimesdrk_mo_0-hstimesdrk_mo_40pl*/ fcol mcol norel-othrel 
pub_pri rural commuter en1-en4 comp1-comp8 religious medianinc taxrate unemp s1-s41 /*cid1-cid144*/ /*t1-t55 neast1993-west2005*/ if reg_sample==1 & gpa<., vce(cluster statefip);
outreg2 strongmerit using otherchars, excel bdec(3) tdec(3) rdec(3) append;
reg passing strongmerit a17-a25 female white-other frosh-gsen married hispanic offcamp memgreek year1993-year2005 /*gpa_a-gpa_na*/ /*hsnumtimes OR hstimesdrk_mo_0-hstimesdrk_mo_40pl*/ fcol mcol norel-othrel 
pub_pri rural commuter en1-en4 comp1-comp8 religious medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 neast1993-west2005 if reg_sample==1 & gpa<., vce(cluster statefip);
outreg2 strongmerit using otherchars, excel bdec(3) tdec(3) rdec(3) append;
/*
gen onecol=(mcol==1 | fcol==1);
reg fcol strongmerit `small' if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using otherchars, excel bdec(3) tdec(3) rdec(3) append;
reg fcol strongmerit a17-a25 female white-other frosh-gsen married hispanic offcamp memgreek year1993-year2005 gpa_a-gpa_na /*hsnumtimes OR hstimesdrk_mo_0-hstimesdrk_mo_40pl*/ /*fcol mcol*/ norel-othrel 
pub_pri rural commuter en1-en4 comp1-comp8 religious medianinc taxrate unemp s1-s41 /*cid1-cid144*/ /*t1-t55 neast1993-west2005*/ if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using otherchars, excel bdec(3) tdec(3) rdec(3) append;
reg fcol strongmerit a17-a25 female white-other frosh-gsen married hispanic offcamp memgreek year1993-year2005 gpa_a-gpa_na /*hsnumtimes OR hstimesdrk_mo_0-hstimesdrk_mo_40pl*/ /*fcol mcol*/ norel-othrel 
pub_pri rural commuter en1-en4 comp1-comp8 religious medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 neast1993-west2005 if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using otherchars, excel bdec(3) tdec(3) rdec(3) append;
*/
reg mcol strongmerit `small' if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using otherchars, excel bdec(3) tdec(3) rdec(3) append;
reg mcol strongmerit a17-a25 female white-other frosh-gsen married hispanic offcamp memgreek year1993-year2005 gpa_a-gpa_na /*hsnumtimes OR hstimesdrk_mo_0-hstimesdrk_mo_40pl*/ /*fcol mcol*/ norel-othrel 
pub_pri rural commuter en1-en4 comp1-comp8 religious medianinc taxrate unemp s1-s41 /*cid1-cid144*/ /*t1-t55 neast1993-west2005*/ if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using otherchars, excel bdec(3) tdec(3) rdec(3) append;
reg mcol strongmerit a17-a25 female white-other frosh-gsen married hispanic offcamp memgreek year1993-year2005 gpa_a-gpa_na /*hsnumtimes OR hstimesdrk_mo_0-hstimesdrk_mo_40pl*/ /*fcol mcol*/ norel-othrel 
pub_pri rural commuter en1-en4 comp1-comp8 religious medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 neast1993-west2005 if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using otherchars, excel bdec(3) tdec(3) rdec(3) append;

reg memgreek strongmerit `small' if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using otherchars, excel bdec(3) tdec(3) rdec(3) append;
reg memgreek strongmerit a17-a25 female white-other frosh-gsen married hispanic offcamp /*memgreek*/ year1993-year2005 gpa_a-gpa_na /*hsnumtimes OR hstimesdrk_mo_0-hstimesdrk_mo_40pl*/ fcol mcol norel-othrel 
pub_pri rural commuter en1-en4 comp1-comp8 religious medianinc taxrate unemp s1-s41 /*cid1-cid144*/ /*t1-t55 neast1993-west2005*/ if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using otherchars, excel bdec(3) tdec(3) rdec(3) append;
reg memgreek strongmerit a17-a25 female white-other frosh-gsen married hispanic offcamp /*memgreek*/ year1993-year2005 gpa_a-gpa_na /*hsnumtimes OR hstimesdrk_mo_0-hstimesdrk_mo_40pl*/ fcol mcol norel-othrel 
pub_pri rural commuter en1-en4 comp1-comp8 religious medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 neast1993-west2005 if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using otherchars, excel bdec(3) tdec(3) rdec(3) append;

reg marij strongmerit `small' if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using otherchars, excel bdec(3) tdec(3) rdec(3) append;
reg marij strongmerit a17-a25 female white-other frosh-gsen married hispanic offcamp memgreek year1993-year2005 gpa_a-gpa_na /*hsnumtimes OR hstimesdrk_mo_0-hstimesdrk_mo_40pl*/ fcol mcol norel-othrel 
pub_pri rural commuter en1-en4 comp1-comp8 religious medianinc taxrate unemp s1-s41 /*cid1-cid144*/ /*t1-t55 neast1993-west2005*/ if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using otherchars, excel bdec(3) tdec(3) rdec(3) append;
reg marij strongmerit a17-a25 female white-other frosh-gsen married hispanic offcamp memgreek year1993-year2005 gpa_a-gpa_na /*hsnumtimes OR hstimesdrk_mo_0-hstimesdrk_mo_40pl*/ fcol mcol norel-othrel 
pub_pri rural commuter en1-en4 comp1-comp8 religious medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 neast1993-west2005 if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using otherchars, excel bdec(3) tdec(3) rdec(3) append;
/*
reg work strongmerit `small' if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using otherchars, excel bdec(3) tdec(3) rdec(3) append;
reg work strongmerit a17-a25 female white-other frosh-gsen married hispanic offcamp memgreek year1993-year2005 gpa_a-gpa_na /*hsnumtimes OR hstimesdrk_mo_0-hstimesdrk_mo_40pl*/ fcol mcol norel-othrel 
pub_pri rural commuter en1-en4 comp1-comp8 religious medianinc taxrate unemp s1-s41 /*cid1-cid144*/ /*t1-t55 neast1993-west2005*/ if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using otherchars, excel bdec(3) tdec(3) rdec(3) append;
reg work strongmerit a17-a25 female white-other frosh-gsen married hispanic offcamp memgreek year1993-year2005 gpa_a-gpa_na /*hsnumtimes OR hstimesdrk_mo_0-hstimesdrk_mo_40pl*/ fcol mcol norel-othrel 
pub_pri rural commuter en1-en4 comp1-comp8 religious medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 neast1993-west2005 if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using otherchars, excel bdec(3) tdec(3) rdec(3) append;

reg allowance strongmerit `small' if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using otherchars, excel bdec(3) tdec(3) rdec(3) append;
reg allowance strongmerit a17-a25 female white-other frosh-gsen married hispanic offcamp memgreek year1993-year2005 gpa_a-gpa_na /*hsnumtimes OR hstimesdrk_mo_0-hstimesdrk_mo_40pl*/ fcol mcol norel-othrel 
pub_pri rural commuter en1-en4 comp1-comp8 religious medianinc taxrate unemp s1-s41 /*cid1-cid144*/ /*t1-t55 neast1993-west2005*/ if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using otherchars, excel bdec(3) tdec(3) rdec(3) append;
reg allowance strongmerit a17-a25 female white-other frosh-gsen married hispanic offcamp memgreek year1993-year2005 gpa_a-gpa_na /*hsnumtimes OR hstimesdrk_mo_0-hstimesdrk_mo_40pl*/ fcol mcol norel-othrel 
pub_pri rural commuter en1-en4 comp1-comp8 religious medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 neast1993-west2005 if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using otherchars, excel bdec(3) tdec(3) rdec(3) append;
*/

*/



gen race_ethnicity=1 if (white==1 | asian==1);
replace race_ethnicity=2 if black==1;
replace race_ethnicity=3 if hispanic==1;

/* Examining how the results change by parental education, race, public/private, GPA, greek status, and year in school/age (all by gender) */

foreach x in /*binged_2wk*/ /*dr5pl*/ num5pl /*times_gt5*/ /*drank*/ /*numtimes*/ /*drks_gt3*/ /*numdrks*/ /*vol30*/ /*allowance*/ /*hsnumtimes*/ {;

/* Pre-determined characteristics */

reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (/*fcol==0 |*/ mcol==0), vce(cluster statefip);
outreg2 strongmerit using pared, excel bdec(3) tdec(3) rdec(3) replace;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (/*fcol==0 |*/ mcol==1), vce(cluster statefip);
outreg2 strongmerit using pared, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (/*fcol==0 |*/ mcol==0), vce(cluster statefip);
outreg2 strongmerit using pared, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (/*fcol==0 |*/ mcol==1), vce(cluster statefip);
outreg2 strongmerit using pared, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (/*fcol==0 |*/ mcol==0), vce(cluster statefip);
outreg2 strongmerit using pared, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (/*fcol==0 |*/ mcol==1), vce(cluster statefip);
outreg2 strongmerit using pared, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ a17-a25 female white-other frosh-gsen hispanic year1993-year2005 /*fcol mcol*/ norel-othrel medianinc taxrate unemp s1-s41 t1-t55 /*if (strongstate==1 | anyaidstate==0)*/ 
if reg_sample==1 & (/*fcol==0 |*/ bothcol==0), vce(cluster statefip);
outreg2 strongmerit using pared2, excel bdec(3) tdec(3) rdec(3) replace;
reg `x' strongmerit /*weakmerit*/ a17-a25 female white-other frosh-gsen hispanic year1993-year2005 /*fcol mcol*/ norel-othrel medianinc taxrate unemp s1-s41 t1-t55 /*if (strongstate==1 | anyaidstate==0)*/ 
if reg_sample==1 & (/*fcol==0 |*/ bothcol==1), vce(cluster statefip);
outreg2 strongmerit using pared2, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ a17-a25 female white-other frosh-gsen hispanic year1993-year2005 /*fcol mcol*/ norel-othrel medianinc taxrate unemp s1-s41 t1-t55 /*if (strongstate==1 | anyaidstate==0)*/ 
if female==0 & reg_sample==1 & (/*fcol==0 |*/ bothcol==0), vce(cluster statefip);
outreg2 strongmerit using pared2, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ a17-a25 female white-other frosh-gsen hispanic year1993-year2005 /*fcol mcol*/ norel-othrel medianinc taxrate unemp s1-s41 t1-t55 /*if (strongstate==1 | anyaidstate==0)*/ 
if female==0 & reg_sample==1 & (/*fcol==0 |*/ bothcol==1), vce(cluster statefip);
outreg2 strongmerit using pared2, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ a17-a25 female white-other frosh-gsen hispanic year1993-year2005 /*fcol mcol*/ norel-othrel medianinc taxrate unemp s1-s41 t1-t55 /*if (strongstate==1 | anyaidstate==0)*/ 
if female==1 & reg_sample==1 & (/*fcol==0 |*/ bothcol==0), vce(cluster statefip);
outreg2 strongmerit using pared2, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ a17-a25 female white-other frosh-gsen hispanic year1993-year2005 /*fcol mcol*/ norel-othrel medianinc taxrate unemp s1-s41 t1-t55 /*if (strongstate==1 | anyaidstate==0)*/ 
if female==1 & reg_sample==1 & (/*fcol==0 |*/ bothcol==1), vce(cluster statefip);
outreg2 strongmerit using pared2, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (fcol==0), vce(cluster statefip);
outreg2 strongmerit using pared3, excel bdec(3) tdec(3) rdec(3) replace;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (fcol==1), vce(cluster statefip);
outreg2 strongmerit using pared3, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (fcol==0), vce(cluster statefip);
outreg2 strongmerit using pared3, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (fcol==1), vce(cluster statefip);
outreg2 strongmerit using pared3, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (fcol==0), vce(cluster statefip);
outreg2 strongmerit using pared3, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (fcol==1), vce(cluster statefip);
outreg2 strongmerit using pared3, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (white==0), vce(cluster statefip);
outreg2 strongmerit using race, excel bdec(3) tdec(3) rdec(3) replace;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (white==1), vce(cluster statefip);
outreg2 strongmerit using race, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (white==0), vce(cluster statefip);
outreg2 strongmerit using race, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (white==1), vce(cluster statefip);
outreg2 strongmerit using race, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (white==0), vce(cluster statefip);
outreg2 strongmerit using race, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (white==1), vce(cluster statefip);
outreg2 strongmerit using race, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (age<21), vce(cluster statefip);
outreg2 strongmerit using age, excel bdec(3) tdec(3) rdec(3) replace;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (age>=21), vce(cluster statefip);
outreg2 strongmerit using age, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (age<21), vce(cluster statefip);
outreg2 strongmerit using age, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (age>=21), vce(cluster statefip);
outreg2 strongmerit using age, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (age<21), vce(cluster statefip);
outreg2 strongmerit using age, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (age>=21), vce(cluster statefip);
outreg2 strongmerit using age, excel bdec(3) tdec(3) rdec(3) append;

/* Endogenous characteristics */

reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (pub_pri==0), vce(cluster statefip);
outreg2 strongmerit using pubpri, excel bdec(3) tdec(3) rdec(3) replace;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (pub_pri==1), vce(cluster statefip);
outreg2 strongmerit using pubpri, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (pub_pri==0), vce(cluster statefip);
outreg2 strongmerit using pubpri, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (pub_pri==1), vce(cluster statefip);
outreg2 strongmerit using pubpri, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (pub_pri==0), vce(cluster statefip);
outreg2 strongmerit using pubpri, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (pub_pri==1), vce(cluster statefip);
outreg2 strongmerit using pubpri, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (memgreek==0), vce(cluster statefip);
outreg2 strongmerit using greek, excel bdec(3) tdec(3) rdec(3) replace;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (memgreek==1), vce(cluster statefip);
outreg2 strongmerit using greek, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (memgreek==0), vce(cluster statefip);
outreg2 strongmerit using greek, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (memgreek==1), vce(cluster statefip);
outreg2 strongmerit using greek, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (memgreek==0), vce(cluster statefip);
outreg2 strongmerit using greek, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (memgreek==1), vce(cluster statefip);
outreg2 strongmerit using greek, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (gpa<2.7), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) replace;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (gpa>=2.7 & gpa<3.4), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (gpa>=3.4 & gpa<.), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (gpa<2.7), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (gpa>=2.7 & gpa<3.4), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (gpa>=3.4 & gpa<.), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (gpa<2.7), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (gpa>=2.7 & gpa<3.4), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (gpa>=3.4 & gpa<.), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) append;

*/


/*

/* Parental education */
reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (/*fcol==0 |*/ mcol==0), vce(cluster statefip);
outreg2 strongmerit using pared, excel bdec(3) tdec(3) rdec(3) replace;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (/*fcol==0 |*/ mcol==0), vce(cluster statefip);
outreg2 strongmerit using pared, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (/*fcol==0 |*/ mcol==0), vce(cluster statefip);
outreg2 strongmerit using pared, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (/*fcol==1 &*/ mcol==1), vce(cluster statefip);
outreg2 strongmerit using pared, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (/*fcol==1 &*/ mcol==1), vce(cluster statefip);
outreg2 strongmerit using pared, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (/*fcol==1 &*/ mcol==1), vce(cluster statefip);
outreg2 strongmerit using pared, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (/*fcol==0 |*/ mcol==0), vce(cluster statefip);
outreg2 strongmerit using pared, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (/*fcol==0 |*/ mcol==0), vce(cluster statefip);
outreg2 strongmerit using pared, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (/*fcol==0 |*/ mcol==0), vce(cluster statefip);
outreg2 strongmerit using pared, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (/*fcol==1 &*/ mcol==1), vce(cluster statefip);
outreg2 strongmerit using pared, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (/*fcol==1 &*/ mcol==1), vce(cluster statefip);
outreg2 strongmerit using pared, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (/*fcol==1 &*/ mcol==1), vce(cluster statefip);
outreg2 strongmerit using pared, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (/*fcol==0 |*/ mcol==0), vce(cluster statefip);
outreg2 strongmerit using pared, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (/*fcol==0 |*/ mcol==0), vce(cluster statefip);
outreg2 strongmerit using pared, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (/*fcol==0 |*/ mcol==0), vce(cluster statefip);
outreg2 strongmerit using pared, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (/*fcol==1 &*/ mcol==1), vce(cluster statefip);
outreg2 strongmerit using pared, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (/*fcol==1 &*/ mcol==1), vce(cluster statefip);
outreg2 strongmerit using pared, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (/*fcol==1 &*/ mcol==1), vce(cluster statefip);
outreg2 strongmerit using pared, excel bdec(3) tdec(3) rdec(3) append;


/* Race */
reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (/*race_ethnicity==1*/white==1), vce(cluster statefip);
outreg2 strongmerit using race, excel bdec(3) tdec(3) rdec(3) replace;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (/*race_ethnicity==1*/white==1), vce(cluster statefip);
outreg2 strongmerit using race, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (/*race_ethnicity==1*/white==1), vce(cluster statefip);
outreg2 strongmerit using race, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (/*race_ethnicity==2 | race_ethnicity==3*/white==0), vce(cluster statefip);
outreg2 strongmerit using race, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (/*race_ethnicity==2 | race_ethnicity==3*/white==0), vce(cluster statefip);
outreg2 strongmerit using race, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (/*race_ethnicity==2 | race_ethnicity==3*/white==0), vce(cluster statefip);
outreg2 strongmerit using race, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (/*race_ethnicity==1*/white==1), vce(cluster statefip);
outreg2 strongmerit using race, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (/*race_ethnicity==1*/white==1), vce(cluster statefip);
outreg2 strongmerit using race, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (/*race_ethnicity==1*/white==1), vce(cluster statefip);
outreg2 strongmerit using race, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (/*race_ethnicity==2 | race_ethnicity==3*/white==0), vce(cluster statefip);
outreg2 strongmerit using race, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (/*race_ethnicity==2 | race_ethnicity==3*/white==0), vce(cluster statefip);
outreg2 strongmerit using race, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (/*race_ethnicity==2 | race_ethnicity==3*/white==0), vce(cluster statefip);
outreg2 strongmerit using race, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (/*race_ethnicity==1*/white==1), vce(cluster statefip);
outreg2 strongmerit using race, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (/*race_ethnicity==1*/white==1), vce(cluster statefip);
outreg2 strongmerit using race, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (/*race_ethnicity==1*/white==1), vce(cluster statefip);
outreg2 strongmerit using race, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (/*race_ethnicity==2 | race_ethnicity==3*/white==0), vce(cluster statefip);
outreg2 strongmerit using race, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (/*race_ethnicity==2 | race_ethnicity==3*/white==0), vce(cluster statefip);
outreg2 strongmerit using race, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (/*race_ethnicity==2 | race_ethnicity==3*/white==0), vce(cluster statefip);
outreg2 strongmerit using race, excel bdec(3) tdec(3) rdec(3) append;


/* Public/private */
reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (pub_pri==1), vce(cluster statefip);
outreg2 strongmerit using pubpri, excel bdec(3) tdec(3) rdec(3) replace;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (pub_pri==1), vce(cluster statefip);
outreg2 strongmerit using pubpri, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (pub_pri==1), vce(cluster statefip);
outreg2 strongmerit using pubpri, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (pub_pri==0), vce(cluster statefip);
outreg2 strongmerit using pubpri, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (pub_pri==0), vce(cluster statefip);
outreg2 strongmerit using pubpri, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (pub_pri==0), vce(cluster statefip);
outreg2 strongmerit using pubpri, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (pub_pri==1), vce(cluster statefip);
outreg2 strongmerit using pubpri, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (pub_pri==1), vce(cluster statefip);
outreg2 strongmerit using pubpri, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (pub_pri==1), vce(cluster statefip);
outreg2 strongmerit using pubpri, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (pub_pri==0), vce(cluster statefip);
outreg2 strongmerit using pubpri, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (pub_pri==0), vce(cluster statefip);
outreg2 strongmerit using pubpri, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (pub_pri==0), vce(cluster statefip);
outreg2 strongmerit using pubpri, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (pub_pri==1), vce(cluster statefip);
outreg2 strongmerit using pubpri, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (pub_pri==1), vce(cluster statefip);
outreg2 strongmerit using pubpri, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (pub_pri==1), vce(cluster statefip);
outreg2 strongmerit using pubpri, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (pub_pri==0), vce(cluster statefip);
outreg2 strongmerit using pubpri, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (pub_pri==0), vce(cluster statefip);
outreg2 strongmerit using pubpri, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (pub_pri==0), vce(cluster statefip);
outreg2 strongmerit using pubpri, excel bdec(3) tdec(3) rdec(3) append;


/* GPA */
reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (gpa>=3.4 & gpa<.), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) replace;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (gpa>=3.4 & gpa<.), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (gpa>=3.4 & gpa<.), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (gpa>=2.7 & gpa<3.4), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (gpa>=2.7 & gpa<3.4), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (gpa>=2.7 & gpa<3.4), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (gpa<2.7), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (gpa<2.7), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (gpa<2.7), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) append;


reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (gpa>=3.4 & gpa<.), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (gpa>=3.4 & gpa<.), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (gpa>=3.4 & gpa<.), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (gpa>=2.7 & gpa<3.4), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (gpa>=2.7 & gpa<3.4), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (gpa>=2.7 & gpa<3.4), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (gpa<2.7), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (gpa<2.7), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (gpa<2.7), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) append;


reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (gpa>=3.4 & gpa<.), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (gpa>=3.4 & gpa<.), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (gpa>=3.4 & gpa<.), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (gpa>=2.7 & gpa<3.4), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (gpa>=2.7 & gpa<3.4), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (gpa>=2.7 & gpa<3.4), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (gpa<2.7), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (gpa<2.7), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (gpa<2.7), vce(cluster statefip);
outreg2 strongmerit using gpa, excel bdec(3) tdec(3) rdec(3) append;


/* Greek status */
reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (memgreek==1), vce(cluster statefip);
outreg2 strongmerit using greek, excel bdec(3) tdec(3) rdec(3) replace;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (memgreek==1), vce(cluster statefip);
outreg2 strongmerit using greek, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (memgreek==1), vce(cluster statefip);
outreg2 strongmerit using greek, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (memgreek==0), vce(cluster statefip);
outreg2 strongmerit using greek, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (memgreek==0), vce(cluster statefip);
outreg2 strongmerit using greek, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (memgreek==0), vce(cluster statefip);
outreg2 strongmerit using greek, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (memgreek==1), vce(cluster statefip);
outreg2 strongmerit using greek, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (memgreek==1), vce(cluster statefip);
outreg2 strongmerit using greek, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (memgreek==1), vce(cluster statefip);
outreg2 strongmerit using greek, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (memgreek==0), vce(cluster statefip);
outreg2 strongmerit using greek, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (memgreek==0), vce(cluster statefip);
outreg2 strongmerit using greek, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (memgreek==0), vce(cluster statefip);
outreg2 strongmerit using greek, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (memgreek==1), vce(cluster statefip);
outreg2 strongmerit using greek, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (memgreek==1), vce(cluster statefip);
outreg2 strongmerit using greek, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (memgreek==1), vce(cluster statefip);
outreg2 strongmerit using greek, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (memgreek==0), vce(cluster statefip);
outreg2 strongmerit using greek, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (memgreek==0), vce(cluster statefip);
outreg2 strongmerit using greek, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (memgreek==0), vce(cluster statefip);
outreg2 strongmerit using greek, excel bdec(3) tdec(3) rdec(3) append;


/* Year in school/age */
reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (class>2), vce(cluster statefip);
outreg2 strongmerit using class, excel bdec(3) tdec(3) rdec(3) replace;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (class>2), vce(cluster statefip);
outreg2 strongmerit using class, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (class>2), vce(cluster statefip);
outreg2 strongmerit using class, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (class<=2), vce(cluster statefip);
outreg2 strongmerit using class, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (class<=2), vce(cluster statefip);
outreg2 strongmerit using class, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & (class<=2), vce(cluster statefip);
outreg2 strongmerit using class, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (class>2), vce(cluster statefip);
outreg2 strongmerit using class, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (class>2), vce(cluster statefip);
outreg2 strongmerit using class, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (class>2), vce(cluster statefip);
outreg2 strongmerit using class, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (class<=2), vce(cluster statefip);
outreg2 strongmerit using class, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (class<=2), vce(cluster statefip);
outreg2 strongmerit using class, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & (class<=2), vce(cluster statefip);
outreg2 strongmerit using class, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (class>2), vce(cluster statefip);
outreg2 strongmerit using class, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (class>2), vce(cluster statefip);
outreg2 strongmerit using class, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (class>2), vce(cluster statefip);
outreg2 strongmerit using class, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (class<=2), vce(cluster statefip);
outreg2 strongmerit using class, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (class<=2), vce(cluster statefip);
outreg2 strongmerit using class, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & (class<=2), vce(cluster statefip);
outreg2 strongmerit using class, excel bdec(3) tdec(3) rdec(3) append;



};

*/



/*
/* Including ONLY strong states */
reg num5pl strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & strongstate==1, vce(cluster statefip);
reg num5pl strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & strongstate==1, vce(cluster statefip);
reg num5pl strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & strongstate==1, vce(cluster statefip);

reg vol30 strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & strongstate==1, vce(cluster statefip);
reg vol30 strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & strongstate==1, vce(cluster statefip);
reg vol30 strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & strongstate==1, vce(cluster statefip);
*/

/*
/* These come from Sjoquist and Winters (2014) and are weighted by percentage of undergrads receiving */
gen amt=0;
replace amt=(760) if state=="FL";
replace amt=(1667) if state=="GA";
replace amt=(617) if state=="KY";
replace amt=(689) if state=="LA";
replace amt=(468) if state=="NM";
replace amt=(1085) if state=="SC";

/* These are based on Dusty's collection and are only awards per recipient (not undergrad) */
gen amt2=0;
replace amt2=(500) if state=="GA" & year==1993;
replace amt2=(1194) if state=="GA" & year==1997;
replace amt2=(1339) if state=="GA" & year==1999;
replace amt2=(1634) if state=="GA" & year==2001;

replace amt2=(0) if state=="FL" & year==1993;
replace amt2=(1549) if state=="FL" & year==1997;
replace amt2=(1665) if state=="FL" & year==1999;
replace amt2=(1893) if state=="FL" & year==2001;

replace amt2=(0) if state=="NM" & year==1993;
replace amt2=(818) if state=="NM" & year==1997;
replace amt2=(673) if state=="NM" & year==1999;
replace amt2=(866) if state=="NM" & year==2001;

replace amt2=(0) if state=="LA" & year==1993;
replace amt2=(0) if state=="LA" & year==1997;
replace amt2=(5033) if state=="LA" & year==1999;
replace amt2=(3138) if state=="LA" & year==2001;

replace amt2=(0) if state=="SC" & year==1993;
replace amt2=(0) if state=="SC" & year==1997;
replace amt2=(1270) if state=="SC" & year==1999;
replace amt2=(627) if state=="SC" & year==2001;

replace amt2=(0) if state=="KY" & year==1993;
replace amt2=(0) if state=="KY" & year==1997;
replace amt2=(458) if state=="KY" & year==1999;
replace amt2=(683) if state=="KY" & year==2001;

gen s_amt=(strongmerit)*(amt);
gen s_amt2=(strongmerit)*(amt2);

reg num5pl strongmerit s_amt /*amt*/ /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & strongstate==1, vce(cluster statefip);
reg num5pl strongmerit s_amt /*amt*/ /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & strongstate==1, vce(cluster statefip);
reg num5pl strongmerit s_amt /*amt*/ /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & strongstate==1, vce(cluster statefip);

reg num5pl amt2 /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & strongstate==1, vce(cluster statefip);
reg num5pl amt2 /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & strongstate==1, vce(cluster statefip);
reg num5pl amt2 /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & strongstate==1, vce(cluster statefip);

reg num5pl amt2 s_amt2 /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & strongstate==1, vce(cluster statefip);
reg num5pl amt2 s_amt2 /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & strongstate==1, vce(cluster statefip);
reg num5pl amt2 s_amt2 /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & strongstate==1, vce(cluster statefip);

reg num5pl amt2 /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 /*& strongstate==1*/, vce(cluster statefip);
reg num5pl amt2 /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 /*& strongstate==1*/, vce(cluster statefip);
reg num5pl amt2 /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 /*& strongstate==1*/, vce(cluster statefip);

reg num5pl amt2 s_amt2 /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 /*& strongstate==1*/, vce(cluster statefip);
reg num5pl amt2 s_amt2 /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 /*& strongstate==1*/, vce(cluster statefip);
reg num5pl amt2 s_amt2 /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 /*& strongstate==1*/, vce(cluster statefip);
*/


/*
/* Cutler's comment: define treatment relative to adoption year */

gen yrs_since=(year-aidyear+1) if /*year>=aidyear &*/ aideligible==1;
*replace yrs_since=(year-aidyear+1) if year<aidyear & aidyear<. & aideligible==0;
replace yrs_since=0 if (/*year<aidyear |*/ aideligible==0) /*aidyear==. & froshyear<.*/;

gen strong_yrs_since = yrs_since if strongstate == 1;
replace strong_yrs_since = 0 if yrs_since<. & strongstate == 0;
gen weak_yrs_since = yrs_since if strongstate == 0 & anyaidstate==1;
replace weak_yrs_since = 0 if yrs_since<. & (strongstate==1 | anyaidstate == 0);

gen s_yrs_1_3=(strong_yrs_since>=1 & strong_yrs_since<=3) if strong_yrs_since<.;
gen s_yrs_4_6=(strong_yrs_since>=4 & strong_yrs_since<=6) if strong_yrs_since<.;
gen s_yrs_7_9=(strong_yrs_since>=7 & strong_yrs_since<=9) if strong_yrs_since<.;


foreach x in /*binged_2wk*/ /*dr5pl*/ num5pl /*times_gt5*/ /*drank*/ /*numtimes*/ /*drks_gt3*/ /*numdrks*/ vol30 /*allowance*/ /*hsnumtimes*/ {;

reg `x' s_yrs_1_3-s_yrs_7_9 /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1, vce(cluster statefip);
*outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) replace;
reg `x' s_yrs_1_3-s_yrs_7_9 /*weakmerit*/ `large' if (strongstate==1 | anyaidstate==0) & reg_sample==1, vce(cluster statefip);
*outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;

reg `x' s_yrs_1_3-s_yrs_7_9 /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1, vce(cluster statefip);
*outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;
reg `x' s_yrs_1_3-s_yrs_7_9 /*weakmerit*/ `largepl' if (strongstate==1 | anyaidstate==0) & reg_sample==1, vce(cluster statefip);
*outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;

reg `x' s_yrs_1_3-s_yrs_7_9 /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1, vce(cluster statefip);
*outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) replace;
reg `x' s_yrs_1_3-s_yrs_7_9 /*weakmerit*/ `large' if female==0 & (strongstate==1 | anyaidstate==0) & reg_sample==1, vce(cluster statefip);
*outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;

reg `x' s_yrs_1_3-s_yrs_7_9 /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1, vce(cluster statefip);
*outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;
reg `x' s_yrs_1_3-s_yrs_7_9 /*weakmerit*/ `largepl' if female==0 & (strongstate==1 | anyaidstate==0) & reg_sample==1, vce(cluster statefip);
*outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;

reg `x' s_yrs_1_3-s_yrs_7_9 /*weakmerit*/ `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1, vce(cluster statefip);
*outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) replace;
reg `x' s_yrs_1_3-s_yrs_7_9 /*weakmerit*/ `large' if female==1 & (strongstate==1 | anyaidstate==0) & reg_sample==1, vce(cluster statefip);
*outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;

reg `x' s_yrs_1_3-s_yrs_7_9 /*weakmerit*/ `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1, vce(cluster statefip);
*outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;
reg `x' s_yrs_1_3-s_yrs_7_9 /*weakmerit*/ `largepl' if female==1 & (strongstate==1 | anyaidstate==0) & reg_sample==1, vce(cluster statefip);
*outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;

};
*/


/*
/*
gen cohort=(froshyear-aidyear+1) if aideligible==1;
replace cohort=0 if aideligible==0;

gen strong_cohort = cohort if strongstate == 1;
replace strong_cohort = 0 if strongstate == 0;
gen weak_cohort = cohort if strongstate == 0 & anyaidstate==1;
replace weak_cohort = 0 if (strongstate==1 | anyaidstate == 0);

gen s_coh_1_3=(strong_cohort>=1 & strong_cohort<=3) if strong_cohort<.;
gen s_coh_4_6=(strong_cohort>=4 & strong_cohort<=6) if strong_cohort<.;
gen s_coh_7_9=(strong_cohort>=7 & strong_cohort<=9) if strong_cohort<.;
*/

/* I will want to take out year effects (year, age, and cohort effects are almost collinear) */
/* I will want to do it with and without Georgia (since they are an early adopter) */
replace aidyear=1998 if (strongstate==0 | aidyear>2001);
gen cohort = (froshyear-aidyear+1);

gen coh1 = (cohort>=-9 & cohort<=-7) if cohort<.;
gen coh2 = (cohort>=-6 & cohort<=-4) if cohort<.;
gen coh3 = (cohort>=-3 & cohort<=-1) if cohort<.;
gen coh4 = (cohort==0) if cohort<.;
gen coh5 = (cohort>=1 & cohort<=3) if cohort<.;
gen coh6 = (cohort>=4 & cohort<=6) if cohort<.;

foreach x in coh1 coh2 coh3 coh4 coh5 coh6 /*coh7 coh8*/ {;
gen s`x' = strongstate*(`x');
};

sum coh1-coh6 if reg_sample==1;
sum scoh1-scoh6 if reg_sample==1;

local states s1-s41;

foreach x in /*binged_2wk*/ /*dr5pl*/ num5pl /*times_gt5*/ /*drank*/ /*numtimes*/ /*drks_gt3*/ /*numdrks*/ vol30 /*allowance*/ /*hsnumtimes*/ {;

reg `x' coh1 coh2 coh3 coh5 coh6 /*coh7 coh8*/ scoh1 scoh2 scoh3 scoh5 scoh6 /*scoh7 scoh8*/ `states' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & state!="GA", vce(cluster statefip);
reg `x' coh1 coh2 coh3 coh5 coh6 /*coh7 coh8*/ scoh1 scoh2 scoh3 scoh5 scoh6 /*scoh7 scoh8*/ `states' if (strongstate==1 | anyaidstate==0) & reg_sample==1 & state!="GA", vce(cluster statefip);

reg `x' coh1 coh2 coh3 coh5 coh6 /*coh7 coh8*/ scoh1 scoh2 scoh3 scoh5 scoh6 /*scoh7 scoh8*/ `states' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & state!="GA", vce(cluster statefip);
reg `x' coh1 coh2 coh3 coh5 coh6 /*coh7 coh8*/ scoh1 scoh2 scoh3 scoh5 scoh6 /*scoh7 scoh8*/ `states' if female==0 & (strongstate==1 | anyaidstate==0) & reg_sample==1 & state!="GA", vce(cluster statefip);

reg `x' coh1 coh2 coh3 coh5 coh6 /*coh7 coh8*/ scoh1 scoh2 scoh3 scoh5 scoh6 /*scoh7 scoh8*/ `states' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & state!="GA", vce(cluster statefip);
reg `x' coh1 coh2 coh3 coh5 coh6 /*coh7 coh8*/ scoh1 scoh2 scoh3 scoh5 scoh6 /*scoh7 scoh8*/ `states' if female==1 & (strongstate==1 | anyaidstate==0) & reg_sample==1 & state!="GA", vce(cluster statefip);

};
*/


/* Define treatment according to year rather than cohort--if we do this we lose positive result for all groups especially with additional controls, with or without GA. 
gen strongmerit2=(year>=aidyear) & strongstate == 1;
*/

/*
Doing it this way totally changes results--it looks like, if anything, women experience larger effect, as in pictures. But of course, treatment is not defined correctly!
gen st97=(strongstate==1)*(year==1997);
gen st99=(strongstate==1)*(year==1999);
gen st01=(strongstate==1)*(year==2001); 
*/



/* Regular results robust to taking out all controls except state and year effects (also adding back in state time trends and region by year effects) */
/* Regular results robust to not including GA */
/* GA shows large positive effects overall, large positive for men with additional controls (but weak w/0), large positive for women w/o additional controls (but weak w/)*/
/* FL & NM show large positive effects overall, weak but positive for men and large and positive for women w/ and w/o additional controls */
/* LA & SC show large negative effects w/o additional controls, weak w/, weak but positive effects for men and large and negative effects for women w/ and w/o additional controls*/
/* KY shows large positive effects overall, large positive effects for men and large negative effects for women w/ and w/o additional controls */

/*
foreach x in /*binged_2wk*/ /*dr5pl*/ num5pl /*times_gt5*/ /*drank*/ /*numtimes*/ /*drks_gt3*/ /*numdrks*/ vol30 /*allowance*/ /*hsnumtimes*/ {;

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & state!="GA" & state!="FL" & state!="NM" & state!="LA" & state!="SC" /*& state!="KY"*/, vce(cluster statefip);
*outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) replace;
reg `x' strongmerit /*weakmerit*/ `small' if (strongstate==1 | anyaidstate==0) & reg_sample==1 & state!="GA" & state!="FL" & state!="NM" & state!="LA" & state!="SC" /*& state!="KY"*/, vce(cluster statefip);
*outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `smallpl' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & state!="GA" & state!="FL" & state!="NM" & state!="LA" & state!="SC" /*& state!="KY"*/, vce(cluster statefip);
*outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `smallpl' if (strongstate==1 | anyaidstate==0) & reg_sample==1 & state!="GA" & state!="FL" & state!="NM" & state!="LA" & state!="SC" /*& state!="KY"*/, vce(cluster statefip);
*outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & state!="GA" & state!="FL" & state!="NM" & state!="LA" & state!="SC" /*& state!="KY"*/, vce(cluster statefip);
*outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) replace;
reg `x' strongmerit /*weakmerit*/ `small' if female==0 & (strongstate==1 | anyaidstate==0) & reg_sample==1 & state!="GA" & state!="FL" & state!="NM" & state!="LA" & state!="SC" /*& state!="KY"*/, vce(cluster statefip);
*outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `smallpl' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & state!="GA" & state!="FL" & state!="NM" & state!="LA" & state!="SC" /*& state!="KY"*/, vce(cluster statefip);
*outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `smallpl' if female==0 & (strongstate==1 | anyaidstate==0) & reg_sample==1 & state!="GA" & state!="FL" & state!="NM" & state!="LA" & state!="SC" /*& state!="KY"*/, vce(cluster statefip);
*outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & state!="GA" & state!="FL" & state!="NM" & state!="LA" & state!="SC" /*& state!="KY"*/, vce(cluster statefip);
*outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) replace;
reg `x' strongmerit /*weakmerit*/ `small' if female==1 & (strongstate==1 | anyaidstate==0) & reg_sample==1 & state!="GA" & state!="FL" & state!="NM" & state!="LA" & state!="SC" /*& state!="KY"*/, vce(cluster statefip);
*outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit /*weakmerit*/ `smallpl' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & state!="GA" & state!="FL" & state!="NM" & state!="LA" & state!="SC" /*& state!="KY"*/, vce(cluster statefip);
*outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit /*weakmerit*/ `smallpl' if female==1 & (strongstate==1 | anyaidstate==0) & reg_sample==1 & state!="GA" & state!="FL" & state!="NM" & state!="LA" & state!="SC" /*& state!="KY"*/, vce(cluster statefip);
*outreg2 strongmerit using `x'tab2, excel bdec(3) tdec(3) rdec(3) append;

};
*/



/*
/********* A new approach to event study that includes all observations in same regression **********/

forvalues x = 1989/2001 {;
gen f`x'=(froshyear==`x');
gen GA`x'=(state=="GA")*(froshyear==`x');
gen FL`x'=(state=="FL")*(froshyear==`x'); 
gen NM`x'=(state=="NM")*(froshyear==`x');
gen LA`x'=(state=="LA")*(froshyear==`x');
gen SC`x'=(state=="SC")*(froshyear==`x');
gen KY`x'=(state=="KY")*(froshyear==`x');
};

gen GA_89_92=(state=="GA")*(froshyear>=1989 & froshyear<=1992);
gen GA_93_95=(state=="GA")*(froshyear>=1993 & froshyear<=1995);
gen GA_96_98=(state=="GA")*(froshyear>=1996 & froshyear<=1998);
gen GA_99_01=(state=="GA")*(froshyear>=1999 & froshyear<=2001);

gen FLNM_89_92=(state=="FL" | state=="NM")*(froshyear>=1989 & froshyear<=1992);
gen FLNM_93_95=(state=="FL" | state=="NM")*(froshyear>=1993 & froshyear<=1995);
gen FLNM_96_98=(state=="FL" | state=="NM")*(froshyear>=1996 & froshyear<=1998);
gen FLNM_99_01=(state=="FL" | state=="NM")*(froshyear>=1999 & froshyear<=2001);

gen FL_89_92=(state=="FL")*(froshyear>=1989 & froshyear<=1992);
gen FL_93_95=(state=="FL")*(froshyear>=1993 & froshyear<=1995);
gen FL_96_98=(state=="FL")*(froshyear>=1996 & froshyear<=1998);
gen FL_99_01=(state=="FL")*(froshyear>=1999 & froshyear<=2001);

gen NM_89_92=(state=="NM")*(froshyear>=1989 & froshyear<=1992);
gen NM_93_95=(state=="NM")*(froshyear>=1993 & froshyear<=1995);
gen NM_96_98=(state=="NM")*(froshyear>=1996 & froshyear<=1998);
gen NM_99_01=(state=="NM")*(froshyear>=1999 & froshyear<=2001);

gen LASC_89_92=(state=="LA" | state=="SC")*(froshyear>=1989 & froshyear<=1992);
gen LASC_93_95=(state=="LA" | state=="SC")*(froshyear>=1993 & froshyear<=1995);
gen LASC_96_98=(state=="LA" | state=="SC")*(froshyear>=1996 & froshyear<=1998);
gen LASC_99_01=(state=="LA" | state=="SC")*(froshyear>=1999 & froshyear<=2001);

gen LA_89_92=(state=="LA")*(froshyear>=1989 & froshyear<=1992);
gen LA_93_95=(state=="LA")*(froshyear>=1993 & froshyear<=1995);
gen LA_96_98=(state=="LA")*(froshyear>=1996 & froshyear<=1998);
gen LA_99_01=(state=="LA")*(froshyear>=1999 & froshyear<=2001);

gen SC_89_92=(state=="SC")*(froshyear>=1989 & froshyear<=1992);
gen SC_93_95=(state=="SC")*(froshyear>=1993 & froshyear<=1995);
gen SC_96_98=(state=="SC")*(froshyear>=1996 & froshyear<=1998);
gen SC_99_01=(state=="SC")*(froshyear>=1999 & froshyear<=2001);

gen KY_89_92=(state=="KY")*(froshyear>=1989 & froshyear<=1992);
gen KY_93_95=(state=="KY")*(froshyear>=1993 & froshyear<=1995);
gen KY_96_98=(state=="KY")*(froshyear>=1996 & froshyear<=1998);
gen KY_99_01=(state=="KY")*(froshyear>=1999 & froshyear<=2001);
/*
replace f1990=1 if f1989==1;
replace f2000=1 if f2001==1;

replace GA1990=1 if GA1989==1;
replace GA2000=1 if GA2001==1;

replace FL1990=1 if FL1989==1;
replace FL2000=1 if FL2001==1;

replace NM1990=1 if NM1989==1;
replace NM2000=1 if NM2001==1;

replace LA1990=1 if LA1989==1;
replace LA2000=1 if LA2001==1;

replace SC1990=1 if SC1989==1;
replace SC2000=1 if SC2001==1;

replace KY1990=1 if KY1989==1;
replace KY2000=1 if KY2001==1;
*/

gen coh_89_92=(froshyear>=1989 & froshyear<=1992);
gen coh_93_95=(froshyear>=1993 & froshyear<=1995);
gen coh_96_98=(froshyear>=1996 & froshyear<=1998);
gen coh_99_01=(froshyear>=1999 & froshyear<=2001);

local states s1-s41;
local cohort /*f1989 f1990 f1991 f1992 f1993 f1994 f1995 f1996 f1997 f1998 f1999 f2000 f2001*/ coh_93_95 coh_96_98 coh_99_01;
local GA /*GA1989 GA1990 GA1991 GA1992 GA1993 GA1994 GA1995 GA1996 GA1997 GA1998 GA1999 GA2000 GA2001*/ GA_93_95 GA_96_98 GA_99_01;
local FL /*FL1989 FL1990 FL1991 FL1992 FL1993 FL1994 FL1995 FL1996 FL1997 FL1998 FL1999 FL2000 FL2001*/ FL_93_95 FL_96_98 FL_99_01;
local NM /*NM1989 NM1990 NM1991 NM1992 NM1993 NM1994 NM1995 NM1996 NM1997 NM1998 NM1999 NM2000 NM2001*/ NM_93_95 NM_96_98 NM_99_01;;
local LA /*LA1989 LA1990 LA1991 LA1992 LA1993 LA1994 LA1995 LA1996 LA1997 LA1998 LA1999 LA2000 LA2001*/ LA_93_95 LA_96_98 LA_99_01;
local SC /*SC1989 SC1990 SC1991 SC1992 SC1993 SC1994 SC1995 SC1996 SC1997 SC1998 SC1999 SC2000 SC2001*/ SC_93_95 SC_96_98 SC_99_01;;
local KY /*KY1989 KY1990 KY1991 KY1992 KY1993 KY1994 KY1995 KY1996 KY1997 KY1998 KY1999 KY2000 KY2001*/ KY_93_95 KY_96_98 KY_99_01;




foreach x in /*binged_2wk*/ /*dr5pl*/ num5pl /*times_gt5*/ /*drank*/ /*numtimes*/ /*drks_gt3*/ /*numdrks*/ vol30 /*allowance*/ /*hsnumtimes*/ {;

reg `x' `cohort' `GA' `FL' `NM' `LA' `SC' `KY' `states' if reg_sample==1, vce(cluster statefip);
};
*/


/* Lars's suggestion */

gen tat = (froshyear-aidyear+1) if strongstate==1 /*& aidyear<2001*/;
replace tat=tat-1 if tat<=0 & strongstate==1;
replace tat=0 if (strongstate==0 | aidyear>2001) & froshyear<.;

gen tat1=(tat<=-5) if tat<.;
gen tat2=(tat>-5 & tat<=-1) if tat<.;
gen tat3=(tat>=1 & tat<5) if tat<.;
gen tat4=(tat>=5) if tat<.;

*tab froshyear, gen(coh);

gen coh1=(froshyear<1992);
gen coh2=(froshyear>=1992 & froshyear<1995);
gen coh3=(froshyear>=1995 & froshyear<1998);
gen coh4=(froshyear>=1998 & froshyear<.);

*reg num5pl coh2-coh13 tat_10_5 tat_4_1 tat_1_4 tat_5_10 if reg_sample==1, vce(cluster statefip);


reg num5pl tat2-tat4 `small' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1, vce(cluster statefip);
test tat2=tat3;
reg num5pl tat2-tat4 `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1, vce(cluster statefip);
reg num5pl tat2-tat4 `small' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1, vce(cluster statefip);

reg num5pl tat2-tat4 s1-s41 coh2-coh4 /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1, vce(cluster statefip);
test tat2=tat3;
outreg2 tat2-tat4 coh2-coh4 using lars, excel bdec(3) tdec(3) rdec(3) replace;
reg num5pl tat2-tat4 s1-s41 coh2-coh4 /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1, vce(cluster statefip);
reg num5pl tat2-tat4 s1-s41 coh2-coh4 /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1, vce(cluster statefip);

reg num5pl tat2-tat4 `large' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1, vce(cluster statefip);
reg num5pl tat2-tat4 `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1, vce(cluster statefip);
reg num5pl tat2-tat4 `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1, vce(cluster statefip);

reg num5pl tat2-tat4 `largep' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1, vce(cluster statefip);
reg num5pl tat2-tat4 `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1, vce(cluster statefip);
reg num5pl tat2-tat4 `largep' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1, vce(cluster statefip);



/*
/* GA cohort analysis */

replace aidyear=1993 if (strongstate==0 | aidyear>2001);
gen cohort = (froshyear-aidyear+1);

gen coh1 = (cohort<=0) if cohort<.;
gen coh2 = (cohort>=1 & cohort<=3) if cohort<.;
gen coh3 = (cohort>=4 & cohort<=6) if cohort<.;
gen coh4 = (cohort>=7) if cohort<.;

foreach x in coh1 coh2 coh3 coh4 {;
gen s`x' = strongstate*(`x');
};

local states s1-s41;

foreach x in /*binged_2wk*/ /*dr5pl*/ num5pl /*times_gt5*/ /*drank*/ /*numtimes*/ /*drks_gt3*/ /*numdrks*/ vol30 /*allowance*/ /*hsnumtimes*/ {;

reg `x' coh2-coh4 scoh2-scoh4 `states' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 /*& state!="GA"*/ & state!="FL" & state!="NM" & state!="LA" & state!="SC" & state!="KY", vce(cluster statefip);
outreg2 coh2-coh4 scoh2-scoh4 using `x'tab4, excel bdec(3) tdec(3) rdec(3) replace;

reg `x' coh2-coh4 scoh2-scoh4 `states' a17-a25 female white-other frosh-gsen hispanic fcol mcol norel-othrel medianinc taxrate unemp if reg_sample==1 
/*& state!="GA"*/ & state!="FL" & state!="NM" & state!="LA" & state!="SC" & state!="KY", vce(cluster statefip);

reg `x' coh2-coh4 scoh2-scoh4 `states' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 /*& state!="GA"*/ & state!="FL" & state!="NM" & state!="LA" & state!="SC" & state!="KY", vce(cluster statefip);

reg `x' coh2-coh4 scoh2-scoh4 `states' a17-a25 female white-other frosh-gsen hispanic fcol mcol norel-othrel medianinc taxrate unemp if female==0 & reg_sample==1 
/*& state!="GA"*/ & state!="FL" & state!="NM" & state!="LA" & state!="SC" & state!="KY", vce(cluster statefip);

reg `x' coh2-coh4 scoh2-scoh4 `states' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 /*& state!="GA"*/ & state!="FL" & state!="NM" & state!="LA" & state!="SC" & state!="KY", vce(cluster statefip);

reg `x' coh2-coh4 scoh2-scoh4 `states' a17-a25 female white-other frosh-gsen hispanic fcol mcol norel-othrel medianinc taxrate unemp if female==1 & reg_sample==1 
/*& state!="GA"*/ & state!="FL" & state!="NM" & state!="LA" & state!="SC" & state!="KY", vce(cluster statefip);
};

/* FL, NM cohort analysis */

drop cohort coh1-coh4 scoh1-scoh4;

replace aidyear=1997 if (strongstate==0 | aidyear>2001);
gen cohort = (froshyear-aidyear+1);

gen coh1 = (cohort<=-3) if cohort<.;
gen coh2 = (cohort>=-2 & cohort<=0) if cohort<.;
gen coh3 = (cohort>=1 & cohort<=3) if cohort<.;
gen coh4 = (cohort>=4) if cohort<.;

foreach x in coh1 coh2 coh3 coh4 {;
gen s`x' = strongstate*(`x');
};

local states s1-s41;

foreach x in /*binged_2wk*/ /*dr5pl*/ num5pl /*times_gt5*/ /*drank*/ /*numtimes*/ /*drks_gt3*/ /*numdrks*/ vol30 /*allowance*/ /*hsnumtimes*/ {;

reg `x' coh1 coh3 coh4 scoh1 scoh3 scoh4 `states' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & state!="GA" /*& state!="FL" & state!="NM"*/ & state!="LA" & state!="SC" & state!="KY", vce(cluster statefip);
outreg2 coh1 coh3 coh4 scoh1 scoh3 scoh4 using `x'tab4, excel bdec(3) tdec(3) rdec(3) append;

reg `x' coh2-coh4 scoh2-scoh4 `states' a17-a25 female white-other frosh-gsen hispanic fcol mcol norel-othrel medianinc taxrate unemp if reg_sample==1 
& state!="GA" /*& state!="FL" & state!="NM"*/ & state!="LA" & state!="SC" & state!="KY", vce(cluster statefip);

reg `x' coh2-coh4 scoh2-scoh4 `states' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & state!="GA" /*& state!="FL" & state!="NM"*/ & state!="LA" & state!="SC" & state!="KY", vce(cluster statefip);

reg `x' coh2-coh4 scoh2-scoh4 `states' a17-a25 female white-other frosh-gsen hispanic fcol mcol norel-othrel medianinc taxrate unemp if female==0 & reg_sample==1 
& state!="GA" /*& state!="FL" & state!="NM"*/ & state!="LA" & state!="SC" & state!="KY", vce(cluster statefip);

reg `x' coh2-coh4 scoh2-scoh4 `states' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & state!="GA" /*& state!="FL" & state!="NM"*/ & state!="LA" & state!="SC" & state!="KY", vce(cluster statefip);

reg `x' coh2-coh4 scoh2-scoh4 `states' a17-a25 female white-other frosh-gsen hispanic fcol mcol norel-othrel medianinc taxrate unemp if female==1 & reg_sample==1 
& state!="GA" /*& state!="FL" & state!="NM"*/ & state!="LA" & state!="SC" & state!="KY", vce(cluster statefip);
};

/* LA, SC cohort analysis */

drop cohort coh1-coh4 scoh1-scoh4;

replace aidyear=1998 if (strongstate==0 | aidyear>2001);
gen cohort = (froshyear-aidyear+1);

gen coh1 = (cohort<=-3) if cohort<.;
gen coh2 = (cohort>=-2 & cohort<=0) if cohort<.;
gen coh3 = (cohort>=1 & cohort<=3) if cohort<.;
gen coh4 = (cohort>=4) if cohort<.;

foreach x in coh1 coh2 coh3 coh4 {;
gen s`x' = strongstate*(`x');
};

local states s1-s41;

foreach x in /*binged_2wk*/ /*dr5pl*/ num5pl /*times_gt5*/ /*drank*/ /*numtimes*/ /*drks_gt3*/ /*numdrks*/ vol30 /*allowance*/ /*hsnumtimes*/ {;

reg `x' coh1 coh3 coh4 scoh1 scoh3 scoh4 `states' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & state!="GA" & state!="FL" & state!="NM" /*& state!="LA" & state!="SC"*/ & state!="KY", vce(cluster statefip);
outreg2 coh1 coh3 coh4 scoh1 scoh3 scoh4 using `x'tab4, excel bdec(3) tdec(3) rdec(3) append;

reg `x' coh2-coh4 scoh2-scoh4 `states' a17-a25 female white-other frosh-gsen hispanic fcol mcol norel-othrel medianinc taxrate unemp if reg_sample==1 
& state!="GA" & state!="FL" & state!="NM" /*& state!="LA" & state!="SC"*/ & state!="KY", vce(cluster statefip);

reg `x' coh2-coh4 scoh2-scoh4 `states' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & state!="GA" & state!="FL" & state!="NM" /*& state!="LA" & state!="SC"*/ & state!="KY", vce(cluster statefip);

reg `x' coh2-coh4 scoh2-scoh4 `states' a17-a25 female white-other frosh-gsen hispanic fcol mcol norel-othrel medianinc taxrate unemp if female==0 & reg_sample==1 
& state!="GA" & state!="FL" & state!="NM" /*& state!="LA" & state!="SC"*/ & state!="KY", vce(cluster statefip);

reg `x' coh2-coh4 scoh2-scoh4 `states' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & state!="GA" & state!="FL" & state!="NM" /*& state!="LA" & state!="SC"*/ & state!="KY", vce(cluster statefip);

reg `x' coh2-coh4 scoh2-scoh4 `states' a17-a25 female white-other frosh-gsen hispanic fcol mcol norel-othrel medianinc taxrate unemp if female==1 & reg_sample==1 
& state!="GA" & state!="FL" & state!="NM" /*& state!="LA" & state!="SC"*/ & state!="KY", vce(cluster statefip);
};

/* KY cohort analysis */

drop cohort coh1-coh4 scoh1-scoh4;

replace aidyear=1999 if (strongstate==0 | aidyear>2001);
gen cohort = (froshyear-aidyear+1);

gen coh1 = (cohort<=-6) if cohort<.;
gen coh2 = (cohort>=-5 & cohort<=-3) if cohort<.;
gen coh3 = (cohort>=-2 & cohort<=0) if cohort<.;
gen coh4 = (cohort>=1) if cohort<.;

foreach x in coh1 coh2 coh3 coh4 {;
gen s`x' = strongstate*(`x');
};

local states s1-s41;

foreach x in /*binged_2wk*/ /*dr5pl*/ num5pl /*times_gt5*/ /*drank*/ /*numtimes*/ /*drks_gt3*/ /*numdrks*/ vol30 /*allowance*/ /*hsnumtimes*/ {;

reg `x' coh1 coh2 coh4 scoh1 scoh2 scoh4 `states' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & state!="GA" & state!="FL" & state!="NM" & state!="LA" & state!="SC" /*& state!="KY"*/, vce(cluster statefip);
outreg2 coh1 coh2 coh4 scoh1 scoh2 scoh4 using `x'tab4, excel bdec(3) tdec(3) rdec(3) append;

reg `x' coh2-coh4 scoh2-scoh4 `states' a17-a25 female white-other frosh-gsen hispanic fcol mcol norel-othrel medianinc taxrate unemp if reg_sample==1 
& state!="GA" & state!="FL" & state!="NM" & state!="LA" & state!="SC" /*& state!="KY"*/, vce(cluster statefip);

reg `x' coh2-coh4 scoh2-scoh4 `states' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & state!="GA" & state!="FL" & state!="NM" & state!="LA" & state!="SC" /*& state!="KY"*/, vce(cluster statefip);

reg `x' coh2-coh4 scoh2-scoh4 `states' a17-a25 female white-other frosh-gsen hispanic fcol mcol norel-othrel medianinc taxrate unemp if female==0 & reg_sample==1 
& state!="GA" & state!="FL" & state!="NM" & state!="LA" & state!="SC" /*& state!="KY"*/, vce(cluster statefip);

reg `x' coh2-coh4 scoh2-scoh4 `states' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & state!="GA" & state!="FL" & state!="NM" & state!="LA" & state!="SC" /*& state!="KY"*/, vce(cluster statefip);

reg `x' coh2-coh4 scoh2-scoh4 `states' a17-a25 female white-other frosh-gsen hispanic fcol mcol norel-othrel medianinc taxrate unemp if female==1 & reg_sample==1 
& state!="GA" & state!="FL" & state!="NM" & state!="LA" & state!="SC" /*& state!="KY"*/, vce(cluster statefip);
};
*/


/*

/**************** Analysis for EER special issue *****************/
/*
reg num5pl strongmerit `large' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & year>=1997 & homestate==1, vce(cluster statefip);
reg num5pl strongmerit `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & year>=1997 & homestate==1, vce(cluster statefip);

reg num5pl strongmerit `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & year>=1997 & homestate==1, vce(cluster statefip);
reg num5pl strongmerit `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & year>=1997 & homestate==1, vce(cluster statefip);

reg vol30 strongmerit `large' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & year>=1997 & homestate==1, vce(cluster statefip);
reg vol30 strongmerit `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & year>=1997 & homestate==1, vce(cluster statefip);

reg vol30 strongmerit `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & year>=1997 & homestate==1, vce(cluster statefip);
reg vol30 strongmerit `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & year>=1997 & homestate==1, vce(cluster statefip);
*/

foreach x in drdrive /*bngdrive*/ /*marij othdrug*/ pardrk anydrug1 owncrime othcrime unsex probsex work allowance /*binge--looks the same as dr5pl*/ supbinge dr5pl num5pl vol30 hsnumtimes {;
reg `x' strongmerit `large' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x', excel bdec(3) tdec(3) rdec(3) replace;
reg `x' strongmerit `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x', excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'm, excel bdec(3) tdec(3) rdec(3) replace;
reg `x' strongmerit `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'm, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'f, excel bdec(3) tdec(3) rdec(3) replace;
reg `x' strongmerit `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1, vce(cluster statefip);
outreg2 strongmerit using `x'f, excel bdec(3) tdec(3) rdec(3) append;


reg `x' strongmerit `large' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & age<21, vce(cluster statefip);
outreg2 strongmerit using `x', excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & age<21, vce(cluster statefip);
outreg2 strongmerit using `x', excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & age<21, vce(cluster statefip);
outreg2 strongmerit using `x'm, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & age<21, vce(cluster statefip);
outreg2 strongmerit using `x'm, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & age<21, vce(cluster statefip);
outreg2 strongmerit using `x'f, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & age<21, vce(cluster statefip);
outreg2 strongmerit using `x'f, excel bdec(3) tdec(3) rdec(3) append;


reg `x' strongmerit `large' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & age>=21, vce(cluster statefip);
outreg2 strongmerit using `x', excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & age>=21, vce(cluster statefip);
outreg2 strongmerit using `x', excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & age>=21, vce(cluster statefip);
outreg2 strongmerit using `x'm, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & age>=21, vce(cluster statefip);
outreg2 strongmerit using `x'm, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit `large' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & age>=21, vce(cluster statefip);
outreg2 strongmerit using `x'f, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & age>=21, vce(cluster statefip);
outreg2 strongmerit using `x'f, excel bdec(3) tdec(3) rdec(3) append;


reg `x' strongmerit `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & age>=21 & gpa>=3.0, vce(cluster statefip);
outreg2 strongmerit using `x', excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if reg_sample==1 & age>=21 & gpa<3.0, vce(cluster statefip);
outreg2 strongmerit using `x', excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & age>=21 & gpa>=3.0, vce(cluster statefip);
outreg2 strongmerit using `x'm, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==0 & reg_sample==1 & age>=21 & gpa<3.0, vce(cluster statefip);
outreg2 strongmerit using `x'm, excel bdec(3) tdec(3) rdec(3) append;

reg `x' strongmerit `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & age>=21 & gpa>=3.0, vce(cluster statefip);
outreg2 strongmerit using `x'f, excel bdec(3) tdec(3) rdec(3) append;
reg `x' strongmerit `largepl' /*if (strongstate==1 | anyaidstate==0)*/ if female==1 & reg_sample==1 & age>=21 & gpa<3.0, vce(cluster statefip);
outreg2 strongmerit using `x'f, excel bdec(3) tdec(3) rdec(3) append;

};

/*
reg gpa strongmerit a17-a25 female white-other frosh-gsen married hispanic offcamp memgreek year1993-year2005 /*gpa_a-gpa_na*/ /*hstimesdrk_mo_0-hstimesdrk_mo_40pl*/ fcol mcol norel-othrel 
pub_pri rural commuter en1-en4 comp1-comp8 religious medianinc taxrate unemp s1-s41 /*cid1-cid144*/ /*t1-t55 neast1993-west2005*/, vce(cluster statefip);
reg gpa strongmerit a17-a25 female white-other frosh-gsen married hispanic offcamp memgreek year1993-year2005 /*gpa_a-gpa_na*/ /*hstimesdrk_mo_0-hstimesdrk_mo_40pl*/ fcol mcol norel-othrel 
pub_pri rural commuter en1-en4 comp1-comp8 religious medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 neast1993-west2005, vce(cluster statefip);

reg gpa strongmerit a17-a25 female white-other frosh-gsen married hispanic offcamp memgreek year1993-year2005 /*gpa_a-gpa_na*/ /*hstimesdrk_mo_0-hstimesdrk_mo_40pl*/ fcol mcol norel-othrel 
pub_pri rural commuter en1-en4 comp1-comp8 religious medianinc taxrate unemp s1-s41 /*cid1-cid144*/ /*t1-t55 neast1993-west2005*/ if female==0, vce(cluster statefip);
reg gpa strongmerit a17-a25 female white-other frosh-gsen married hispanic offcamp memgreek year1993-year2005 /*gpa_a-gpa_na*/ /*hstimesdrk_mo_0-hstimesdrk_mo_40pl*/ fcol mcol norel-othrel 
pub_pri rural commuter en1-en4 comp1-comp8 religious medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 neast1993-west2005 if female==0, vce(cluster statefip);

reg gpa strongmerit a17-a25 female white-other frosh-gsen married hispanic offcamp memgreek year1993-year2005 /*gpa_a-gpa_na*/ /*hstimesdrk_mo_0-hstimesdrk_mo_40pl*/ fcol mcol norel-othrel 
pub_pri rural commuter en1-en4 comp1-comp8 religious medianinc taxrate unemp s1-s41 /*cid1-cid144*/ /*t1-t55 neast1993-west2005*/ if female==1, vce(cluster statefip);
reg gpa strongmerit a17-a25 female white-other frosh-gsen married hispanic offcamp memgreek year1993-year2005 /*gpa_a-gpa_na*/ /*hstimesdrk_mo_0-hstimesdrk_mo_40pl*/ fcol mcol norel-othrel 
pub_pri rural commuter en1-en4 comp1-comp8 religious medianinc taxrate unemp s1-s41 /*cid1-cid144*/ t1-t55 neast1993-west2005 if female==1, vce(cluster statefip);
*/

*/



/*
/* To compare means with NLSY97 */

keep if reg_sample==1 & (year==1999 | year==2001);
rename vol30 numdrinks_permo;
rename hispanic hisp;
rename fcol fsomcol;
rename mcol msomcol;
rename mwest ncentral;
keep drank numdrinks_permo female black hisp msomcol fsomcol neast ncentral south west;
gen sample="cas";
gen weight=1;

append using "C:\Users\ben.cowan\Bcowan\merit aid risky behaviors\nlsy_comp";

sum if sample=="nlsy" [aweight=weight];
sum if sample=="cas" [aweight=weight];

gen temp=(sample=="cas");

/* I don't know if this is kosher. Weights for CAS observations are all "1." I don't know if this is the same as a t-test for difference between (weighted) means */

foreach x in drank numdrinks_permo female black hisp msomcol fsomcol neast ncentral south west {;
reg `x' temp [aweight=weight];
};

foreach x in drank numdrinks_permo black hisp msomcol fsomcol neast ncentral south west {;
reg `x' temp [aweight=weight] if female==0;
};

foreach x in drank numdrinks_permo black hisp msomcol fsomcol neast ncentral south west {;
reg `x' temp [aweight=weight] if female==1;
};
*/

log close;
