
import plotly
from plotly.graph_objs import *

plotly.offline.plot({
    "data": [
        Scatter(
        x=[2,3,4,5,6],
        y=[0.021, 0.007, 0.087, 0.085, 0.034],
        error_y=dict(
            type='data',
            array=[0.113*1.96, 0.105*1.96, 0.126*1.96, 0.161*1.96, 0.169*1.96],
            visible=True,
            color="black"
        ),
        mode="markers",
        marker = dict(size=25, symbol="square", color="black"),
    )
],
    "layout": Layout(title="Varying the Response Lag Among Female Respondents (w/ 95% CI)",
        height=500, width=600,
        xaxis=dict(title="Response Lag (Weeks)"),
        yaxis=dict(title="Change in Binge Drinking (# Incidents)"))
})
