
import plotly
from plotly.graph_objs import *

plotly.offline.plot({
    "data": [
        Scatter(
        x=[2,3,4,5,6],
        y=[-0.029, 0.067, 0.305, 0.193, -0.001],
        error_y=dict(
            type='data',
            array=[0.096*1.96, 0.088*1.96, 0.106*1.96, 0.130*1.96, 0.137*1.96],
            visible=True,
            color="black"
        ),
        mode="markers",
        marker = dict(size=25, symbol="square", color="black"),
    )
],
    "layout": Layout(title="Varying the Response Lag (w/ 95% CI)",
        height=500, width=600,
        xaxis=dict(title="Response Lag (Weeks)"),
        yaxis=dict(title="Change in Binge Drinking (# Incidents)"))
})
