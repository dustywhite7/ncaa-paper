
import plotly
from plotly.graph_objs import *

plotly.offline.plot({
    "data": [
        Scatter(
        x=[2,3,4,5,6],
        y=[-0.149, 0.126, 0.580, 0.330, -0.028],
        error_y=dict(
            type='data',
            array=[0.165*1.96, 0.153*1.96, 0.179*1.96, 0.212*1.96, 0.227*1.96],
            visible=True,
            color="black"
        ),
        mode="markers",
        marker = dict(size=25, symbol="square", color="black"),
    )
],
    "layout": Layout(title="Varying the Response Lag Among Male Respondents (w/ 95% CI)",
        height=500, width=600,
        xaxis=dict(title="Response Lag (Weeks)"),
        yaxis=dict(title="Change in Binge Drinking (# Incidents)"))
})
