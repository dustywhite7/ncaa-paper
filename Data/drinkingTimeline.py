
import plotly
from plotly.graph_objs import *

plotly.offline.plot({
    "data": [
        Scatter(
        x=[0, 1, 2],
        y=[1.212, 1.285, 1.107],
        mode="lines",
        line = dict(width=4, color="black"),
	name = "All Observations"
    ),
	Scatter(
		x=[0, 1, 2],
		y=[1.467, 1.520, 1.370],
		mode="lines",
		line = dict(width=4, color="black", dash="dash"),
		name = "Tournament Schools"
	    ),
	Scatter(
		x=[0, 1, 2],
		y=[1.165, 1.259, 1.083],
		mode="lines",
		line = dict(width=4, color="black", dash="dot"),
		name = "Non-Tournament Schools"
	    )
],
    "layout": Layout(title="Binge Drinking During the Response Window",
        height=500, width=600,
        xaxis=dict(title="Response Timing Relative to NCAA Tournament", tickmode="array", tickvals=[0,1,2], ticktext=["Before","During","After"]),
        yaxis=dict(title="Mean Binge Drinking (# Incidents in Past Month)"))
})
